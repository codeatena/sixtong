package com.han.quickblox.module;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.han.chatapp.GlobalData;
import com.han.chatapp.main.MainActivity;
import com.han.chatapp.model.User;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.model.QBDialog;
import com.quickblox.core.QBEntityCallbackImpl;
import com.quickblox.core.request.QBCustomObjectRequestBuilder;
import com.quickblox.core.request.QBPagedRequestBuilder;
import com.quickblox.customobjects.QBCustomObjects;
import com.quickblox.customobjects.model.QBCustomObject;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;
import android.os.Bundle;

public class QBRuntimeService {

	public void startService() {
		readMe();
	}
	
	public void readMe() {
    	QBPagedRequestBuilder pagedRequestBuilder = new QBPagedRequestBuilder();
    	pagedRequestBuilder = null;
    	
    	ArrayList<Integer> arrIDs = new ArrayList<Integer>();
    	arrIDs.add(GlobalData.myUser.qbUser.getId());
    	
    	QBUsers.getUsersByIDs(arrIDs, pagedRequestBuilder, new QBEntityCallbackImpl<ArrayList<QBUser>>() {
    		@Override
            public void onSuccess(ArrayList<QBUser> qbUsers, Bundle bundle) {

    			if (qbUsers.size() > 0) {
    				GlobalData.myUser.qbUser = qbUsers.get(0);
    				readMyMoreInfo();
    			}
            }
    		
            @Override
            public void onError(List<String> errors) {

            }
    	});
	}
	
	public void readMyMoreInfo() {
		QBCustomObjectRequestBuilder requestBuilder = new QBCustomObjectRequestBuilder();
		requestBuilder.eq("user_id", GlobalData.myUser.qbUser.getId());
		
		QBCustomObjects.getObjects("Contacts", requestBuilder, new QBEntityCallbackImpl<ArrayList<QBCustomObject>>() {
			@SuppressWarnings("unchecked")
			@Override
		    public void onSuccess(ArrayList<QBCustomObject> arg0, Bundle bundle) {

				QBCustomObject tmpObject = arg0.get(0);
            	GlobalData.myUser.qbCustomObjectID = tmpObject.getCustomObjectId();
            	
            	HashMap<String, Object> fields = tmpObject.getFields();
            	for (String key: fields.keySet()) {
            		if (key.equals("friend_ids")) {
            			ArrayList<String> value = (ArrayList<String>) fields.get(key);
            			if (value != null) {
            				GlobalData.myUser.arrFriendIDs = value;
            			} else {
            				GlobalData.myUser.arrFriendIDs = new ArrayList<String>();
            			}
            		}
            		if (key.equals("invite_ids")) {
            			ArrayList<String> value = (ArrayList<String>) fields.get(key);
            			if (value != null) {
            				GlobalData.myUser.arrInviterIDs = value;
            			} else {
            				GlobalData.myUser.arrInviterIDs = new ArrayList<String>();
            			}
            		}
            		if (key.equals("secret_id")) {
            			String value = (String) fields.get(key);
            			if (value != null) {
            				GlobalData.myUser.secretID = value;
            			} else {
            				GlobalData.myUser.secretID = "";
            			}
            		}
            		if (key.equals("photo")) {
            			String value = (String) fields.get(key);
            			GlobalData.myUser.strPhoto = value;
            		}
            		if (key.equals("gender")) {
            			String value = (String) fields.get(key);
            			if (value != null) {
            				GlobalData.myUser.gender = value;
            			} else {
            				GlobalData.myUser.gender = "Male";
            			}
            		}
            		if (key.equals("where")) {
            			String value = (String) fields.get(key);
            			if (value != null) {
            				GlobalData.myUser.where = value;
            			} else {
            				GlobalData.myUser.where = "";
            			}
            		}
            		if (key.equals("words")) {
            			String value = (String) fields.get(key);
            			if (value != null) {
            				GlobalData.myUser.words = value;
            			}
            		}
            	}
            	
            	readDialogs();
			}
			
			@Override
			public void onError(List<String> errors) {
				// TODO Auto-generated method stub

			}
		});
	}
	
	public void readDialogs() {
		QBCustomObjectRequestBuilder customObjectRequestBuilder = new QBCustomObjectRequestBuilder();
        customObjectRequestBuilder.setPagesLimit(100);
        customObjectRequestBuilder = null;
        
        QBChatService.getChatDialogs(null, customObjectRequestBuilder, new QBEntityCallbackImpl<ArrayList<QBDialog>>() {
            @Override
            public void onSuccess(final ArrayList<QBDialog> dialogs, Bundle args) {

            	GlobalData.myUser.arrDialogs = dialogs;
            	if (dialogs == null) 
            		GlobalData.myUser.arrDialogs = new ArrayList<QBDialog>();
            	
            	readUsers();
            }

            @Override
            public void onError(List<String> errors) {

            }
        });
	}
	
	public void readUsers() {
		
		ArrayList<Integer> arrUserIDs = new ArrayList<Integer>();
		
		for (String freindID: GlobalData.myUser.arrFriendIDs) {
			arrUserIDs.add(Integer.parseInt(freindID));
		}
		for (String inviterID: GlobalData.myUser.arrInviterIDs) {
			arrUserIDs.add(Integer.parseInt(inviterID));
		}
		
		for (QBDialog dialog: GlobalData.myUser.arrDialogs) {
			ArrayList<Integer> arrOccupantIDs = dialog.getOccupants();
			for (Integer occupantID: arrOccupantIDs) {
				arrUserIDs.add(occupantID);
			}
		}
		
    	QBPagedRequestBuilder pagedRequestBuilder = new QBPagedRequestBuilder();
    	pagedRequestBuilder = null;
    	
    	QBUsers.getUsersByIDs(arrUserIDs, pagedRequestBuilder, new QBEntityCallbackImpl<ArrayList<QBUser>>() {
    		@Override
            public void onSuccess(ArrayList<QBUser> qbUsers, Bundle bundle) {
				
				readUsersMoreInf(qbUsers);
            }
    		
            @Override
            public void onError(List<String> errors) {

            }
    	});
	}
	
	public void readUsersMoreInf(final ArrayList<QBUser> arrQBUser) {
		QBCustomObjectRequestBuilder requestBuilder = new QBCustomObjectRequestBuilder();
		
		ArrayList<Integer> arrUserIDs = new ArrayList<Integer>();
		for (QBUser qbUser: arrQBUser) {
			arrUserIDs.add(qbUser.getId());
		}
		
		requestBuilder.in("user_id", arrUserIDs);
	
		QBCustomObjects.getObjects("Contacts", requestBuilder, new QBEntityCallbackImpl<ArrayList<QBCustomObject>>() {
			@SuppressWarnings("unchecked")
			@Override
		    public void onSuccess(ArrayList<QBCustomObject> arrQBCustomObject, Bundle bundle) {
				
				ArrayList<User> arrUsers = new ArrayList<User>();
				for (QBCustomObject qbCustomObject: arrQBCustomObject) {
					User user = new User();
					int qbUserID = qbCustomObject.getUserId();
					for (QBUser qbUser: arrQBUser) {
						if (qbUserID == qbUser.getId()) {
							user.qbUser = qbUser;
							break;
						}
					}
					
					user.qbCustomObjectID = qbCustomObject.getCustomObjectId();
	            	HashMap<String, Object> fields = qbCustomObject.getFields();
	            	
	            	for (String key: fields.keySet()) {
	            		if (key.equals("friend_ids")) {
							ArrayList<String> value = (ArrayList<String>) fields.get(key);
	            			if (value != null) {
	            				user.arrFriendIDs = value;
	            			} else {
	            				user.arrFriendIDs = new ArrayList<String>();
	            			}
	            		}
	            		if (key.equals("invite_ids")) {
							ArrayList<String> value = (ArrayList<String>) fields.get(key);
	            			if (value != null) {
	            				user.arrInviterIDs = value;
	            			} else {
	            				user.arrInviterIDs = new ArrayList<String>();
	            			}
	            		}
	            		if (key.equals("secret_id")) {
	            			String value = (String) fields.get(key);
	            			if (value != null) {
	            				user.secretID = value;
	            			}
	            		}
	            		if (key.equals("photo")) {
	            			String value = (String) fields.get(key);
	            			if (value != null) {
	            				user.strPhoto = value;
	            			}
	            		}
	            		if (key.equals("gender")) {
	            			String value = (String) fields.get(key);
	            			if (value != null) {
	            				user.gender = value;
	            			}
	            		}
	            		if (key.equals("where")) {
	            			String value = (String) fields.get(key);
	            			if (value != null) {
	            				user.where = value;
	            			}
	            		}
	            		if (key.equals("words")) {
	            			String value = (String) fields.get(key);
	            			if (value != null) {
	            				user.words = value;
	            			}
	            		}
	            	}
	            	arrUsers.add(user);
				}
				
				arrUsers.add(GlobalData.myUser);
				
				GlobalData.myUser.checkUpdatedContacts();
				GlobalData.arrAllUsers = arrUsers;
				
				User.updateUsersDatabase(arrUsers);
				
				if (MainActivity.getInstatnce() != null) {
					MainActivity.getInstatnce().updateChats();
					MainActivity.getInstatnce().updateContacts();	
				}
				
				if (GlobalData.qbPrivateChatService != null) {
					GlobalData.qbPrivateChatService.init();	
				}
				if (GlobalData.qbGroupChatService != null) {
					GlobalData.qbGroupChatService.init();	
				}
			}
			
			@Override
			public void onError(List<String> errors) {

			}
		});
	}
}