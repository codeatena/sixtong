package com.han.quickblox.module;

import java.util.List;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smackx.muc.DiscussionHistory;

import com.han.chatapp.App;
import com.han.chatapp.GlobalData;
import com.han.chatapp.chat.ChatActivity;
import com.han.chatapp.main.MainActivity;
import com.han.chatapp.model.Message;
import com.han.utility.NotificationUtility;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.QBGroupChat;
import com.quickblox.chat.QBGroupChatManager;
import com.quickblox.chat.exception.QBChatException;
import com.quickblox.chat.listeners.QBMessageListener;
import com.quickblox.chat.model.QBChatMessage;
import com.quickblox.chat.model.QBDialog;
import com.quickblox.chat.model.QBDialogType;
import com.quickblox.core.QBEntityCallbackImpl;

public class QBGroupChatService {

	QBMessageListener<QBGroupChat> groupChatQBMessageListener = new QBMessageListener<QBGroupChat>() {
	    @Override
	    public void processMessage(final QBGroupChat groupChat, final QBChatMessage chatMessage) {
	    	if (chatMessage.getBody().equals(""))
	    		return;
	    	
	    	if (chatMessage.getSenderId().equals(GlobalData.myUser.qbUser.getId())) {
	    		return;
	    	}
	    	
	    	QBDialog dialog = getDialogFromGroupChat(groupChat);
	    	if (dialog == null) return;
	    	
	    	MainActivity.getInstatnce().addGroupChatMessage(dialog, chatMessage);
	    	
	    	if (ChatActivity.dialog != null) {
		    	if (groupChat.getJid().equals(ChatActivity.dialog.getRoomJid())) {
		    		ChatActivity.getInstatnce().addMessage(chatMessage);
		    		NotificationUtility.notification(chatMessage, dialog, true);
		    	} else {
		    		NotificationUtility.notification(chatMessage, dialog, false);
		    	}
	    	} else {
	    		NotificationUtility.notification(chatMessage, dialog, false);
	    	}
	    }
	    
	    @Override
	    public void processError(final QBGroupChat groupChat, QBChatException error, QBChatMessage originMessage){
	 
	    }
	    
	    @Override
	    public void processMessageDelivered(QBGroupChat groupChat, String messageID){
	        // never be called, works only for 1-1 chat
	    }
	 
	    @Override
	    public void processMessageRead(QBGroupChat groupChat, String messageID){
	        // never be called, works only for 1-1 chat
	    }
	};
	
	private QBDialog getDialogFromGroupChat(QBGroupChat groupChat) {
    	for (QBDialog dialog: GlobalData.myUser.arrDialogs) {
    		if (dialog.getType().equals(QBDialogType.GROUP)) {
        		if (dialog.getRoomJid().equals(groupChat.getJid())) {
        			return dialog;
        		}
    		}
    	}
    	
    	return null;
	}
	
	public void init() {
		if (QBChatService.isInitialized()) {
			QBGroupChatManager groupChatManager = QBChatService.getInstance().getGroupChatManager();
			if (groupChatManager == null) return;
			
			addGroupChats();
		} else {
			QBChatService.init(App.getInstance());
		}
	}
	
	public void addGroupChats() {
		
		QBGroupChatManager groupChatManager = QBChatService.getInstance().getGroupChatManager();
		
		if (groupChatManager == null) return;
		
		for (QBDialog dialog: GlobalData.myUser.arrDialogs) {
			if (dialog.getType().equals(QBDialogType.GROUP)) {
				QBGroupChat groupChat = groupChatManager.getGroupChat(dialog.getRoomJid());
				if (groupChat == null) {
					groupChat = groupChatManager.createGroupChat(dialog.getRoomJid());
				}
				
				joinGroupChat(groupChat);
			}
		}
	}
	
	public void removeGroupChats() {
		
		QBGroupChatManager groupChatManager = QBChatService.getInstance().getGroupChatManager();

		for (QBDialog dialog: GlobalData.myUser.arrDialogs) {
			if (dialog.getType().equals(QBDialogType.GROUP)) {
				QBGroupChat groupChat = groupChatManager.getGroupChat(dialog.getRoomJid());
				if (groupChat == null) {
					groupChat = groupChatManager.createGroupChat(dialog.getRoomJid());
				}
				
				try {
					if (groupChat.isJoined()) {
						groupChat.leave();
					}
					
					groupChat.removeMessageListener(groupChatQBMessageListener);				
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	@SuppressWarnings("rawtypes")
	public void joinGroupChat(final QBGroupChat groupChat) {
		
		if (groupChat.isJoined())
			return;
		
		DiscussionHistory history = new DiscussionHistory();
		history.setMaxStanzas(0);
		
		groupChat.join(history, new QBEntityCallbackImpl() {
		    @Override
		    public void onSuccess() {
		        // add listeners
		    	groupChat.addMessageListener(groupChatQBMessageListener);
		    }
		    
		    @Override
		    public void onError(final List list) {
		    	
		    }
		});
	}
	
	public void sendMessage(QBDialog dialog, String body) {
		
		QBGroupChatManager groupChatManager = QBChatService.getInstance().getGroupChatManager();
		QBGroupChat groupChat = groupChatManager.getGroupChat(dialog.getRoomJid());
		
		if (groupChat == null) {
			groupChatManager.createGroupChat(dialog.getRoomJid());
		}
		
        QBChatMessage chatMessage = new QBChatMessage();
        
        chatMessage.setBody(body);
        chatMessage.setProperty(QBConst.PROPERTY_SAVE_TO_HISTORY, "1");
        chatMessage.setProperty(QBConst.PROPERTY_NOTIFICATION_TYPE, "1");
        
        Message.sendGroupChatMessage(dialog, body);
        
        try {
        	groupChat.sendMessage(chatMessage);
        } catch (XMPPException e) {
        	e.printStackTrace();
        } catch (SmackException.NotConnectedException e) {
        	e.printStackTrace();
        } catch (IllegalStateException e){
        	e.printStackTrace();
        }
	}
}