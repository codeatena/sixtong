package com.han.quickblox.module;

import android.app.Activity;
import com.han.chatapp.GlobalData;
import com.quickblox.chat.model.QBDialog;
import com.quickblox.chat.model.QBDialogType;

public class QBDialogService extends QBBaseService {
	
	public QBDialogService(Activity parent) {
		super(parent);
	}
	
	public QBDialogService(Activity parent, String title) {
		super(parent, title);
	}
	
	public QBDialogService(Activity parent, boolean isShowProgDlg) {
		super(parent, isShowProgDlg);
	}
	
    public static Integer getOpponentIDForPrivateDialog(QBDialog dialog){
        Integer opponentID = -1;
        for(Integer userID : dialog.getOccupants()){
            if(!userID.equals(GlobalData.myUser.qbUser.getId())) {
                opponentID = userID;
                break;
            }
        }
        return opponentID;
    }
    
    public static QBDialog getQBDialogWithSpecificUser(int receiver_id) {
    	for (QBDialog dialog: GlobalData.myUser.arrDialogs) {
    		if (dialog.getType().equals(QBDialogType.PRIVATE)) {
    			if (getOpponentIDForPrivateDialog(dialog) == receiver_id) {
    				return dialog;
    			}
    		}
    	}
    	return null;
    }
    
//    public void getMyDialogs() {
//		QBCustomObjectRequestBuilder customObjectRequestBuilder = new QBCustomObjectRequestBuilder();
//        customObjectRequestBuilder.setPagesLimit(100);
//
//        QBChatService.getChatDialogs(null, customObjectRequestBuilder, new QBEntityCallbackImpl<ArrayList<QBDialog>>() {
//            @Override
//            public void onSuccess(final ArrayList<QBDialog> dialogs, Bundle args) {
//            	if (progDlg.isShowing()) {
//            		progDlg.dismiss();
//            	}
//            	GlobalData.myUser.arrDialogs = dialogs;
//            	if (dialogs == null) GlobalData.myUser.arrDialogs = new ArrayList<QBDialog>();
//            	for (QBDialog dialog: dialogs) {
//            		ArrayList<Integer> occupantIDs = dialog.getOccupants();
//            		for (Integer occupantID: occupantIDs) {
//            			User user = User.getUserFromQBUserID(occupantID);
//            			if (user == null) {
//            				user = new User();
//            				user.getInfFromServer(occupantID);
//            			}
//            		}
//            	}
//            }
//
//            @Override
//            public void onError(List<String> errors) {
//            	if (progDlg.isShowing()) {
//            		progDlg.dismiss();
//            	}
//                DialogUtility.showGeneralAlert(parent, "Error", errors.get(0));
//            }
//        });
//    } 
}