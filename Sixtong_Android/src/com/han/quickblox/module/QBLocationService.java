package com.han.quickblox.module;

import java.util.ArrayList;
import java.util.List;

import com.han.chatapp.GlobalData;
import com.han.utility.DialogUtility;
import com.quickblox.core.QBEntityCallbackImpl;
import com.quickblox.location.QBLocations;
import com.quickblox.location.model.QBLocation;
import com.quickblox.location.request.QBLocationRequestBuilder;
import android.app.Activity;
import android.os.Bundle;

public class QBLocationService extends QBBaseService {

	public QBLocationService(Activity parent) {
		super(parent);
	}
	
	public QBLocationService(Activity parent, String title) {
		super(parent, title);
	}
	
	public QBLocationService(Activity parent, boolean isShowProgDlg) {
		super(parent, isShowProgDlg);
	}
	
	public void createLocation() {
		QBLocation location = new QBLocation(GlobalData.curLat, GlobalData.curLng, "");
		QBLocations.createLocation(location, new QBEntityCallbackImpl<QBLocation>() {
			@Override
            public void onSuccess(QBLocation qbLocation, Bundle bundle) {
				if (progDlg.isShowing()) {
					progDlg.dismiss();
				}
            }

            @Override
            public void onError(List<String> errors) {
            	if (progDlg.isShowing()) {
					progDlg.dismiss();
				}
            	DialogUtility.showGeneralAlert(parent, "", errors.get(0));
            }
		});
	}
	
	public void readPeoplesAround() {
		QBLocationRequestBuilder getLocationsBuilder = new QBLocationRequestBuilder();
		
		getLocationsBuilder.setLastOnly();
		getLocationsBuilder.setCurrentPosition(GlobalData.curLat, GlobalData.curLng);
		getLocationsBuilder.setRadius(GlobalData.curLat, GlobalData.curLng, 10);
				
		QBLocations.getLocations(getLocationsBuilder, new QBEntityCallbackImpl<ArrayList<QBLocation>>() {
		    @Override
		    public void onSuccess(ArrayList<QBLocation> locations, Bundle params) {
		    	if (progDlg.isShowing()) {
		    		progDlg.dismiss();
		    	}
		    	
		    	new QBUserService(parent).readUsersInf(locations);
		    }
		    
		    @Override
		    public void onError(List<String> errors) {
		    	if (progDlg.isShowing()) {
		    		progDlg.dismiss();
		    	}
		    }
		});
	}
}
