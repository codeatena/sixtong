package com.han.quickblox.module;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;

import com.han.chatapp.App;
import com.han.chatapp.GlobalData;
import com.han.chatapp.chat.ChatActivity;
import com.han.chatapp.main.MainActivity;
import com.han.chatapp.model.Message;
import com.han.utility.NotificationUtility;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.QBPrivateChat;
import com.quickblox.chat.QBPrivateChatManager;
import com.quickblox.chat.exception.QBChatException;
import com.quickblox.chat.listeners.QBMessageListener;
import com.quickblox.chat.listeners.QBPrivateChatManagerListener;
import com.quickblox.chat.model.QBChatMessage;
import com.quickblox.chat.model.QBDialog;
import com.quickblox.chat.model.QBDialogType;

public class QBPrivateChatService {
	
	QBMessageListener<QBPrivateChat> privateChatMessageListener = new QBMessageListener<QBPrivateChat>() {
	    @Override
	    public void processMessage(QBPrivateChat privateChat, final QBChatMessage chatMessage) {
	    	
	    	if (chatMessage.getBody().equals(""))
	    		return;
	    	
	    	QBDialog dialog = getDialogForPrivateChat(chatMessage);
	    	if (dialog == null) return;
	    	
	    	MainActivity.getInstatnce().addPrivateChatMessage(chatMessage);
	    	
			if (chatMessage.getSenderId().equals(ChatActivity.opponentID)) {
				ChatActivity.getInstatnce().addMessage(chatMessage);
				NotificationUtility.notification(chatMessage, dialog, true);
			} else {
				NotificationUtility.notification(chatMessage, dialog, false);
			}
	    }
	    
	    @Override
	    public void processError(QBPrivateChat privateChat, QBChatException error, QBChatMessage originMessage){
	    	
	    }
	    
	    @Override
	    public void processMessageDelivered(QBPrivateChat privateChat, String messageID){
	    	
	    }
	 
	    @Override
	    public void processMessageRead(QBPrivateChat privateChat, String messageID){
	    	
	    }
	};
	
	private QBDialog getDialogForPrivateChat(QBChatMessage chatMessage) {
		for (QBDialog dialog: GlobalData.myUser.arrDialogs) {
			if (dialog.getType().equals(QBDialogType.PRIVATE)) {
				int opponentID = QBDialogService.getOpponentIDForPrivateDialog(dialog);
				if (chatMessage.getSenderId().equals(opponentID)) {
					return dialog;
				}
			}
		}
		return null;
	}
	
	QBPrivateChatManagerListener privateChatManagerListener = new QBPrivateChatManagerListener() {
	    @Override
	    public void chatCreated(final QBPrivateChat privateChat, final boolean createdLocally) {
	        if(!createdLocally){
	            privateChat.addMessageListener(privateChatMessageListener);
	        }
	    }
	};
	
	public void init() {
		if (QBChatService.isInitialized()) {
			
			QBPrivateChatManager privateChatManager = QBChatService.getInstance().getPrivateChatManager();
			if (privateChatManager == null) {
				return;
			}
			
			QBChatService.getInstance().getPrivateChatManager().addPrivateChatManagerListener(privateChatManagerListener);
			addPrivateChats();	
		} else {
			QBChatService.init(App.getInstance());
		}
	}
	
	public void sendMessage(int opponentID, String body) {
		try {
			QBPrivateChatManager privateChatManager = QBChatService.getInstance().getPrivateChatManager();
			QBPrivateChat privateChat = privateChatManager.getChat(opponentID);
			
	        if (privateChat == null) {
	            privateChat = privateChatManager.createChat(opponentID, privateChatMessageListener);
	        }
	        
	        QBChatMessage chatMessage = new QBChatMessage();
	        
	        chatMessage.setBody(body);
	        chatMessage.setProperty(QBConst.PROPERTY_SAVE_TO_HISTORY, "1");
	        chatMessage.setProperty(QBConst.PROPERTY_NOTIFICATION_TYPE, "1");
	        
	        privateChat.sendMessage(chatMessage);
	        
	        Message.sendPrivateChatMessage(opponentID, body);
		} catch (XMPPException e) {
			e.printStackTrace();
		} catch (SmackException.NotConnectedException e) {
			e.printStackTrace();
		}
	}
	
	public void addPrivateChats() {
		QBPrivateChatManager privateChatManager = QBChatService.getInstance().getPrivateChatManager();
		if (privateChatManager == null) return;
		
		for (QBDialog dialog: GlobalData.myUser.arrDialogs) {
			if (dialog.getType().equals(QBDialogType.PRIVATE)) {
				int opponentID = QBDialogService.getOpponentIDForPrivateDialog(dialog);
				QBPrivateChat privateChat = privateChatManager.getChat(opponentID);
				
		        if (privateChat == null) {
		            privateChat = privateChatManager.createChat(opponentID, privateChatMessageListener);
		        }
		        
		        privateChat.addMessageListener(privateChatMessageListener);
			}
		}
	}
	
	public void removePrivateChats() {
		QBPrivateChatManager privateChatManager = QBChatService.getInstance().getPrivateChatManager();		
		for (QBDialog dialog: GlobalData.myUser.arrDialogs) {
			if (dialog.getType().equals(QBDialogType.PRIVATE)) {
				int opponentID = QBDialogService.getOpponentIDForPrivateDialog(dialog);
				QBPrivateChat privateChat = privateChatManager.getChat(opponentID);
				
		        if (privateChat == null) {
		            privateChat = privateChatManager.createChat(opponentID, privateChatMessageListener);
		        }
		        
		        privateChat.close();
		        privateChat.removeMessageListener(privateChatMessageListener);
			}
		}
	}
}
