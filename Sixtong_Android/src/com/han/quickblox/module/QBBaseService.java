package com.han.quickblox.module;

import com.han.utility.DialogUtility;

import android.app.Activity;
import android.app.ProgressDialog;

public class QBBaseService {

	protected Activity parent;
	protected ProgressDialog progDlg;
	protected boolean isShowProgDlg;
	
	public QBBaseService(Activity _parent) {
		parent = _parent;
		progDlg = DialogUtility.getProgressDialog(parent);
		isShowProgDlg = true;
		progDlg.show();
	}
	
	public QBBaseService(Activity _parent, String title) {
		parent = _parent;
		progDlg = DialogUtility.getProgressDialog(parent, title);
		isShowProgDlg = true;
		progDlg.show();
	}
	
	public QBBaseService(Activity _parent, boolean _isShowProgDlg) {
		parent = _parent;
		progDlg = DialogUtility.getProgressDialog(parent);
		isShowProgDlg = _isShowProgDlg;
		if (isShowProgDlg)
			progDlg.show();
	}
	
	public void dismissProgDlg() {
		if (progDlg.isShowing()) {
			progDlg.dismiss();
		}
	}
}