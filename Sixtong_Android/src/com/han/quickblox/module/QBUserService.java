package com.han.quickblox.module;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import com.han.chatapp.GlobalData;
import com.han.chatapp.main.MainActivity;
import com.han.chatapp.model.User;
import com.han.chatapp.user.MyProfileActivity;
import com.han.chatapp.user.PeopleAroundActivity;
import com.han.utility.DialogUtility;
import com.han.utility.GeneralUtility;
import com.han.utility.LocationUtility;
import com.han.utility.PreferenceUtility;
import com.quickblox.core.QBEntityCallbackImpl;
import com.quickblox.core.request.QBCustomObjectRequestBuilder;
import com.quickblox.customobjects.QBCustomObjects;
import com.quickblox.customobjects.model.QBCustomObject;
import com.quickblox.location.model.QBLocation;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;

public class QBUserService extends QBBaseService {
	
	public QBUserService(Activity parent) {
		super(parent);
	}
	
	public QBUserService(Activity parent, String title) {
		super(parent, title);
	}
	
	public QBUserService(Activity parent, boolean isShowProgDlg) {
		super(parent, isShowProgDlg);
	}
	
	public void sendInvite(User inviter) {
//		readInviterBeforeInvite(inviter_id);		
		if (inviter.isMyInviter()) {
			return;
		}
		if (inviter.isMyFriend()) {
			return;
		}
		
		int nIndex = -1;
		
		nIndex = GeneralUtility.getIndexStringInArray(Integer.toString(GlobalData.myUser.qbUser.getId()), inviter.arrFriendIDs);
		if (nIndex > -1) {
			inviter.arrFriendIDs.remove(nIndex);
		}
		nIndex = GeneralUtility.getIndexStringInArray(Integer.toString(GlobalData.myUser.qbUser.getId()), inviter.arrInviterIDs);
		if (nIndex == -1) {
			inviter.arrInviterIDs.add(Integer.toString(GlobalData.myUser.qbUser.getId()));
		}
		
		updateUserInf(inviter.qbCustomObjectID, inviter.arrFriendIDs, inviter.arrInviterIDs);
		MainActivity.getInstatnce().updateContacts();
	}
	
	public void acceptInvite(User friend) {
		acceptMe(friend);
		acceptFriend(friend);
		MainActivity.getInstatnce().updateContacts();
		GlobalData.qbPrivateChatService.sendMessage(friend.qbUser.getId(), "");
	}
	
	public void acceptMe(User friend) {
		
		int friend_id = friend.qbUser.getId();
		int nIndex = -1;
		
		nIndex = GeneralUtility.getIndexStringInArray(Integer.toString(friend_id), GlobalData.myUser.arrInviterIDs);
		if (nIndex > -1) {
			GlobalData.myUser.arrInviterIDs.remove(nIndex);
		}
		nIndex = GeneralUtility.getIndexStringInArray(Integer.toString(friend_id), GlobalData.myUser.arrFriendIDs);
		if (nIndex == -1) {
			GlobalData.myUser.arrFriendIDs.add(Integer.toString(friend_id));
		}
		
		updateUserInf(GlobalData.myUser.qbCustomObjectID, GlobalData.myUser.arrFriendIDs, GlobalData.myUser.arrInviterIDs); 
	}
	
	public void acceptFriend(User friend) {

		int nIndex = -1;
		
		nIndex = GeneralUtility.getIndexStringInArray(Integer.toString(GlobalData.myUser.qbUser.getId()), friend.arrInviterIDs);
		if (nIndex > -1) {
			friend.arrInviterIDs.remove(nIndex);
		}
		nIndex = GeneralUtility.getIndexStringInArray(Integer.toString(GlobalData.myUser.qbUser.getId()), friend.arrFriendIDs);
		if (nIndex == -1) {
			friend.arrFriendIDs.add(Integer.toString(GlobalData.myUser.qbUser.getId()));
		}
		
		updateUserInf(friend.qbCustomObjectID, friend.arrFriendIDs, friend.arrInviterIDs);
	}
	
	public void block(User user) {
		blockMe(user);
//		blockFriend(user);
		MainActivity.getInstatnce().updateContacts();
	}
	
	public void blockMe(User user) {
		for (String qbUserID: GlobalData.myUser.arrFriendIDs) {
			if (qbUserID.equals(Integer.toString(user.qbUser.getId()))) {
				GlobalData.myUser.arrFriendIDs.remove(qbUserID);				
				break;
			}
		}
		updateUserInf(GlobalData.myUser.qbCustomObjectID, GlobalData.myUser.arrFriendIDs, GlobalData.myUser.arrInviterIDs);
	}
	
	public void blockFriend(User user) {
		if (user == null) return;
		for (String qbUserID: user.arrFriendIDs) {
			if (qbUserID.equals(Integer.toString(GlobalData.myUser.qbUser.getId()))) {
				user.arrFriendIDs.remove(qbUserID);
				break;
			}
		}
		updateUserInf(user.qbCustomObjectID, user.arrFriendIDs, user.arrInviterIDs);
	}

	public void updateUserInf(String objectID, ArrayList<String> arrFriendIDs, ArrayList<String> arrInviteIDs) {
		QBCustomObject co = new QBCustomObject("Contacts");
		HashMap<String, Object> fields = new HashMap<String, Object>();
		fields.put("friend_ids", arrFriendIDs);
		fields.put("invite_ids", arrInviteIDs);
		co.setFields(fields);
		co.setCustomObjectId(objectID);
		
		QBCustomObjects.updateObject(co, new QBEntityCallbackImpl<QBCustomObject>() {
			@Override
		    public void onSuccess(QBCustomObject arg0, Bundle bundle) {
				if (progDlg.isShowing()) {
					progDlg.dismiss();
				}
			}
			
			@Override
			public void onError(List<String> errors) {
				if (progDlg.isShowing()) {
					progDlg.dismiss();
				}
				DialogUtility.showGeneralAlert(parent, "Error", errors.get(0));
			}
		});
	}
	
	public void updateMyFullName(String fullName) {

		GlobalData.myUser.qbUser.setFullName(fullName);
		String curPassword = PreferenceUtility.sharedPreferences.getString(PreferenceUtility.PREFERENCE_KEY_CUR_USER_PASSWORD, "");
		
		GlobalData.myUser.qbUser.setPassword(curPassword);
		GlobalData.myUser.qbUser.setOldPassword(curPassword);
		
        QBUsers.updateUser(GlobalData.myUser.qbUser, new QBEntityCallbackImpl<QBUser>() {
            @Override
            public void onSuccess(QBUser qbUser, Bundle bundle) {
            	GlobalData.myUser.qbUser = qbUser;
            	
            	if (progDlg.isShowing())
            		progDlg.dismiss();
            	parent.finish();           	
            }

            @Override
            public void onError(List<String> errors) {
            	if (progDlg.isShowing())
            		progDlg.dismiss();
            	DialogUtility.showGeneralAlert(parent, "Error", errors.get(0));
            }
        });
	}
	
	public void updateMyWords(String words) {
		
		QBCustomObject co = new QBCustomObject("Contacts");
		HashMap<String, Object> fields = new HashMap<String, Object>();
		fields.put("words", words);
		co.setFields(fields);
		co.setCustomObjectId(GlobalData.myUser.qbCustomObjectID);
		
		QBCustomObjects.updateObject(co, new QBEntityCallbackImpl<QBCustomObject>() {
			@Override
		    public void onSuccess(QBCustomObject qbCustomObject, Bundle bundle) {
				
				if (progDlg.isShowing()) {
					progDlg.dismiss();
				}
            	HashMap<String, Object> fields = qbCustomObject.getFields();
            	for (String key: fields.keySet()) {
            		if (key.equals("words")) {
            			String value = (String) fields.get(key);
            			if (value != null) {
            				GlobalData.myUser.words = value;
            			}
            		}
            	}
            	parent.finish();
			}
			
			@Override
			public void onError(List<String> errors) {
				if (progDlg.isShowing()) {
					progDlg.dismiss();
				}
				DialogUtility.showGeneralAlert(parent, "Error", errors.get(0));
			}
		});
	}
	
	public void updateMyGender(String gender) {
		QBCustomObject co = new QBCustomObject("Contacts");
		HashMap<String, Object> fields = new HashMap<String, Object>();
		fields.put("gender", gender);
		co.setFields(fields);
		co.setCustomObjectId(GlobalData.myUser.qbCustomObjectID);
		
		QBCustomObjects.updateObject(co, new QBEntityCallbackImpl<QBCustomObject>() {
			@Override
		    public void onSuccess(QBCustomObject qbCustomObject, Bundle bundle) {
				if (progDlg.isShowing()) {
					progDlg.dismiss();
				}
            	HashMap<String, Object> fields = qbCustomObject.getFields();
            	for (String key: fields.keySet()) {
            		if (key.equals("gender")) {
            			String value = (String) fields.get(key);
            			if (value != null) {
            				GlobalData.myUser.gender = value;
            				MyProfileActivity myProfileActivity = (MyProfileActivity) parent;
            				myProfileActivity.initValue();
            			}
            		}
            	}
			}
			
			@Override
			public void onError(List<String> errors) {
				if (progDlg.isShowing()) {
					progDlg.dismiss();
				}
				DialogUtility.showGeneralAlert(parent, "Error", errors.get(0));
			}
		});
	}
	
	public void updateMyPhoto(String strBmp) {
		QBCustomObject co = new QBCustomObject("Contacts");
		HashMap<String, Object> fields = new HashMap<String, Object>();
		fields.put("photo", strBmp);
		co.setFields(fields);
		co.setCustomObjectId(GlobalData.myUser.qbCustomObjectID);
		
		QBCustomObjects.updateObject(co, new QBEntityCallbackImpl<QBCustomObject>() {
			@Override
		    public void onSuccess(QBCustomObject qbCustomObject, Bundle bundle) {
				if (progDlg.isShowing()) {
					progDlg.dismiss();
				}
            	HashMap<String, Object> fields = qbCustomObject.getFields();
            	for (String key: fields.keySet()) {
            		if (key.equals("photo")) {
            			String value = (String) fields.get(key);
            			if (value != null) {
            				GlobalData.myUser.strPhoto = value;
            				MyProfileActivity myProfileActivity = (MyProfileActivity) parent;
            				myProfileActivity.initValue();
            			}
            		}
            	}
			}
			
			@Override
			public void onError(List<String> errors) {
				if (progDlg.isShowing()) {
					progDlg.dismiss();
				}
				DialogUtility.showGeneralAlert(parent, "Error", errors.get(0));
			}
		});
	}
	
	public void updateMyRegion(String region) {
		QBCustomObject co = new QBCustomObject("Contacts");
		HashMap<String, Object> fields = new HashMap<String, Object>();
		fields.put("where", region);
		co.setFields(fields);
		co.setCustomObjectId(GlobalData.myUser.qbCustomObjectID);
		
		QBCustomObjects.updateObject(co, new QBEntityCallbackImpl<QBCustomObject>() {
			@Override
		    public void onSuccess(QBCustomObject qbCustomObject, Bundle bundle) {
				if (progDlg.isShowing()) {
					progDlg.dismiss();
				}
            	HashMap<String, Object> fields = qbCustomObject.getFields();
            	for (String key: fields.keySet()) {
            		if (key.equals("where")) {
            			String value = (String) fields.get(key);
            			if (value != null) {
            				GlobalData.myUser.where = value;
            				parent.setResult(Activity.RESULT_OK);
            				parent.finish();
            			}
            		}
            	}
			}
			
			@Override
			public void onError(List<String> errors) {
				if (progDlg.isShowing()) {
					progDlg.dismiss();
				}
				DialogUtility.showGeneralAlert(parent, "Error", errors.get(0));
			}
		});
	}
	
	public void readInviterBeforeInvite(int inviter_id) {
		QBCustomObjectRequestBuilder requestBuilder = new QBCustomObjectRequestBuilder();
		requestBuilder.eq("user_id", inviter_id);
		
		QBCustomObjects.getObjects("Contacts", requestBuilder, new QBEntityCallbackImpl<ArrayList<QBCustomObject>>() {
			@SuppressWarnings("unchecked")
			@Override
		    public void onSuccess(ArrayList<QBCustomObject> arg0, Bundle bundle) {
				QBCustomObject tmpObject = arg0.get(0);
            	HashMap<String, Object> fields = tmpObject.getFields();
            	
            	ArrayList<String> arrInviteIDs = new ArrayList<String>();
            	ArrayList<String> arrFriendIDs = new ArrayList<String>();
            	
            	Log.e("QBCustomObjects id", tmpObject.getCustomObjectId());
            	for (String key: fields.keySet()) {
            		if (key.equals("invite_ids")) {
                    	arrInviteIDs = (ArrayList<String>) fields.get(key);
                    	if (arrInviteIDs == null) arrInviteIDs = new ArrayList<String>();
                    	int index = GeneralUtility.getIndexStringInArray(Integer.toString(GlobalData.myUser.qbUser.getId()), arrInviteIDs);
                    	if (index == -1) {
                    		arrInviteIDs.add(Integer.toString(GlobalData.myUser.qbUser.getId()));
                    	}
            		}
            		if (key.equals("friend_ids")) {
            			arrFriendIDs = (ArrayList<String>) fields.get(key);
                    	if (arrFriendIDs == null) arrFriendIDs = new ArrayList<String>();
            		}
            	}
            	
            	updateUserInf(tmpObject.getCustomObjectId(), arrFriendIDs, arrInviteIDs);
			}
			
			@Override
			public void onError(List<String> errors) {
				if (progDlg.isShowing()) {
					progDlg.dismiss();
				}
				DialogUtility.showGeneralAlert(parent, "Error", errors.get(0));
			}
		});
	}
	
	public void readUsersInf(final ArrayList<QBLocation> arrQBLocation) {
		QBCustomObjectRequestBuilder requestBuilder = new QBCustomObjectRequestBuilder();
		
		ArrayList<Integer> arrUserQBUserIDs = new ArrayList<Integer>();
		for (QBLocation qbLocation: arrQBLocation) {
			if (!qbLocation.getUserId().equals(GlobalData.myUser.qbUser.getId())) {
				arrUserQBUserIDs.add(qbLocation.getUserId());
			}
		}
		
		requestBuilder.in("user_id", arrUserQBUserIDs);
	
		QBCustomObjects.getObjects("Contacts", requestBuilder, new QBEntityCallbackImpl<ArrayList<QBCustomObject>>() {
			
			@SuppressWarnings("unchecked")
			@Override
		    public void onSuccess(ArrayList<QBCustomObject> arrQBCustomObject, Bundle bundle) {
				
				if (progDlg.isShowing()) {
					progDlg.dismiss();
				}
				
				ArrayList<User> arrUser = new ArrayList<User>();
				
				for (QBCustomObject qbCustomObject: arrQBCustomObject) {
					User user = new User();
					int qbUserID = qbCustomObject.getUserId();
					for (QBLocation qbLocation: arrQBLocation) {
						if (qbUserID == qbLocation.getUserId()) {
							user.qbUser = qbLocation.getUser();
							user.distance = LocationUtility.getDistance(GlobalData.curLat, GlobalData.curLng,
									qbLocation.getLatitude(), qbLocation.getLongitude());
							break;
						}
					}
					
					user.qbCustomObjectID = qbCustomObject.getCustomObjectId();
	            	HashMap<String, Object> fields = qbCustomObject.getFields();
	            	
	            	for (String key: fields.keySet()) {
	            		if (key.equals("secret_id")) {
	            			String value = (String) fields.get(key);
	            			if (value != null) {
	            				user.secretID = value;
	            			}
	            		}
	            		if (key.equals("photo")) {
	            			String value = (String) fields.get(key);
	            			if (value != null) {
	            				user.strPhoto = value;	
	            			}
	            		}
	            		if (key.equals("gender")) {
	            			String value = (String) fields.get(key);
	            			if (value != null) {
	            				user.gender = value;
	            			}
	            		}
	            		if (key.equals("where")) {
	            			String value = (String) fields.get(key);
	            			if (value != null) {
	            				user.where = value;
	            			}
	            		}
	            		if (key.equals("words")) {
	            			String value = (String) fields.get(key);
	            			if (value != null) {
	            				user.words = value;
	            			}
	            		}
	            		if (key.equals("friend_ids")) {
							ArrayList<String> value = (ArrayList<String>) fields.get(key);
	            			if (value != null) {
	            				user.arrFriendIDs = value;
	            			} else {
	            				user.arrFriendIDs = new ArrayList<String>();
	            			}
	            		}
	            		if (key.equals("invite_ids")) {
							ArrayList<String> value = (ArrayList<String>) fields.get(key);
	            			if (value != null) {
	            				user.arrInviterIDs = value;
	            			} else {
	            				user.arrInviterIDs = new ArrayList<String>();
	            			}
	            		}
	            	}
	            	arrUser.add(user);
				}
				
				if (parent instanceof PeopleAroundActivity) {
					PeopleAroundActivity peopleAroundActivity = (PeopleAroundActivity) parent;
					peopleAroundActivity.successGetAroundPeoples(arrUser);
				}
			}
			
			@Override
			public void onError(List<String> errors) {
				if (progDlg.isShowing()) {
					progDlg.dismiss();
				}
				DialogUtility.showGeneralAlert(parent, "Error", errors.get(0));
			}
		});
	}
}