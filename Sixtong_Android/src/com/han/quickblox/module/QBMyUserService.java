package com.han.quickblox.module;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.jivesoftware.smack.SmackException;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.han.chatapp.App;
import com.han.chatapp.GlobalData;
import com.han.chatapp.SearchActivity;
import com.han.chatapp.StartActivity;
import com.han.chatapp.main.MainActivity;
import com.han.chatapp.model.User;
import com.han.chatapp.settings.SettingsActivity;
import com.han.chatapp.user.VerifyActivity;
import com.han.contact.ContactFetcher;
import com.han.contact.MobileContactsActivity;
import com.han.contact.model.Contact;
import com.han.utility.DialogUtility;
import com.han.utility.PreferenceUtility;
import com.quickblox.chat.QBChatService;
import com.quickblox.core.QBEntityCallbackImpl;
import com.quickblox.core.request.QBCustomObjectRequestBuilder;
import com.quickblox.core.request.QBPagedRequestBuilder;
import com.quickblox.customobjects.QBCustomObjects;
import com.quickblox.customobjects.model.QBCustomObject;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;
import com.han.chatapp.user.LoginActivity;

public class QBMyUserService extends QBBaseService {

	public static final int AUTO_PRESENCE_INTERVAL_IN_SECONDS = 30;

	public QBMyUserService(Activity parent) {
		super(parent);
	}
	
	public QBMyUserService(Activity parent, String title) {
		super(parent, title);
	}
	
	public QBMyUserService(Activity parent, boolean isShowProgDlg) {
		super(parent, isShowProgDlg);
	}
	
	public void login(final String username, final String password) {
		QBUser qbUser = new QBUser(username, password);
		
        QBUsers.signIn(qbUser, new QBEntityCallbackImpl<QBUser>() {
            @Override
            public void onSuccess(QBUser qbUser, Bundle bundle) {
            	
            	Log.e("login result", "success");
            	
            	GlobalData.init();
                GlobalData.myUser.qbUser = qbUser;
                
                PreferenceUtility.prefEditor.putString(PreferenceUtility.PREFERENCE_KEY_CUR_USER_NAME, username);
                PreferenceUtility.prefEditor.putString(PreferenceUtility.PREFERENCE_KEY_CUR_USER_PASSWORD, password);
                PreferenceUtility.prefEditor.putString(PreferenceUtility.PREFERENCE_KEY_USER_STATUS, PreferenceUtility.USER_STATUS_LOGIN);
                PreferenceUtility.prefEditor.commit();
                
            	if (parent instanceof StartActivity) {
            		
            	} else {
            		if (StartActivity.instance != null)
            			StartActivity.instance.finish();
            	}
            	
                readMyMoreInfo();

            	dismissProgDlg();

//            	parent.startActivity(new Intent(parent, MainActivity.class));
//            	parent.finish();
            	
                loginToChat();
            }

            @Override
            public void onError(List<String> errors) {
            	
            	dismissProgDlg();
            	
            	if (parent instanceof LoginActivity) {
            		LoginActivity loginActivity = (LoginActivity) parent;
            		loginActivity.failedLogin(errors.get(0));
            	} else {
            		DialogUtility.showGeneralAlert(parent, "Error", errors.get(0));	
            	}
            }
        });
	}
	
	public void readMyMoreInfo() {
		QBCustomObjectRequestBuilder requestBuilder = new QBCustomObjectRequestBuilder();
		requestBuilder.eq("user_id", GlobalData.myUser.qbUser.getId());
		
		QBCustomObjects.getObjects("Contacts", requestBuilder, new QBEntityCallbackImpl<ArrayList<QBCustomObject>>() {
			@SuppressWarnings("unchecked")
			@Override
		    public void onSuccess(ArrayList<QBCustomObject> arg0, Bundle bundle) {
				Log.e("my contact count", Integer.toString(arg0.size()));
				QBCustomObject tmpObject = arg0.get(0);
            	GlobalData.myUser.qbCustomObjectID = tmpObject.getCustomObjectId();
            	
            	HashMap<String, Object> fields = tmpObject.getFields();
            	for (String key: fields.keySet()) {
            		if (key.equals("friend_ids")) {
            			ArrayList<String> value = (ArrayList<String>) fields.get(key);
            			if (value != null) {
            				GlobalData.myUser.arrFriendIDs = (ArrayList<String>) fields.get(key);
            				
            			} else {
            				GlobalData.myUser.arrFriendIDs = new ArrayList<String>();
            			}
            		}
            		if (key.equals("invite_ids")) {
            			ArrayList<String> value = (ArrayList<String>) fields.get(key);
            			if (value != null) {
            				GlobalData.myUser.arrInviterIDs = value;	
            			} else {
            				GlobalData.myUser.arrInviterIDs = new ArrayList<String>();
            			}
            		}
            		if (key.equals("secret_id")) {
            			String value = (String) fields.get(key);
            			if (value != null) {
            				GlobalData.myUser.secretID = value;
            			} else {
            				GlobalData.myUser.secretID = "";
            			}
            		}
            		if (key.equals("photo")) {
            			String value = (String) fields.get(key);
            			GlobalData.myUser.strPhoto = value;
            		}
            		if (key.equals("gender")) {
            			String value = (String) fields.get(key);
            			if (value != null) {
            				GlobalData.myUser.gender = value;
            			} else {
            				GlobalData.myUser.gender = "Male";
            			}
            		}
            		if (key.equals("where")) {
            			String value = (String) fields.get(key);
            			if (value != null) {
            				GlobalData.myUser.where = value;
            			} else {
            				GlobalData.myUser.where = "";
            			}
            		}
            		if (key.equals("words")) {
            			String value = (String) fields.get(key);
            			if (value != null) {
            				GlobalData.myUser.words = value;
            			} else {
            				GlobalData.myUser.words = "";
            			}
            		}
            	}

            	parent.startActivity(new Intent(parent, MainActivity.class));
            	parent.finish();
			}
			
			@Override
			public void onError(List<String> errors) {
				// TODO Auto-generated method stub
				if (progDlg.isShowing()) {
            		progDlg.dismiss();	
            	}
				DialogUtility.showGeneralAlert(parent, "Error", errors.get(0));
			}
		});
	}
	
	public void readContactUsers() {
    	
    	QBPagedRequestBuilder pagedRequestBuilder = new QBPagedRequestBuilder();
    	pagedRequestBuilder = null;
    	
    	ArrayList<String> phones = new ArrayList<String>();
    	ArrayList<Contact> arrContacts = new ContactFetcher(parent).fetchAll();
    	
    	GlobalData.arrContactUsers = new ArrayList<User>();
        
    	for (Contact contact: arrContacts) {
    		phones.add(contact.numbers.get(0).number);
    		User user = new User();
    		user.contact = contact;
    		GlobalData.arrContactUsers.add(user);
    	}
    	
    	QBUsers.getUsersByPhoneNumbers(phones, pagedRequestBuilder, new QBEntityCallbackImpl<ArrayList<QBUser>>() {
    		@Override
            public void onSuccess(ArrayList<QBUser> qbUsers, Bundle bundle) {              
                for (QBUser qbUser: qbUsers) {
                	for (User user: GlobalData.arrContactUsers) {
                		if (qbUser.getPhone().equals(user.contact.numbers.get(0).number)) {
                			user.qbUser = qbUser;
                		}
                	}
                }
                
                readContactUsersMoreInf(qbUsers);
    		}
    		
            @Override
            public void onError(List<String> errors) {
            	if (progDlg.isShowing()) {
            		progDlg.dismiss();
            	}
            	DialogUtility.showGeneralAlert(parent, "Error", errors.get(0));
            }
    	});
    }
	
	public void readContactUsersMoreInf(final ArrayList<QBUser> arrQBUser) {
		QBCustomObjectRequestBuilder requestBuilder = new QBCustomObjectRequestBuilder();
		
		ArrayList<Integer> arrUserIDs = new ArrayList<Integer>();
		for (QBUser qbUser: arrQBUser) {
			arrUserIDs.add(qbUser.getId());
		}
		
		requestBuilder.in("user_id", arrUserIDs);
	
		QBCustomObjects.getObjects("Contacts", requestBuilder, new QBEntityCallbackImpl<ArrayList<QBCustomObject>>() {
			@Override
		    public void onSuccess(ArrayList<QBCustomObject> arrQBCustomObject, Bundle bundle) {
				
				if (progDlg.isShowing()) {
					progDlg.dismiss();
				}
				
				for (QBCustomObject qbCustomObject: arrQBCustomObject) {
                	for (User user: GlobalData.arrContactUsers) {
                		if (user.qbUser == null) continue;
                		
                		if (qbCustomObject.getUserId().equals(user.qbUser.getId())) {
                			user.qbCustomObjectID = qbCustomObject.getCustomObjectId();
        	            	HashMap<String, Object> fields = qbCustomObject.getFields();
                		
			            	for (String key: fields.keySet()) {
			            		if (key.equals("secret_id")) {
			            			String value = (String) fields.get(key);
			            			if (value != null) {
			            				user.secretID = value;
			            			}
			            		}
			            		if (key.equals("photo")) {
			            			String value = (String) fields.get(key);
			            			if (value != null) {
			            				user.strPhoto = value;
			            			}
			            		}
			            		if (key.equals("gender")) {
			            			String value = (String) fields.get(key);
			            			if (value != null) {
			            				user.gender = value;
			            			}
			            		}
			            		if (key.equals("where")) {
			            			String value = (String) fields.get(key);
			            			if (value != null) {
			            				user.where = value;
			            			}
			            		}
			            		if (key.equals("words")) {
			            			String value = (String) fields.get(key);
			            			if (value != null) {
			            				user.words = value;
			            			}
			            		}
			            	}
                		}
                	}
				}
				
				MobileContactsActivity resultActivity = (MobileContactsActivity) parent;
				resultActivity.makeList();
			}
			
			@Override
			public void onError(List<String> errors) {
				if (progDlg.isShowing()) {
					progDlg.dismiss();
				}
				DialogUtility.showGeneralAlert(parent, "Error", errors.get(0));
			}
		});
	}
	
	@SuppressWarnings("rawtypes")
	public void loginToChat(){
		
   		String curUserName = PreferenceUtility.sharedPreferences.getString(PreferenceUtility.PREFERENCE_KEY_CUR_USER_NAME, "");
		String curPassword = PreferenceUtility.sharedPreferences.getString(PreferenceUtility.PREFERENCE_KEY_CUR_USER_PASSWORD, "");
        
		if (!QBChatService.isInitialized()) {
			QBChatService.init(App.getInstance());
		}
		
        GlobalData.myUser.qbUser.setLogin(curUserName);
        GlobalData.myUser.qbUser.setPassword(curPassword);

        QBChatService.getInstance().login(GlobalData.myUser.qbUser, new QBEntityCallbackImpl() {
            @Override
            public void onSuccess() {

                try {


                	QBChatService.getInstance().startAutoSendPresence(AUTO_PRESENCE_INTERVAL_IN_SECONDS);
                	

                    
                } catch (SmackException.NotLoggedInException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(List errors) {
//                if (progDlg.isShowing()) {
//            		progDlg.dismiss();	
//            	}
//                DialogUtility.showGeneralAlert(parent, "Error", errors.get(0).toString());
            }
        });
    }
	
	public void createQBUser() {
		
		final String fullName = PreferenceUtility.sharedPreferences.getString(PreferenceUtility.PREFERENCE_KEY_USER_FULL_NAME, "");
//		String countryCode = PreferenceUtility.sharedPreferences.getString(PreferenceUtility.PREFERENCE_KEY_USER_COUNTRY_CODE, "");
		final String phoneNumber = PreferenceUtility.sharedPreferences.getString(PreferenceUtility.PREFERENCE_KEY_USER_PHONE_NUMBER, "");
		final String password = PreferenceUtility.sharedPreferences.getString(PreferenceUtility.PREFERENCE_KEY_USER_PASSWORD, "");
		final String secretID = PreferenceUtility.sharedPreferences.getString(PreferenceUtility.PREFERENCE_KEY_USER_SECRET_ID, "");
		
		QBUser qbUser = new QBUser();
        qbUser.setLogin(QBConst.FORWARD_LOGIN_STRING + secretID);
        qbUser.setPassword(password);
        qbUser.setFullName(fullName);
        qbUser.setPhone(phoneNumber);
        //qbUser.setExternalId(secretID);
        
        QBUsers.signUpSignInTask(qbUser, new QBEntityCallbackImpl<QBUser>() {
        	@Override
        	public void onSuccess(QBUser qbUser, Bundle bundle) {

        		createQBUserCustom();
        	}
            @Override
            public void onError(List<String> errors) {
                if (progDlg.isShowing()) {
            		progDlg.dismiss();	
            	}
                DialogUtility.show(parent, errors.get(0));
            }
        });
	}
	
	private void createQBUserCustom() {
		HashMap<String, Object> fields = new HashMap<String, Object>();
        
		String secretID = PreferenceUtility.sharedPreferences.getString(PreferenceUtility.PREFERENCE_KEY_USER_SECRET_ID, "");

        fields.put("secret_id", secretID);
        fields.put("gender", "Male");
        fields.put("where", "");
        fields.put("words", "");

        String strPhoto = PreferenceUtility.sharedPreferences.getString(PreferenceUtility.PREFERENCE_KEY_USER_PHOTO, "");
		if (!strPhoto.equals("")) {
			fields.put("photo", strPhoto);
		}
        
        QBCustomObject qbCustomObject = new QBCustomObject();
        qbCustomObject.setClassName("Contacts");
        qbCustomObject.setFields(fields);

        QBCustomObjects.createObject(qbCustomObject, new QBEntityCallbackImpl<QBCustomObject>() {
            @Override
            public void onSuccess(QBCustomObject qbCustomObject, Bundle bundle) {
//            	DialogUtility.show(parent, "New user created successfully.");
        		String secretID = PreferenceUtility.sharedPreferences.getString(PreferenceUtility.PREFERENCE_KEY_USER_SECRET_ID, "");
        		String password = PreferenceUtility.sharedPreferences.getString(PreferenceUtility.PREFERENCE_KEY_USER_PASSWORD, "");
        		
                PreferenceUtility.prefEditor.putString(PreferenceUtility.PREFERENCE_KEY_CUR_USER_NAME, QBConst.FORWARD_LOGIN_STRING + secretID);
                PreferenceUtility.prefEditor.putString(PreferenceUtility.PREFERENCE_KEY_CUR_USER_PASSWORD, password);
                PreferenceUtility.prefEditor.putString(PreferenceUtility.PREFERENCE_KEY_USER_STATUS, PreferenceUtility.USER_STATUS_VERIFIED);
                PreferenceUtility.prefEditor.commit();
                
                if (progDlg.isShowing()) {
            		progDlg.dismiss();
            	}
                
            	VerifyActivity verifyActivity = (VerifyActivity) parent;
            	verifyActivity.successCreateQBUser();
            }
            
            @Override
            public void onError(List<String> errors) {
                if (progDlg.isShowing()) {
            		progDlg.dismiss();
            	}
                DialogUtility.show(parent, errors.get(0));
            }
        });
	}
	
	public void signout() {
//		logoutChat();
//		GlobalData.qbPrivateChatService.removePrivateChats();
//		GlobalData.qbGroupChatService.removeGroupChats();
//		
//		GlobalData.qbPrivateChatService = null;
//		GlobalData.qbGroupChatService = null;
		
    	logoutUser();
	}
	
	@SuppressWarnings("rawtypes")
	public void logoutChat() {
		if(!QBChatService.getInstance().isLoggedIn()){
            if (progDlg.isShowing()) {
        		progDlg.dismiss();
        	}
		    return;
		}
		
		QBChatService.getInstance().logout(new QBEntityCallbackImpl() {
            @Override
            public void onSuccess() {
            	Log.e("chat", "logout");
            	
				PreferenceUtility.prefEditor.putString(PreferenceUtility.PREFERENCE_KEY_USER_STATUS, PreferenceUtility.USER_STATUS_VERIFIED);
				PreferenceUtility.prefEditor.commit();
				
                if (progDlg.isShowing()) {
            		progDlg.dismiss();
            	}
//            	logoutUser();
                
                QBChatService.getInstance().destroy();
                
				parent.setResult(SettingsActivity.RESULT_CODE_SIGN_OUT);
				parent.finish();
            }

            @Override
            public void onError(List errors) {
                if (progDlg.isShowing()) {
            		progDlg.dismiss();
            	}

            	DialogUtility.showGeneralAlert(parent, "Error", errors.get(0).toString());
            }
        });
	}
	
	@SuppressWarnings("rawtypes")
	public void logoutUser() {
		QBUsers.signOut(new QBEntityCallbackImpl() {
            @Override
            public void onSuccess() {
            	
                logoutChat();
            }
            
            @Override
            public void onError(List errors) {
                if (progDlg.isShowing()) {
            		progDlg.dismiss();
            	}
				DialogUtility.showGeneralAlert(parent, "Error", errors.get(0).toString());
            }
        });
	}
	
	public void readAllUsers() {
    	QBPagedRequestBuilder requestBuilder = new QBPagedRequestBuilder();
    	requestBuilder = null;
    	
		QBUsers.getUsers(requestBuilder, new QBEntityCallbackImpl<ArrayList<QBUser>>() {
    		@Override
            public void onSuccess(ArrayList<QBUser> qbUsers, Bundle bundle) {
    			readAllUsersMoreInf(qbUsers);
            }
    		
            @Override
            public void onError(List<String> errors) {
            	if (progDlg.isShowing()) {
            		progDlg.dismiss();
            	}
            	DialogUtility.showGeneralAlert(parent, "Error", errors.get(0));
            }
		});
	}
	
	public void readAllUsersMoreInf(final ArrayList<QBUser> arrQBUser) {
		QBCustomObjectRequestBuilder requestBuilder = new QBCustomObjectRequestBuilder();
		
		ArrayList<Integer> arrUserIDs = new ArrayList<Integer>();
		for (QBUser qbUser: arrQBUser) {
			if (!qbUser.getId().equals(GlobalData.myUser.qbUser.getId())) {
				arrUserIDs.add(qbUser.getId());	
			}
		}
		
		requestBuilder.in("user_id", arrUserIDs);
	
		QBCustomObjects.getObjects("Contacts", requestBuilder, new QBEntityCallbackImpl<ArrayList<QBCustomObject>>() {
			@SuppressWarnings("unchecked")
			@Override
		    public void onSuccess(ArrayList<QBCustomObject> arrQBCustomObject, Bundle bundle) {
				
            	if (progDlg.isShowing()) {
            		progDlg.dismiss();
            	}
            	
				ArrayList<User> arrUsers = new ArrayList<User>();
				for (QBCustomObject qbCustomObject: arrQBCustomObject) {
					
					User user = new User();
					int qbUserID = qbCustomObject.getUserId();
					for (QBUser qbUser: arrQBUser) {
						if (qbUserID == qbUser.getId()) {
							user.qbUser = qbUser;
							break;
						}
					}
					
					user.qbCustomObjectID = qbCustomObject.getCustomObjectId();
	            	HashMap<String, Object> fields = qbCustomObject.getFields();
	            	
	            	for (String key: fields.keySet()) {
	            		if (key.equals("secret_id")) {
	            			String value = (String) fields.get(key);
	            			if (value != null) {
	            				user.secretID = value;
	            			}
	            		}
	            		if (key.equals("photo")) {
	            			String value = (String) fields.get(key);
	            			if (value != null) {
	            				user.strPhoto = value;
	            			}
	            		}
	            		if (key.equals("gender")) {
	            			String value = (String) fields.get(key);
	            			if (value != null) {
	            				user.gender = value;
	            			}
	            		}
	            		if (key.equals("where")) {
	            			String value = (String) fields.get(key);
	            			if (value != null) {
	            				user.where = value;
	            			}
	            		}
	            		if (key.equals("words")) {
	            			String value = (String) fields.get(key);
	            			if (value != null) {
	            				user.words = value;
	            			}
	            		}
	            		if (key.equals("friend_ids")) {
							ArrayList<String> value = (ArrayList<String>) fields.get(key);
	            			if (value != null) {
	            				user.arrFriendIDs = value;
	            			} else {
	            				user.arrFriendIDs = new ArrayList<String>();
	            			}
	            		}
	            		if (key.equals("invite_ids")) {
							ArrayList<String> value = (ArrayList<String>) fields.get(key);
	            			if (value != null) {
	            				user.arrInviterIDs = value;
	            			} else {
	            				user.arrInviterIDs = new ArrayList<String>();
	            			}
	            		}
	            	}
	            	
//	            	User.addUser(user);
	            	arrUsers.add(user);
				}
				
				User.updateUsersDatabase(arrUsers);
            	SearchActivity searchActivity = (SearchActivity) parent;
            	searchActivity.successGetAllPeoples(arrUsers);
			}
			
			@Override
			public void onError(List<String> errors) {

			}
		});
	}
}