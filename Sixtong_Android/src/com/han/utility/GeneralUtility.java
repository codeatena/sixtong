package com.han.utility;

import java.util.ArrayList;

public class GeneralUtility {

	public static boolean isExistStringInArray(String item, ArrayList<String> arrData) {
		for (String member: arrData) {
			if (item.equals(member)) {
				return true;
			}
		}
		return false;
	}
	
	public static boolean isExistIntegerInArray(int item, ArrayList<Integer> arrData) {
		for (int member: arrData) {
			if (item == member) {
				return true;
			}
		}
		return false;
	}
	
	public static int getIndexStringInArray(String str, ArrayList<String> arrData) {
		int k = 0;
		for (String item: arrData) {
			if (str.equals(item)) {
				return k;
			}
			k++;
		}
		return -1;
	}
}
