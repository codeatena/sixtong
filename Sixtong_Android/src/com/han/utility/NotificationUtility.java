package com.han.utility;

import com.han.chatapp.App;
import com.han.chatapp.R;
import com.han.chatapp.chat.ChatActivity;
import com.han.chatapp.main.MainActivity;
import com.han.chatapp.model.User;
import com.quickblox.chat.model.QBChatMessage;
import com.quickblox.chat.model.QBDialog;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;

public class NotificationUtility {

    public static void notification(QBChatMessage chatMessage, QBDialog dialog, boolean isTickerOnly) {
//    	notifyAlert();
    	if (PreferenceUtility.isEnableNewMessageAlerts()) {
    		notifyAlert(chatMessage, dialog, isTickerOnly);
        	playSoundAlert();
        	vibrate();
    	}
	}
    
    public static void notifyAlert(QBChatMessage chatMessage, QBDialog dialog, boolean isTickerOnly) {
    	
    	User sender = User.getUserFromQBUserID(chatMessage.getSenderId());
    	
    	NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(MainActivity.getInstatnce());
    	mBuilder.setContentTitle(sender.qbUser.getFullName());
    	mBuilder.setContentText(chatMessage.getBody());
    	mBuilder.setTicker(User.getUserFromQBUserID(chatMessage.getSenderId()).qbUser.getFullName() + ": " + chatMessage.getBody());
    	mBuilder.setSmallIcon(R.drawable.ic_launcher);
    	mBuilder.setLargeIcon(ImageUtility.StringToBitmap(sender.strPhoto));
    	
    	ChatActivity.notifyDialog = dialog;
        Intent intent = new Intent(MainActivity.getInstatnce(), ChatActivity.class);
        
        PendingIntent contentIntent = PendingIntent.getActivity(MainActivity.getInstatnce(), 0, intent, 0);
        
        mBuilder.setContentIntent(contentIntent);
        NotificationManager notificationManager = (NotificationManager) App.getInstance().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, mBuilder.build());
        
        if (isTickerOnly) 
        	notificationManager.cancel(0);
    }
    
    public static void playSoundAlert() {
		try {
			if (PreferenceUtility.isEnableSound()) {
				
				Uri notification = RingtoneManager
						.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
				Ringtone r = RingtoneManager.getRingtone(App.getInstance(),
						notification);
				r.play();	
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
    
    public static void vibrate() {
		if (PreferenceUtility.isEnableInAppVibrate()) {
			Vibrator mVibrator = (Vibrator) App.getInstance().getSystemService(Context.VIBRATOR_SERVICE);
			 
			// Vibrate for 300 milliseconds
			mVibrator.vibrate(1000);
		}
    }
}