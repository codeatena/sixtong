package com.han.utility;

import com.han.chatapp.R;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

public class TabsUtility {

	@SuppressLint("InflateParams")
	public static View renderTabView(Context context, int titleResource, int backgroundResource, int badgeNumber) {
        FrameLayout view = (FrameLayout) LayoutInflater.from(context).inflate(R.layout.tab_badge, null);
        // We need to manually set the LayoutParams here because we don't have a view root
        view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        TextView txtTitle = (TextView) view.findViewById(R.id.tab_text);
        txtTitle.setText(titleResource);
        view.findViewById(R.id.tab_text).setBackgroundResource(backgroundResource);
        updateTabBadge((TextView) view.findViewById(R.id.badge_textView), badgeNumber);
        return view;
    }

    @SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	public static void updateTabBadge(ActionBar.Tab tab, int badgeNumber) {
    	if (tab != null) {
    		updateTabBadge((TextView) tab.getCustomView().findViewById(R.id.badge_textView), badgeNumber);	
    	}
    }

    private static void updateTabBadge(TextView view, int badgeNumber) {
    	if (view == null) return;
    	
        if (badgeNumber > 0) {
            view.setVisibility(View.VISIBLE);
            view.setText(Integer.toString(badgeNumber));
        }
        else {
            view.setVisibility(View.GONE);
        }
    }
}
