package com.han.utility;

public class ValueUtility {

	public static Object getValueWithDefault(Object defaultValue, Object value) {
		
		if (value == null) {
			return defaultValue;
		}
		
		return value;
	}
}
