package com.han.utility;

import android.content.Context;
import android.telephony.SmsManager;

public class SMSUtility {

	public static void sendSMS(Context context, String phone, String message) {
	    try {
	    	SmsManager smsManager = SmsManager.getDefault();
	    	smsManager.sendTextMessage(phone, null, message, null, null);
	    	DialogUtility.show(context, "SMS sent success");
	    } catch (Exception e) {
	    	DialogUtility.show(context, "SMS sent Failed");
	    	e.printStackTrace();
	    }
	}
}