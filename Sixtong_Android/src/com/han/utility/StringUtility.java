package com.han.utility;

public class StringUtility {

	public static String getBetweenString(String src, String first, String last) {
		
		String ret;
		
		int pos1 = src.indexOf(first);
		if (pos1 < 0) return null;
		int pos2 = src.indexOf(last, pos1);
		if (pos2 < 0) return null;
		
		ret = src.substring(pos1 + 1, pos2);
		return ret;
	}
	
	public static String getPhoneFormatString(String phoneNumber) {
		
		if (phoneNumber.length() < 10) return "";
		String str = phoneNumber.substring(0, 3) + "-" + phoneNumber.substring(3, 7) + "-" + phoneNumber.substring(7);
		
		return str;
	}
}