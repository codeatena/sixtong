package com.han.utility;

import com.han.chatapp.App;
import com.han.chatapp.R;
import com.han.chatapp.main.MainActivity;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class PreferenceUtility {
	
	public static final String PREF_NAME = "chatapp_pref";
	
	public static SharedPreferences sharedPreferences;
	public static Editor prefEditor;
	
    public static final String PREFERENCE_KEY_APP_VERSION = "appVersion";
    public static final String PREFERENCE_KEY_PUSH_REG_ID = "push_registration_id";
	
	public static final String PREFERENCE_KEY_CUR_USER_NAME = "current_user_name";
	public static final String PREFERENCE_KEY_CUR_USER_PASSWORD = "current_user_password";
	
	public static final String PREFERENCE_KEY_USER_STATUS = "user_status";
	public static final String PREFERENCE_KEY_USER_FULL_NAME = "user_full_name";
	public static final String PREFERENCE_KEY_USER_COUNTRY_CODE = "user_country_code";
	public static final String PREFERENCE_KEY_USER_PHONE_NUMBER = "user_phone_number";
	public static final String PREFERENCE_KEY_USER_PASSWORD = "user_password";
	public static final String PREFERENCE_KEY_USER_SECRET_ID = "user_secret_id";
	public static final String PREFERENCE_KEY_USER_PHOTO = "user_photo";
	
	public static final String PREFERENCE_KEY_FONT_SIZE = "user_font_size";
	public static final String PREFERENCE_KEY_LANGUAGE = "user_language";
	
	public static final String PREFERENCE_KEY_NEW_MESSAGE_ALERTS = "enable_new_message_alerts";
	public static final String PREFERENCE_KEY_NOTIFICATION_CENTER = "enable_notification_center";
	public static final String PREFERENCE_KEY_SOUND = "enable_sound";
	public static final String PREFERENCE_KEY_ALERT_SOUND_TYPE = "alert_sound_type";
	public static final String PREFERENCE_KEY_IN_APP_VIBRATE = "enable_in_app_vibrate";
	public static final String PREFERENCE_KEY_MOMENTS_UPDATES = "enable_moments_updates";
	
	public static final String PREFERENCE_KEY_FRIEND_IDS = "my_friend_ids";
	public static final String PREFERENCE_KEY_INVITER_IDS = "my_inviter_ids";
	
	public static final String PREFERENCE_KEY_CONTACTS_BADGE = "contacts_badge";
//	public static final String PREFERENCE_KEY_CHATS_BADGE = "chats_badge";

	public static final String USER_STATUS_SIGNUP = "signup";
	public static final String USER_STATUS_VERIFIED = "verified";
	public static final String USER_STATUS_LOGIN = "login";
	
	public static final int FONT_SIZE_SMALL = 15;
	public static final int FONT_SIZE_MEDIUM = 18;
	public static final int FONT_SIZE_LARGE = 20;
	public static final int FONT_SIZE_X_LARGE = 25;
	public static final int FONT_SIZE_XX_LARGE = 30;
	
	public static final int FONT_INDEX_SMALL = 0;
	public static final int FONT_INDEX_MEDIUM = 1;
	public static final int FONT_INDEX_LARGE = 2;
	public static final int FONT_INDEX_X_LARGE = 3;
	public static final int FONT_INDEX_XX_LARGE = 4;
	
	public static final int LANGUAGE_AUTO = 1;
	
//	public int nFontSize;
//	public int nCurLanguage;
	
	public static String getFontSizeString() {
		String ret = "";
		
		int nFontSize = PreferenceUtility.sharedPreferences.getInt(PREFERENCE_KEY_FONT_SIZE, FONT_SIZE_MEDIUM);
		
		if (nFontSize == FONT_SIZE_SMALL) {
			ret = App.getInstance().getResources().getString(R.string.small);
		}
		if (nFontSize == FONT_SIZE_MEDIUM) {
			ret = App.getInstance().getResources().getString(R.string.medium);
		}
		if (nFontSize == FONT_SIZE_LARGE) {
			ret = App.getInstance().getResources().getString(R.string.large);
		}
		if (nFontSize == FONT_SIZE_X_LARGE) {
			ret = App.getInstance().getResources().getString(R.string.x_large);
		}
		if (nFontSize == FONT_SIZE_XX_LARGE) {
			ret = App.getInstance().getResources().getString(R.string.xx_large);
		}
		
		return ret;
	}
	
	public static float getFontSize() {
		String str = getFontSizeString();
		if (str.equals(App.getInstance().getString(R.string.small))) {
			return FONT_SIZE_SMALL;
		}
		if (str.equals(App.getInstance().getString(R.string.medium))) {
			return FONT_SIZE_MEDIUM;
		}
		if (str.equals(App.getInstance().getString(R.string.large))) {
			return FONT_SIZE_LARGE;
		}
		if (str.equals(App.getInstance().getString(R.string.x_large))) {
			return FONT_INDEX_X_LARGE;
		}
		if (str.equals(App.getInstance().getString(R.string.xx_large))) {
			return FONT_SIZE_XX_LARGE;
		}
		
		return FONT_SIZE_MEDIUM;
	}
	
	public static void setFontSize(int index) {
		int nFontSize = FONT_SIZE_MEDIUM;
		
		if (index == FONT_INDEX_SMALL) nFontSize = FONT_SIZE_SMALL;
		if (index == FONT_INDEX_MEDIUM) nFontSize = FONT_SIZE_MEDIUM;
		if (index == FONT_INDEX_LARGE) nFontSize = FONT_SIZE_LARGE;
		if (index == FONT_INDEX_X_LARGE) nFontSize = FONT_SIZE_X_LARGE;
		if (index == FONT_INDEX_XX_LARGE) nFontSize = FONT_SIZE_XX_LARGE;
		
		prefEditor.putInt(PREFERENCE_KEY_FONT_SIZE, nFontSize);
		prefEditor.commit();
	}
	
	public static String getLanguageString() {
		int nCurLanguage = sharedPreferences.getInt(PREFERENCE_KEY_LANGUAGE, LANGUAGE_AUTO);
		String[] languages = App.getInstance().getResources().getStringArray(R.array.languages_array);
		return languages[nCurLanguage - 1];
	}
	
	public static void setEnableNewMessageAlerts(boolean value) {
		prefEditor.putBoolean(PREFERENCE_KEY_NEW_MESSAGE_ALERTS, value);
		prefEditor.commit();
	}
	
	public static boolean isEnableNewMessageAlerts() {
		return sharedPreferences.getBoolean(PREFERENCE_KEY_NEW_MESSAGE_ALERTS, true);
	}
	
	public static void setEnableNotficationCenter(boolean value) {
		prefEditor.putBoolean(PREFERENCE_KEY_NOTIFICATION_CENTER, value);
		prefEditor.commit();
	}
	
	public static boolean isEnableNotificationCenter() {
		return sharedPreferences.getBoolean(PREFERENCE_KEY_NOTIFICATION_CENTER, true);
	}
	
	public static void setEnableSound(boolean value) {
		prefEditor.putBoolean(PREFERENCE_KEY_SOUND, value);
		prefEditor.commit();
	}
	
	public static boolean isEnableSound() {
		return sharedPreferences.getBoolean(PREFERENCE_KEY_SOUND, true);
	}
	
	public static void setAlertSoundType(int nType) {
		prefEditor.putInt(PREFERENCE_KEY_ALERT_SOUND_TYPE, nType);
		prefEditor.commit();
	}
	
	public static int getAlertSoundType() {
		return sharedPreferences.getInt(PREFERENCE_KEY_ALERT_SOUND_TYPE, 0);
	}
	
	public static void setEnableInAppVibrate(boolean value) {
		prefEditor.putBoolean(PREFERENCE_KEY_IN_APP_VIBRATE, value);
		prefEditor.commit();
	}
	
	public static boolean isEnableInAppVibrate() {
		return sharedPreferences.getBoolean(PREFERENCE_KEY_IN_APP_VIBRATE, true);
	}
	
	public static void setEnableMomentsUpdates(boolean value) {
		prefEditor.putBoolean(PREFERENCE_KEY_MOMENTS_UPDATES, value);
		prefEditor.commit();
	}
	
	public static boolean isEnableMomentsUpdates() {
		return sharedPreferences.getBoolean(PREFERENCE_KEY_MOMENTS_UPDATES, true);
	}
	
	public static int getContactsBadge() {
		return sharedPreferences.getInt(PREFERENCE_KEY_CONTACTS_BADGE, 0);
	}
	
	public static void increaseContactsMoment() {
		int k = sharedPreferences.getInt(PREFERENCE_KEY_CONTACTS_BADGE, 0);
		k++;
		prefEditor.putInt(PREFERENCE_KEY_CONTACTS_BADGE, k);
		prefEditor.commit();
	}
	
	public static void clearMoment(int nTabIndex) {
		if (nTabIndex == MainActivity.TAB_INDEX_CONTACTS) {
			prefEditor.putInt(PREFERENCE_KEY_CONTACTS_BADGE, 0);
			prefEditor.commit();
		}
	}
}