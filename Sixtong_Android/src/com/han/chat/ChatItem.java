package com.han.chat;

import com.han.chatapp.GlobalData;
import com.han.utility.TimeUtility;

public class ChatItem {
	
	public String message;
	public int userID;
	public String time;
	
	public ChatItem(String _message, int _userID) {
		message = _message;
		userID = _userID;
		time = TimeUtility.getCurrentTime();
	}
	
	public ChatItem(String _message, int _userID, long timeStamp) {
		message = _message;
		userID = _userID;
		time = TimeUtility.getStringFromTimeStamp(timeStamp * 1000, "yyyy-MM-dd HH:mm:ss");
	}
	
	public boolean isMyChat() {
		if (userID == GlobalData.myUser.qbUser.getId()) {
			return true;
		}
		return false;
	}
}