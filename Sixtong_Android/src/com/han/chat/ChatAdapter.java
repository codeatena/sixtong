package com.han.chat;

import java.util.ArrayList;
import com.han.chatapp.R;
import com.han.chatapp.model.User;
import com.han.utility.ImageUtility;
import com.han.utility.PreferenceUtility;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ChatAdapter extends ArrayAdapter<ChatItem> {

    public ChatAdapter(Context context, ArrayList<ChatItem> chatItems) {
        super(context, 0, chatItems);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
    	
        ChatItem chatItem = getItem(position);
        
        View view;
        
        LayoutInflater inflater = LayoutInflater.from(getContext());
        if (chatItem.isMyChat()) {
        	view = inflater.inflate(R.layout.chat_item_sent, parent, false);
        } else {
        	view = inflater.inflate(R.layout.chat_item_recv, parent, false);
        }
        
        TextView txtTime = (TextView) view.findViewById(R.id.chat_time_textView);
        TextView txtMessage = (TextView) view.findViewById(R.id.chat_message_textView);
        ImageView imgPhoto = (ImageView) view.findViewById(R.id.chat_user_photo_imageView);
        
        txtMessage.setTextSize(PreferenceUtility.getFontSize());
        
        txtTime.setText(chatItem.time);
        txtMessage.setText(chatItem.message);
    	User user = User.getUserFromQBUserID(chatItem.userID);
    	
    	if (user == null) {
    		new User().getInfFromServer(chatItem.userID);
//    		imgPhoto.setImageResource(R.drawable.ic_action_person);
    	} else {
    		if (user.strPhoto != null && !user.strPhoto.equals("")) {
    			imgPhoto.setImageBitmap(ImageUtility.StringToBitmap(user.strPhoto));
    		}
    	}
    	
        return view;
    }
}
