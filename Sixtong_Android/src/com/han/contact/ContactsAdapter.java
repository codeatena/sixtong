package com.han.contact;

import java.util.ArrayList;
import com.han.chatapp.R;
import com.han.chatapp.model.User;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ContactsAdapter extends ArrayAdapter<User> {

    public ContactsAdapter(Context context, ArrayList<User> users) {
        super(context, 0, users);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item
        User user = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            view = inflater.inflate(R.layout.row_contact, parent, false);
        }
        
        // Populate the data into the template view using the data object
        TextView txtName = (TextView) view.findViewById(R.id.contact_name_textView);
        TextView txtPhone = (TextView) view.findViewById(R.id.contact_phone_textView);
        TextView txtAction = (TextView) view.findViewById(R.id.contact_action_textView);
        
        if (user.qbUser == null) {
            txtAction.setText(R.string.invite_string);
        } else {
        	if (user.isMyFriend()) {
        		txtAction.setText(R.string.added_string);
        	} else {
        		txtAction.setText(R.string.add_string);
        	}
        }
        
        txtName.setText(user.contact.name);
        txtPhone.setText("");

        if (user.contact.numbers.size() > 0 && user.contact.numbers.get(0) != null) {
        	txtPhone.setText(user.contact.numbers.get(0).number);
        }
        
        return view;
    }
}