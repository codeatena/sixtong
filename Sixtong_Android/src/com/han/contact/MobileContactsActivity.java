package com.han.contact;

import java.util.ArrayList;
import java.util.Locale;
import com.han.chatapp.GlobalData;
import com.han.chatapp.R;
import com.han.chatapp.model.User;
import com.han.chatapp.user.ProfileActivity;
import com.han.quickblox.module.QBMyUserService;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.MenuItemCompat.OnActionExpandListener;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;

public class MobileContactsActivity extends Activity {

	ArrayList<User> arrSrchUsers = new ArrayList<User>();
	
    ListView lstContacts;
    SearchView searchView;
    ContactsAdapter adapterContacts;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile_contacts);
        
        new QBMyUserService(this).readContactUsers();
//        makeList();
    }
    
    public void makeList() {
    	
    	arrSrchUsers.addAll(GlobalData.arrContactUsers);
        lstContacts = (ListView) findViewById(R.id.mobile_contact_listView);
        adapterContacts = new ContactsAdapter(this, arrSrchUsers);
        lstContacts.setAdapter(adapterContacts);
        
        lstContacts.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				// TODO Auto-generated method stub
				Log.e("list click position", Integer.toString(position));
				User user = arrSrchUsers.get(position);
				ProfileActivity.ownUser = user;
				MobileContactsActivity.this.startActivity(new Intent(MobileContactsActivity.this, ProfileActivity.class));
//				Contact contact = arrSrchContact.get(position);
			}
        });
    }
    
    @Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    // Inflate the menu items for use in the action bar
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.search_activity_action, menu);
	    MenuItem searchItem = menu.findItem(R.id.action_search);
	    
	    searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
	    ActionBar actionBar = MobileContactsActivity.this.getActionBar();
	    actionBar.setHomeButtonEnabled(true);
	    
	    MenuItemCompat.setOnActionExpandListener(searchItem, new OnActionExpandListener() {
	        @Override
	        public boolean onMenuItemActionCollapse(MenuItem item) {
	            // Do something when collapsed
	        	Log.e("searchitem", "onMenuItemActionCollapse");

	            return true;  // Return true to collapse action view
	        }

	        @Override
	        public boolean onMenuItemActionExpand(MenuItem item) {
	            // Do something when expanded
	        	Log.e("searchitem", "onMenuItemActionExpand");

	            return true;  // Return true to expand action view
	        }
	    });
	    searchView.setOnQueryTextListener(new OnQueryTextListener() {
	        @Override
	        public boolean onQueryTextSubmit(String query) {
	            search(query);
	            return true;
	        }

	        @Override
	        public boolean onQueryTextChange(String newText) {
	        	search(newText);
	            return true;
	        }
	    });
	    return super.onCreateOptionsMenu(menu);
	}
    
    @Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle presses on the action bar items
	    switch (item.getItemId()) {
	        case android.R.id.home:
	        	finish();
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
    
    @SuppressLint("DefaultLocale")
	private void search(String query) {
    	arrSrchUsers.clear();
    	for (User user: GlobalData.arrContactUsers) {
    		if (user.contact.name.toLowerCase(Locale.ENGLISH).contains(query.toLowerCase())) {
    			arrSrchUsers.add(user);
    		}
    	}
    	adapterContacts.notifyDataSetChanged();
    }
}
