package com.han.contact.model;

import java.util.ArrayList;

public class Contact {

	public String id;
    public String name;
//    public String image_uri;
    
    public ArrayList<ContactEmail> emails;
    public ArrayList<ContactPhone> numbers;
//    public QBUser qbUser;

    public Contact(String id, String name) {
        this.id = id;
        this.name = name;
        this.emails = new ArrayList<ContactEmail>();
        this.numbers = new ArrayList<ContactPhone>();
//        qbUser = null;
    }

    public void addEmail(String address, String type) {
        emails.add(new ContactEmail(address, type));
    }

    public void addNumber(String number, String type) {
        numbers.add(new ContactPhone(number, type));
    }
}