package com.han.widget.listView;

import android.graphics.Bitmap;

import com.han.chatapp.R;

public class CellItem extends MyListItem {
	
	public String title;
	public String desc;
	public String status = "";
	public int nStatusPictureID = -1;
	public boolean isHavePhoto;
	
	public Bitmap bmpPhoto = null;
	
	public CellItem(String _title, String _desc, String _status, boolean _isHavePhoto) {
		layoutID = R.layout.row_big_cell;
		title = _title;
		desc = _desc;
		status = _status;
		isHavePhoto = _isHavePhoto;
	}
	
	public CellItem(String _title, String _desc, int _nStatusPictureID, boolean _isHavePhoto) {
		layoutID = R.layout.row_big_cell;
		title = _title;
		desc = _desc;
		status = "";
		nStatusPictureID = _nStatusPictureID;
		isHavePhoto = _isHavePhoto;
	}
	
	public CellItem(String _title, String _status, boolean _isHavePhoto) {
		layoutID = R.layout.row_small_cell;
		title = _title;
		status = _status;
		isHavePhoto = _isHavePhoto;
	}
	
	public CellItem(String _title, int _nStatusPictureID, boolean _isHavePhoto) {
		layoutID = R.layout.row_small_cell;
		title = _title;
		nStatusPictureID = _nStatusPictureID;
	}
	
	public void setImageBitmap(Bitmap bmp) {
		bmpPhoto = bmp;
	}
}