package com.han.widget.listView;

import android.net.Uri;

import com.han.chatapp.R;
import com.han.chatapp.model.Post;

public class NewPostItem extends MyListItem {

	Post myPost = new Post();
	Uri fileUri;
	
	public NewPostItem(Post post, Uri uri) {
		layoutID = R.layout.row_my_post;
		myPost = post;
		fileUri = uri;
	}
}
