package com.han.widget.listView;

import com.han.chatapp.R;
import com.han.chatapp.model.Post;

public class PostItem extends MyListItem{

	Post post = new Post();
	public PostItem(Post _post) {
		layoutID = R.layout.row_post;
		post = _post;
	}
}
