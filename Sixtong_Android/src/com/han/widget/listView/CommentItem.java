package com.han.widget.listView;

import com.han.chatapp.R;
import com.han.chatapp.model.Comment;

public class CommentItem extends MyListItem {
	Comment comment;
	
	public CommentItem(Comment _comment) {
		layoutID = R.layout.row_comment;
		comment = _comment;
	}
}