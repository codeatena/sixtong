package com.han.widget.listView;

import java.util.ArrayList;

import com.han.chatapp.R;
import com.han.chatapp.model.Comment;
import com.han.chatapp.model.Post;
import com.han.chatapp.model.User;
import com.han.chatapp.post.MyPostsActivity;
import com.han.chatapp.post.NewCommentActivity;
import com.han.chatapp.post.PostsActivity;
import com.han.utility.ImageUtility;
import com.han.utility.TimeUtility;
import com.koushikdutta.urlimageviewhelper.UrlImageViewCallback;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MyListAdapter extends ArrayAdapter<MyListItem> {
	
	public int nSelectedItem;
	
    public MyListAdapter(Context context, ArrayList<MyListItem> listItems) {
        super(context, 0, listItems);
        nSelectedItem = -1;
    }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
    	View view = convertView;
    	
    	MyListItem item = getItem(position);
		LayoutInflater inflater = LayoutInflater.from(getContext());

    	if (item instanceof SectionItem) {
    		SectionItem sectionItem = (SectionItem) item;
    		
    		view = inflater.inflate(sectionItem.layoutID, parent, false);
    		view.setOnClickListener(null);
    		view.setOnLongClickListener(null);
    		view.setLongClickable(false);
    		
    		if (sectionItem.layoutID == R.layout.row_section) {
        		TextView txtTitle = (TextView) view.findViewById(R.id.title_textView);
        		txtTitle.setText(sectionItem.title);
    		}
    	}
    	if (item instanceof CellItem) {
    		CellItem cellItem = (CellItem) item;
    		
    		view = inflater.inflate(cellItem.layoutID, parent, false);
    		
    		if (cellItem.layoutID == R.layout.row_big_cell) {
    			ImageView imgPhoto = (ImageView) view.findViewById(R.id.cell_photo_imageView);
    			TextView txtTitle = (TextView) view.findViewById(R.id.cell_title_textView);
    			TextView txtDesc = (TextView) view.findViewById(R.id.cell_desc_textView);
    			TextView txtStatus = (TextView) view.findViewById(R.id.cell_status_textView);
    			TextView txtDevider = (TextView) view.findViewById(R.id.devider_textView);
    			ImageView imgStatus = (ImageView) view.findViewById(R.id.cell_status_imageView); 
    			
    			txtTitle.setText(cellItem.title);
    			txtDesc.setText(cellItem.desc);
    			txtStatus.setText(cellItem.status);
    			
    			if (cellItem.isHavePhoto)
    				imgPhoto.setVisibility(View.VISIBLE);
    			else
    				imgPhoto.setVisibility(View.GONE);
    			
    			if (cellItem.bmpPhoto != null) {
    				imgPhoto.setImageBitmap(cellItem.bmpPhoto);
    			}
    			
    			imgStatus.setVisibility(View.GONE);
    			
    			if (cellItem.nStatusPictureID != -1 && position == nSelectedItem) {
    				imgStatus.setVisibility(View.VISIBLE);
    				imgStatus.setImageResource(cellItem.nStatusPictureID);
    			}
    			
    			txtDevider.setVisibility(View.VISIBLE);
    			
    			if (position == this.getCount() - 1) {
    				txtDevider.setVisibility(View.GONE);
    			} else {
    				MyListItem nextItem = getItem(position + 1);
    				if (nextItem instanceof SectionItem)
    					txtDevider.setVisibility(View.GONE);
    			}
    		}
    		if (cellItem.layoutID == R.layout.row_small_cell) {
    			ImageView imgPhoto = (ImageView) view.findViewById(R.id.cell_photo_imageView);
    			TextView txtTitle = (TextView) view.findViewById(R.id.cell_title_textView);
    			TextView txtStatus = (TextView) view.findViewById(R.id.cell_status_textView);
    			TextView txtDevider = (TextView) view.findViewById(R.id.devider_textView);
    			ImageView imgStatus = (ImageView) view.findViewById(R.id.cell_status_imageView); 

    			txtTitle.setText(cellItem.title);
    			txtStatus.setText(cellItem.status);
    			
    			if (cellItem.isHavePhoto)
    				imgPhoto.setVisibility(View.VISIBLE);
    			else
    				imgPhoto.setVisibility(View.GONE);
    			
    			if (cellItem.bmpPhoto != null) {
    				imgPhoto.setImageBitmap(cellItem.bmpPhoto);
    			}
    			
    			imgStatus.setVisibility(View.GONE);
    			
    			if (cellItem.nStatusPictureID != -1 && position == nSelectedItem) {
    				imgStatus.setVisibility(View.VISIBLE);
    				imgStatus.setImageResource(cellItem.nStatusPictureID);
    			}
    			
    			txtDevider.setVisibility(View.VISIBLE);
    			
    			if (position == this.getCount() - 1) {
    				txtDevider.setVisibility(View.GONE);
    			} else {
    				MyListItem nextItem = getItem(position + 1);
    				if (nextItem instanceof SectionItem)
    					txtDevider.setVisibility(View.GONE);
    			}
    		}
    	}
    	if (item instanceof UploadPostItem) {
    		view = inflater.inflate(item.layoutID, parent, false);
    		
    		RelativeLayout rlytUploadPost = (RelativeLayout) view.findViewById(R.id.upload_post_relativeLayout);
    		TextView txtTime = (TextView) view.findViewById(R.id.post_date_textView);
    		
    		String strDate = TimeUtility.getCurrentTimeAsFormat("MM.dd");
    		txtTime.setText(strDate);
    		
    		rlytUploadPost.setOnClickListener(new Button.OnClickListener() {
    			@Override
    			public void onClick(View v) {
    				MyPostsActivity myPostsActivity = (MyPostsActivity) getContext();
    				myPostsActivity.showDialogForPicture();
    			}
            });
    	}
    	if (item instanceof MyPostItem) {
    		view = inflater.inflate(item.layoutID, parent, false);
    		
    		TextView txtText = (TextView) view.findViewById(R.id.my_post_text_textView);
    		final ImageView imgPhoto = (ImageView) view.findViewById(R.id.my_post_photo_imageView);
    		TextView txtTime = (TextView) view.findViewById(R.id.post_date_textView);
    		
    		final Post post = ((MyPostItem) item).myPost;
    		txtText.setText(post.text);
    		
    		String strDate = TimeUtility.getStringFromTimeStamp(Long.parseLong(post.time) * 1000, "MM.dd");
    		txtTime.setText(strDate);
    		
            UrlImageViewHelper.setUrlDrawable(imgPhoto, ((MyPostItem) item).myPost.url, new UrlImageViewCallback() {
                @Override
                public void onLoaded(ImageView imageView, Bitmap loadedBitmap, String url, boolean loadedFromCache) {
                	imgPhoto.setImageBitmap(ImageUtility.getRotatedBitmap(loadedBitmap, post.orientation));
                }
            });
    	}
    	if (item instanceof NewPostItem) {
    		view = inflater.inflate(item.layoutID, parent, false);
    		
    		TextView txtText = (TextView) view.findViewById(R.id.my_post_text_textView);
    		ImageView imgPhoto = (ImageView) view.findViewById(R.id.my_post_photo_imageView);
    		TextView txtTime = (TextView) view.findViewById(R.id.post_date_textView);
    		
    		final Post post = ((NewPostItem) item).myPost;
    		txtText.setText(post.text);
    		
    		imgPhoto.setImageBitmap(ImageUtility.getBitmapFromFileUri(((NewPostItem) item).fileUri, 0, 0));
    		String strDate = TimeUtility.getStringFromTimeStamp(Long.parseLong(post.time) * 1000, "MM.dd");
    		txtTime.setText(strDate);
    	}
    	if (item instanceof PostItem) {
    		view = inflater.inflate(item.layoutID, parent, false);
    		
    		ImageView imgUserPhoto = (ImageView) view.findViewById(R.id.post_user_photo_imageView);
    		TextView txtUserFullName = (TextView) view.findViewById(R.id.post_user_fullname_textView);
    		TextView txtPostText = (TextView) view.findViewById(R.id.post_text_textView);
    		final ImageView imgPhoto = (ImageView) view.findViewById(R.id.post_imageView);
    		LinearLayout llytCommentList = (LinearLayout) view.findViewById(R.id.comment_list_linearLayout);
    		ImageView imgCommentButton = (ImageView) view.findViewById(R.id.comment_buttonImageView);
    		
    		final Post post = ((PostItem) item).post;
    		
            UrlImageViewHelper.setUrlDrawable(imgPhoto, post.url, new UrlImageViewCallback() {
                @Override
                public void onLoaded(ImageView imageView, Bitmap loadedBitmap, String url, boolean loadedFromCache) {
                	imgPhoto.setImageBitmap(ImageUtility.getRotatedBitmap(loadedBitmap, post.orientation));
                }
            });
    		txtPostText.setText(post.text);
//    		imgPhoto.setImageUrl(post.url);
    		
    		for (Comment comment: post.arrComment) {
    			
    			View viewComment = inflater.inflate(R.layout.row_comment, parent, false);
    			TextView txtCommentFullName = (TextView) viewComment.findViewById(R.id.comment_fullname_textView);
    			TextView txtCommentText = (TextView) viewComment.findViewById(R.id.comment_text_textView);
    			
    			txtCommentFullName.setText(comment.fullName + ":");
    			txtCommentText.setText(comment.text);
    			
    			llytCommentList.addView(viewComment);
    		}

    		User user = User.getUserFromSecretID(post.userSecretID);
    		if (user != null) {
    			txtUserFullName.setText(user.qbUser.getFullName());
    			imgUserPhoto.setImageBitmap(ImageUtility.StringToBitmap(user.strPhoto));
    		}
	        
	        imgCommentButton.setOnClickListener(new Button.OnClickListener() {
				@Override
				public void onClick(View v) {
					PostsActivity postActivity = (PostsActivity) getContext();
					NewCommentActivity.curPost = post;
					postActivity.startActivityForResult(new Intent(getContext(), NewCommentActivity.class), PostsActivity.REQUEST_CODE_NEW_COMMENT);
				}
	        });
    	}
    	if (item instanceof CommentItem) {
    		view = inflater.inflate(item.layoutID, parent, false);
    		TextView txtFullName = (TextView) view.findViewById(R.id.comment_fullname_textView);
    		TextView txtComment = (TextView) view.findViewById(R.id.comment_text_textView);
    		
    		Comment comment = ((CommentItem) item).comment;
    		txtFullName.setText(comment.fullName + ":");
    		txtComment.setText(comment.text);
    	}
    	
        return view;
    }
    
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
		  // pre-condition
        	return;
		}

	    int totalHeight = listView.getPaddingTop() + listView.getPaddingBottom();
	    for (int i = 0; i < listAdapter.getCount(); i++) {
	    	View listItem = listAdapter.getView(i, null, listView);
	    	
	    	if (listItem instanceof ViewGroup) {
	    		listItem.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
	    	}
	    	
	    	listItem.measure(0, 0);
	    	totalHeight += listItem.getMeasuredHeight();
	    }

	    ViewGroup.LayoutParams params = listView.getLayoutParams();
	    params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
	    
	    listView.setLayoutParams(params);
	    listView.requestLayout();
    }
}