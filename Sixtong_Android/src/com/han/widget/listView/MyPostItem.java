package com.han.widget.listView;

import com.han.chatapp.R;
import com.han.chatapp.model.Post;

public class MyPostItem extends MyListItem {
	
	Post myPost = new Post();
	public MyPostItem(Post post) {
		layoutID = R.layout.row_my_post;
		myPost = post;
	}
}