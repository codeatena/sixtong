package com.han.widget.listView;

import com.han.chatapp.R;

public class SectionItem extends MyListItem {
	public String title;
	public SectionItem(String _title) {
		title = _title;
		layoutID = R.layout.row_section;
	}
}