package com.han.chatapp.post;

import java.util.ArrayList;

import com.han.chatapp.GlobalData;
import com.han.chatapp.R;
import com.han.chatapp.model.Post;
import com.han.chatapp.service.PostAsyncTask;
import com.han.utility.CameraUtility;
import com.han.utility.ImageUtility;
import com.han.widget.listView.CellItem;
import com.han.widget.listView.MyListAdapter;
import com.han.widget.listView.MyListItem;
import com.han.widget.listView.MyPostItem;
import com.han.widget.listView.NewPostItem;
import com.han.widget.listView.SectionItem;
import com.han.widget.listView.UploadPostItem;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ListView;

public class MyPostsActivity extends Activity {

	final int REQUEST_CODE_NEW_POST = 1010;
	
	ListView lstPost;
	MyListAdapter adpPost;
	ArrayList<MyListItem> arrItem;
	
	private Uri fileUri;

	public static Post newPost = new Post();
	
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_simple_list);

	    ActionBar actionBar = getActionBar();
	    actionBar.setHomeButtonEnabled(true);
	    
	    initWidget();
	    initValue();
	    initEvent();
	}
	
	private void initWidget() {
		lstPost = (ListView) findViewById(R.id.listView);
	}
	
	private void initValue() {
		arrItem = new ArrayList<MyListItem>();
		arrItem.add(new SectionItem(""));
		
		CellItem cell = null;
		
		if (GlobalData.myUser.qbUser != null) {
			cell = new CellItem(GlobalData.myUser.qbUser.getFullName(), GlobalData.myUser.words, "", true);
		}
		if (GlobalData.myUser != null && GlobalData.myUser.strPhoto != null) {
			cell.setImageBitmap(ImageUtility.StringToBitmap(GlobalData.myUser.strPhoto));	
		}
		
		if (cell != null) {
			arrItem.add(cell);	
		}
		
		arrItem.add(new UploadPostItem());
		
		adpPost = new MyListAdapter(this, arrItem);
		lstPost.setAdapter(adpPost);
		lstPost.setDividerHeight(0);
	}
	
	private void initEvent() {
		new PostAsyncTask(this).execute(PostAsyncTask.ACTION_GET_MY_POST);
	}
	
	public void successGetMyPosts(ArrayList<Post> arrPost) {
		for (Post post: arrPost) {
			adpPost.add(new MyPostItem(post));
		}
		adpPost.notifyDataSetChanged();
	}
	
	public void takePhoto() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
 
        fileUri = CameraUtility.getOutputMediaFileUri(CameraUtility.MEDIA_TYPE_IMAGE);
        
        try {
            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        } catch (Exception e) {
        	e.printStackTrace();
        }
 
        startActivityForResult(intent, CameraUtility.CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }
	
	public void selectPhoto() {
		Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
		photoPickerIntent.setType("image/*");
		startActivityForResult(photoPickerIntent, CameraUtility.SELECT_PICTURE_REQUEST_CODE);
	}
	
	protected void onActivityResult(int requestCode, int resultCode, Intent imageReturedIntent) {
		if (requestCode == CameraUtility.CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
//            	nGetPicture = TAKE_PICTURE_MODE;
//            	previewImage();
            	goNewPost();
            }
        }
		if (requestCode == CameraUtility.SELECT_PICTURE_REQUEST_CODE) {
			if (resultCode == RESULT_OK) {
//            	nGetPicture = SELECT_PICTURE_MODE;
				fileUri = imageReturedIntent.getData();
				fileUri = ImageUtility.getRealUriFromContentUri(fileUri);
//				previewImage();
				goNewPost();
			}
		}
		if (requestCode == REQUEST_CODE_NEW_POST) {
			if (resultCode == RESULT_OK) {
				arrItem.add(3, new NewPostItem(newPost, ImageUtility.getRealUriFromContentUri(fileUri)));
//				adpPost.notifyDataSetChanged();
				
				adpPost = new MyListAdapter(this, arrItem);
				lstPost.setAdapter(adpPost);
			}
		}
	}
	
	public void goNewPost() {
		NewPostActivity.photoUri = fileUri;
		startActivityForResult(new Intent(this, NewPostActivity.class), REQUEST_CODE_NEW_POST);
//		new PostAsyncTask(this).execute(PostAsyncTask.ACTION_UPLOAD_POST, "sample image", ImageUtility.getRealPathFromURI(fileUri));
	}
	
	public void showDialogForPicture() {
		DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
			
			private static final int TAKE_PICTURE = 0;
			private static final int SELECT_PICTURE = 1;
			private static final int CANCEL = 2;
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				switch (which) {
					case TAKE_PICTURE:
						Log.e("TAKE_PICTURE", "click");
						takePhoto();
						break;
					case SELECT_PICTURE:
						Log.e("SELECT_PICTURE", "click");
						selectPhoto();
						break;
					case CANCEL:
		    			dialog.dismiss();
		    			break;
				}
			}
		};
		
		new AlertDialog.Builder(MyPostsActivity.this)
	    	.setItems(R.array.dialog_get_picture, listener)
	    	.setCancelable(true)
	    	.show();
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle presses on the action bar items
	    switch (item.getItemId()) {
	        case android.R.id.home:
	        	finish();
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
}