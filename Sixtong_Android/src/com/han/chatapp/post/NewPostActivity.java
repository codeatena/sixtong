package com.han.chatapp.post;

import com.han.chatapp.R;
import com.han.chatapp.model.Post;
import com.han.chatapp.service.PostAsyncTask;
import com.han.utility.ImageUtility;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;

public class NewPostActivity extends Activity {

	EditText edtPostTitle;
	ImageView imgPostPhoto;
	
	public static Uri photoUri;
	
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_new_post);

	    ActionBar actionBar = getActionBar();
	    actionBar.setHomeButtonEnabled(true);
	    
	    initWidget();
	    initValue();
	    initEvent();
	}
	
	private void initWidget() {
		edtPostTitle = (EditText) findViewById(R.id.post_title_editText);
		imgPostPhoto = (ImageView) findViewById(R.id.post_photo_imageView);
	}
	
	@SuppressLint("NewApi")
	private void initValue() {
		ImageUtility.drawImageUri(imgPostPhoto, photoUri);
	}
	
	private void initEvent() {
		
	}
	
	private void uploadPost() {
		
		try {
			ExifInterface ei;
	        ei = new ExifInterface(photoUri.getPath());
	        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
	                ExifInterface.ORIENTATION_NORMAL);
	        
			new PostAsyncTask(this).execute(PostAsyncTask.ACTION_UPLOAD_POST, edtPostTitle.getText().toString(), ImageUtility.getRealPathFromContentUri(photoUri), 
					Integer.toString(orientation));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void succesUpload(Post post) {
		MyPostsActivity.newPost = post;
		setResult(RESULT_OK);
		finish();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    // Inflate the menu items for use in the action bar
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.upload_post_action, menu);

	    return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle presses on the action bar items
	    switch (item.getItemId()) {
	        case android.R.id.home:
	        	finish();
	            return true;
	        case R.id.action_upload:
	        	uploadPost();
	        	return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
}
