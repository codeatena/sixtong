package com.han.chatapp.post;

import java.util.ArrayList;

import com.han.chatapp.GlobalData;
import com.han.chatapp.R;
import com.han.chatapp.model.Post;
import com.han.chatapp.model.User;
import com.han.chatapp.service.PostAsyncTask;
import com.han.widget.listView.MyListAdapter;
import com.han.widget.listView.MyListItem;
import com.han.widget.listView.PostItem;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ListView;

public class PostsActivity extends Activity {

	public static final int REQUEST_CODE_NEW_COMMENT = 1011;
	
	ListView lstPosts;
	MyListAdapter adpPosts;
	ArrayList<MyListItem> arrItem;
	
	public static User ownUser;
	
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_simple_list);

	    ActionBar actionBar = getActionBar();
	    actionBar.setHomeButtonEnabled(true);
	    
	    initWidget();
	    initValue();
	    initEvent();
	}
	
	private void initWidget() {
		lstPosts = (ListView) findViewById(R.id.listView);
	}
	
	@SuppressLint("NewApi")
	private void initValue() {
		
		arrItem = new ArrayList<MyListItem>();
		adpPosts = new MyListAdapter(this, arrItem);
		lstPosts.setAdapter(adpPosts);
		
		String strSecretIDs = "";
		if (ownUser == null) {
			strSecretIDs = GlobalData.myUser.secretID;
			for (User user: GlobalData.myUser.getFriends()) {
				strSecretIDs = strSecretIDs + "," + user.secretID;
			}
		} else {
			strSecretIDs = ownUser.secretID;
			ActionBar actionBar = getActionBar();
			actionBar.setTitle(ownUser.qbUser.getFullName() + "'s Posts");
		}
		
		new PostAsyncTask(this).execute(PostAsyncTask.ACTION_GET_POSTS, strSecretIDs);
	}
	
	private void initEvent() {
		
	}
	
	protected void onActivityResult(int requestCode, int resultCode, Intent imageReturedIntent) {
		if (requestCode == REQUEST_CODE_NEW_COMMENT) {
            if (resultCode == RESULT_OK) {
            	Log.e("new comment", "success");
            	adpPosts.notifyDataSetChanged();
            }
        }
	}
	
	public void successGetPosts(ArrayList<Post> arrPost) {
		for (Post post: arrPost) {
			adpPosts.add(new PostItem(post));
		}
		adpPosts.notifyDataSetChanged();
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle presses on the action bar items
	    switch (item.getItemId()) {
	        case android.R.id.home:
	        	finish();
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
}
