package com.han.chatapp.post;

import com.han.chatapp.GlobalData;
import com.han.chatapp.R;
import com.han.chatapp.model.Comment;
import com.han.chatapp.model.Post;
import com.han.chatapp.service.PostAsyncTask;
import com.han.utility.DialogUtility;

import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;

public class NewCommentActivity extends Activity {

	EditText edtName;
	public static Post curPost;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_name);
	    
	    initWidget();
	    initValue();
	    initEvent();
	}
	
	private void initWidget() {
	    ActionBar actionBar = getActionBar();
	    actionBar.setHomeButtonEnabled(true);
	}
	
	private void initValue() {
		edtName = (EditText) findViewById(R.id.name_editText);
	}
	
	private void initEvent() {
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    // Inflate the menu items for use in the action bar
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.edit_name_activity_action, menu);

	    return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle presses on the action bar items
	    switch (item.getItemId()) {
	        case android.R.id.home:
	        	finish();
	            return true;
	        case R.id.action_save:
	        	actionSave();
	        	return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
	
	private void actionSave() {
		String text = edtName.getText().toString();
		
		if (text == null || text.equals("")) {
			DialogUtility.showGeneralAlert(this, "Error!", "Please input text");
			return;
		}
		new PostAsyncTask(this).execute(PostAsyncTask.ACTION_NEW_COMMENT, curPost.id, text);
	}
	
	public void successNewComment() {
		setResult(RESULT_OK);
		
		Comment comment = new Comment();
		comment.fullName = GlobalData.myUser.qbUser.getFullName();
		comment.userSecretID = GlobalData.myUser.secretID;
		comment.text = edtName.getText().toString();
		
		curPost.arrComment.add(comment);
		
		finish();
	}
}