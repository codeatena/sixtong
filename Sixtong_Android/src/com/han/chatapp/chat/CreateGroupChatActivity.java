package com.han.chatapp.chat;

import java.util.ArrayList;
import java.util.List;

import com.han.chatapp.GlobalData;
import com.han.chatapp.R;
import com.han.chatapp.model.User;
import com.han.chatapp.model.UserSelectItem;
import com.han.utility.DialogUtility;
import com.han.utility.ImageUtility;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.model.QBDialog;
import com.quickblox.chat.model.QBDialogType;
import com.quickblox.core.QBEntityCallbackImpl;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class CreateGroupChatActivity extends Activity {

	ListView lstUser;
	public static final int 	SELECT_CHECKBOX_ID = 100;
	ArrayList<UserSelectItem> arrUserSelectItems;
	UserAdapter userAdapter;
	MenuItem menuItemOK;
	EditText edtRoomName;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_create_group_chat);

		initWidget();
	}
	
	private void initWidget() {
		lstUser = (ListView) findViewById(R.id.user_listView);
		edtRoomName = (EditText) findViewById(R.id.room_name_editText);
		
	    ActionBar actionBar = CreateGroupChatActivity.this.getActionBar();
	    actionBar.setHomeButtonEnabled(true);
	    
	    arrUserSelectItems = new ArrayList<UserSelectItem>();
	    for (User user: GlobalData.myUser.getFriends()) {
	    	UserSelectItem userSelectItem = new UserSelectItem();
	    	userSelectItem.user = user;
	    	arrUserSelectItems.add(userSelectItem);
	    }
	    userAdapter = new UserAdapter(this, arrUserSelectItems);
	    lstUser.setAdapter(userAdapter);
//	    lstUser.setItemsCanFocus(true);
	    
	    lstUser.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				// TODO Auto-generated method stub
//				Log.e("list click position", Integer.toString(position));
				UserSelectItem userSelectItem = userAdapter.getItem(position);
				userSelectItem.isSelect = !userSelectItem.isSelect;
            	int selectedCount = userAdapter.selectedCount();
            	if (selectedCount > 0) {
            		menuItemOK.setTitle("OK(" + Integer.toString(selectedCount) + ")");	
            	} else {
            		menuItemOK.setTitle(R.string.dlg_ok);
            	}
				userAdapter.notifyDataSetChanged();
//				Contact contact = arrSrchContact.get(position);
			}
        });
	}
	
	public class UserAdapter extends ArrayAdapter<UserSelectItem> {
    	
		public UserAdapter(Context context, ArrayList<UserSelectItem> arrData) {
			super(context, 0, arrData);

		}
		
		@SuppressLint("ViewHolder")
		public View getView(int position, View convertView, ViewGroup parent) {
			
	        View row = convertView;
	        LayoutInflater inflater = LayoutInflater.from(getContext());
	        row = inflater.inflate(R.layout.row_sel_user, parent, false);
	        
	        TextView txtFullName = (TextView) row.findViewById(R.id.me_fullname_textView);
	        ImageView imgPhoto = (ImageView) row.findViewById(R.id.user_photo_imageView);
	        CheckBox chkSelect = (CheckBox) row.findViewById(R.id.user_select_checkBox);
	        UserSelectItem userSelectItem = getItem(position);
	        txtFullName.setText(userSelectItem.user.qbUser.getFullName());
	        
	        if (userSelectItem.user.strPhoto == null || userSelectItem.user.strPhoto.equals("")) {
		        imgPhoto.setImageResource(R.drawable.ic_action_person);
	        } else {
	        	imgPhoto.setImageBitmap(ImageUtility.StringToBitmap(userSelectItem.user.strPhoto));
	        }
	        
	        chkSelect.setChecked(userSelectItem.isSelect);
	        chkSelect.setId(SELECT_CHECKBOX_ID + position);
	        
	        chkSelect.setOnClickListener( new View.OnClickListener() {  
	            public void onClick(View v) {  
	            	CheckBox cb = (CheckBox) v;
	            	int pos = cb.getId() - SELECT_CHECKBOX_ID;
	            	UserSelectItem userSelectItem = getItem(pos);
	            	userSelectItem.isSelect = cb.isChecked();
	            	int selectedCount = selectedCount();
	            	if (selectedCount > 0) {
	            		menuItemOK.setTitle("OK(" + Integer.toString(selectedCount) + ")");	
	            	} else {
	            		menuItemOK.setTitle(R.string.dlg_ok);
	            	}
	            }
	        });
	        
	        return row;
		}
		
		public int selectedCount() {
			int count = 0;
			for (int i = 0; i < getCount(); i++) {
				UserSelectItem userSelectItem = getItem(i);
				if (userSelectItem.isSelect) {
					count++;
				}
			}
			
			return count;
		}
		
		public ArrayList<UserSelectItem> getSelectedUsers() {
			ArrayList<UserSelectItem> selectedUsers = new ArrayList<UserSelectItem>();
			for (int i = 0; i < getCount(); i++) {
				UserSelectItem userSelectItem = getItem(i);
				if (userSelectItem.isSelect) {
					selectedUsers.add(userSelectItem);
				}
			}
			
			return selectedUsers;
		}
    }
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    // Inflate the menu items for use in the action bar
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.user_select_activity_action, menu);
	    menuItemOK = menu.getItem(0);

	    return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle presses on the action bar items
	    switch (item.getItemId()) {
	        case android.R.id.home:
	        	finish();
	            return true;
	        case R.id.action_select_user:
	        	actionSelectUser();
	        	return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
	
	private void actionSelectUser() {
		
		ArrayList<UserSelectItem> selectedUsers = userAdapter.getSelectedUsers();
		String roomName = edtRoomName.getText().toString();
		if (roomName.equals("") || roomName == null) {
			DialogUtility.showGeneralAlert(this, "Error", "Please input room name!");
			return;
		}
		if (selectedUsers.size() == 0) {
			DialogUtility.showGeneralAlert(this, "Error", "You must choose one user at least");
			return;
		}
		ArrayList<Integer> occupantIdsList = new ArrayList<Integer>();
		for (UserSelectItem userSelectItem: selectedUsers) {
			occupantIdsList.add(userSelectItem.user.qbUser.getId());
		}
		
		QBDialog newDialog = new QBDialog();
		newDialog.setName(roomName);
		newDialog.setOccupantsIds(occupantIdsList);
		newDialog.setType(QBDialogType.GROUP);
		QBChatService.getInstance().getGroupChatManager().createDialog(newDialog, new QBEntityCallbackImpl<QBDialog>() {
			
		    @Override
		    public void onSuccess(QBDialog dialog, Bundle args) {
		    	finish();
		    }
		    
		    @Override
		    public void onError(List<String> errors) {
		        // handle errors
		    	DialogUtility.showGeneralAlert(CreateGroupChatActivity.this, "Error", errors.get(0));
		    }
		});
	}
}