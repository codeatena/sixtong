package com.han.chatapp.chat;

import java.util.ArrayList;
import java.util.List;
import com.han.chat.ChatAdapter;
import com.han.chat.ChatItem;
import com.han.chatapp.App;
import com.han.chatapp.GlobalData;
import com.han.chatapp.R;
import com.han.chatapp.main.MainActivity;
import com.han.quickblox.module.QBDialogService;
import com.han.quickblox.module.QBConst;
import com.han.utility.TimeUtility;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.model.QBChatHistoryMessage;
import com.quickblox.chat.model.QBChatMessage;
import com.quickblox.chat.model.QBDialog;
import com.quickblox.chat.model.QBDialogType;
import com.quickblox.core.QBEntityCallbackImpl;
import com.quickblox.core.request.QBCustomObjectRequestBuilder;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.Context;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

public class ChatActivity extends Activity {

	public static int opponentID;
//	public static int type;
	private static ChatActivity instance;

    public static QBDialog dialog = null;
    public static QBDialog notifyDialog = null;
    
	EditText edtMessage;
	Button btnSend;
	
	ListView lstChat;
	ArrayList<ChatItem> arrChatItems;
	ChatAdapter chatAdapter;
	
	boolean isLoadedChatHistory;
	
//    ChatManager chat;
	
	@SuppressLint("NewApi")
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_chat);

	    ActionBar actionBar = ChatActivity.this.getActionBar();
	    actionBar.setHomeButtonEnabled(true);
	    
	    instance = this;
	    
	    initWidget();
	    initValue();
	    initEvent();
	}
	
	public static ChatActivity getInstatnce() {
		return instance;
	}
	
	private void initWidget() {
		lstChat = (ListView) findViewById(R.id.chat_listView);
		edtMessage = (EditText) findViewById(R.id.send_message_editText);
		btnSend = (Button) findViewById(R.id.send_message_button);
	}
	
	private void initValue() {
		
        NotificationManager notificationManager = (NotificationManager) App.getInstance().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(0);
        
		isLoadedChatHistory = false;
		
		arrChatItems = new ArrayList<ChatItem>();
		chatAdapter = new ChatAdapter(this, arrChatItems);
		lstChat.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
		lstChat.setAdapter(chatAdapter);
		
		chatAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                lstChat.setSelection(chatAdapter.getCount() - 1);
            }
        });
		
		if (notifyDialog != null) {
			dialog = notifyDialog;
		}
		if (dialog == null) {
//            chat = new PrivateChatManagerImpl(this, receiver_id);
			return;
		}
		
		loadChatHistory();
		if (dialog.getType().equals(QBDialogType.PRIVATE)) {
			opponentID = QBDialogService.getOpponentIDForPrivateDialog(dialog);
//            chat = new PrivateChatManagerImpl(this, receiver_id);
		}
		if (dialog.getType().equals(QBDialogType.GROUP)) {
			opponentID = -1;
		}
	}
	
	private void initEvent() {
		btnSend.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				sendMessage();
			}
        });
	}
	
	private void sendMessage() {
		String message = edtMessage.getText().toString();
		if (message == null || message.equals("")) {
			return;
		}
		if (opponentID == -1 && dialog == null)
			return;
		
		Log.e("send message", message);
		edtMessage.setText("");
		ChatItem chatItem = new ChatItem(message, GlobalData.myUser.qbUser.getId());
		chatAdapter.add(chatItem);
		
        QBChatMessage chatMessage = new QBChatMessage();
        chatMessage.setBody(message);
        chatMessage.setProperty(QBConst.PROPERTY_SAVE_TO_HISTORY, "1");
        chatMessage.setProperty(QBConst.PROPERTY_NOTIFICATION_TYPE, "1");

        if (opponentID == -1) {
        	GlobalData.qbGroupChatService.sendMessage(dialog, message);
        } else {
        	GlobalData.qbPrivateChatService.sendMessage(opponentID, message);
        }
        
        if (dialog != null) {
        	
        	dialog.setUnreadMessageCount(0);
        	dialog.setLastMessage(message);
        	dialog.setLastMessageDateSent(TimeUtility.getCurrentTimeStamp() / 1000);
        	
			ArrayList<QBDialog> arrDialog = new ArrayList<QBDialog>();

	        for (QBDialog dialog: GlobalData.myUser.arrDialogs) {
				arrDialog.add(dialog);
			}
	        
        	for (int i = 0; i < arrDialog.size(); i++) {
        		QBDialog tmpDialog = arrDialog.get(i);
        		if (tmpDialog.getDialogId().equals(dialog.getDialogId())) {
    				QBDialog dialog = GlobalData.myUser.arrDialogs.get(i);
    				GlobalData.myUser.arrDialogs.remove(i);
    				GlobalData.myUser.arrDialogs.add(0, dialog);
    				break;
        		}
        	}
        }
        
		MainActivity.getInstatnce().updateChats();
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle presses on the action bar items
	    switch (item.getItemId()) {
	        case android.R.id.home:
	        	opponentID = -1;
	        	dialog = null;
	        	notifyDialog = null;
	        	finish();
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
	
	private void loadChatHistory() {
        QBCustomObjectRequestBuilder customObjectRequestBuilder = new QBCustomObjectRequestBuilder();
        customObjectRequestBuilder.eq("sort_desc", "date_sent");
        customObjectRequestBuilder.setPagesLimit(50);

        QBChatService.getDialogMessages(dialog, customObjectRequestBuilder, new QBEntityCallbackImpl<ArrayList<QBChatHistoryMessage>>() {
            @Override
            public void onSuccess(ArrayList<QBChatHistoryMessage> messages, Bundle args) {
                for(QBChatHistoryMessage msg : messages) {
                	
                	if (!msg.getBody().equals("") && !msg.getBody().equals("null") && msg.getBody() != null) {
                    	ChatItem chatItem = new ChatItem(msg.getBody(), msg.getSenderId(), msg.getDateSent());
                    	arrChatItems.add(0, chatItem);
                    }

//                	addMessage(msg);
                }
                
                chatAdapter.notifyDataSetChanged();
                isLoadedChatHistory = true;
                
                if (dialog != null) {
            		dialog.setUnreadMessageCount(0);
                	MainActivity.getInstatnce().updateChats();	
            	}
            }

            @Override
            public void onError(List<String> errors) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(ChatActivity.this);
                dialog.setMessage("load chat history errors: " + errors).create().show();
            }
        });
    }
	
    public void addMessage(final QBChatMessage message) {
    	final ChatItem chatItem = new ChatItem(message.getBody(), message.getSenderId());
    	runOnUiThread(new Runnable() {
			@Override
			public void run() {
	    		MainActivity.getInstatnce().updateChats();
				arrChatItems.add(chatItem);
				chatAdapter.notifyDataSetChanged();
				if (dialog == null) return;
				
		        dialog.setUnreadMessageCount(0);
//		        dialog.set
		        
				ArrayList<QBDialog> arrDialog = new ArrayList<QBDialog>();

		        for (QBDialog dialog: GlobalData.myUser.arrDialogs) {
					arrDialog.add(dialog);
				}
		        
	        	for (int i = 0; i < arrDialog.size(); i++) {
	        		QBDialog tmpDialog = arrDialog.get(i);
	        		if (tmpDialog.getDialogId().equals(dialog.getDialogId())) {
	    				QBDialog dialog = GlobalData.myUser.arrDialogs.get(i);
	    				GlobalData.myUser.arrDialogs.remove(i);
	    				GlobalData.myUser.arrDialogs.add(0, dialog);
	        			break;
	        		}
	        	}
	        	
            	MainActivity.getInstatnce().updateChats();
            	
		        QBCustomObjectRequestBuilder customObjectRequestBuilder = new QBCustomObjectRequestBuilder();
		        customObjectRequestBuilder.setPagesLimit(100);
		        ArrayList<String> messageIDs = new ArrayList<String>();
		        messageIDs.add(message.getId());
		        customObjectRequestBuilder.in("_id", messageIDs);

				QBChatService.getDialogMessages(dialog, customObjectRequestBuilder, new QBEntityCallbackImpl<ArrayList<QBChatHistoryMessage>>() {
		            @Override
		            public void onSuccess(ArrayList<QBChatHistoryMessage> messages, Bundle args) {
//		            	dialog.setUnreadMessageCount(0);
		            }

		            @Override
		            public void onError(List<String> errors) {
		                AlertDialog.Builder dialog = new AlertDialog.Builder(ChatActivity.this);
		                dialog.setMessage("load chat history errors: " + errors).create().show();
		            }
		        });
			}
		});
    }
    
    @Override
    public void onBackPressed() {
    	opponentID = -1;
    	dialog = null;
    	notifyDialog = null;
    	
    	super.onBackPressed();
    }
}