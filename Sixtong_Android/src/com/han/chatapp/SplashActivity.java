package com.han.chatapp;

import java.util.List;

import com.han.chatapp.push.PlayServicesHelper;
import com.han.quickblox.module.QBMyUserService;
import com.han.quickblox.module.QBConst;
import com.han.utility.DialogUtility;
import com.han.utility.PreferenceUtility;
import com.quickblox.auth.QBAuth;
import com.quickblox.auth.model.QBSession;
import com.quickblox.chat.QBChatService;
import com.quickblox.core.QBEntityCallbackImpl;
import com.quickblox.core.QBSettings;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class SplashActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
        
		initWidget();
		initValue();
		initEvent();
	}
	
	private void initWidget() {

	}
	
	private void initValue() {
        QBSettings.getInstance().fastConfigInit(QBConst.APP_ID, QBConst.AUTH_KEY, QBConst.AUTH_SECRET);

        PlayServicesHelper playServicesHelper = new PlayServicesHelper(this);
        playServicesHelper.checkPlayService();
	}
	
	private void initEvent() {
        QBAuth.createSession(new QBEntityCallbackImpl<QBSession>() {
            @Override
            public void onSuccess(QBSession qbSession, Bundle bundle) {
            	Log.e("auth result", "sucess");
            	Log.e("qbtoken", qbSession.getToken());
            	
                if (!QBChatService.isInitialized()) {
                    QBChatService.init(App.getInstance());
                }
                
            	autoLogin();
            }

            @Override
            public void onError(List<String> errors) {
                // print errors that came from server
                DialogUtility.showLong(SplashActivity.this, errors.get(0));
            }
        });
	}

    private void autoLogin() {

		String curUserStatus = PreferenceUtility.sharedPreferences.getString(PreferenceUtility.PREFERENCE_KEY_USER_STATUS, "");
		if (!curUserStatus.equals("login")) {
			startActivity(new Intent(this, StartActivity.class));
			finish();
		} else {
	   		String curUserName = PreferenceUtility.sharedPreferences.getString(PreferenceUtility.PREFERENCE_KEY_CUR_USER_NAME, "");
			String curPassword = PreferenceUtility.sharedPreferences.getString(PreferenceUtility.PREFERENCE_KEY_CUR_USER_PASSWORD, "");
			new QBMyUserService(this, false).login(curUserName, curPassword);
		}
    }
}