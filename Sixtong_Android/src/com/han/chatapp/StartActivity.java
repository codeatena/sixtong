package com.han.chatapp;

import com.han.chatapp.user.LoginActivity;
import com.han.chatapp.user.SignupActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class StartActivity extends Activity {
	
	Button btnLogin;
	Button btnSignup;
	
	public static StartActivity instance;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_start);
		instance = this;
		
		initWidget();
		initValue();
		initEvent();
	}
	
	private void initWidget() {
		btnLogin = (Button) findViewById(R.id.start_login_button);
		btnSignup = (Button) findViewById(R.id.start_signup_button);
	}
	
	private void initValue() {
		
	}
	
	private void initEvent() {
		btnLogin.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(StartActivity.this, LoginActivity.class));
//				finish();
			}
        });
		btnSignup.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(StartActivity.this, SignupActivity.class));
//				finish();
			}
        });
	}
}
