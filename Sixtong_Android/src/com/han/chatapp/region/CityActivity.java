package com.han.chatapp.region;

import java.util.ArrayList;

import com.han.chatapp.GlobalData;
import com.han.chatapp.R;
import com.han.quickblox.module.QBUserService;
import com.han.widget.listView.CellItem;
import com.han.widget.listView.MyListAdapter;
import com.han.widget.listView.MyListItem;
import com.han.widget.listView.SectionItem;

import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class CityActivity extends Activity {

	ListView lstCity;
	MyListAdapter adpCity;
	
	ArrayList<MyListItem> arrItem;
	ArrayList<City> arrCity;
	
	public static Country curCountry;
	public static State curState;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_simple_list);

	    ActionBar actionBar = getActionBar();
	    actionBar.setHomeButtonEnabled(true);
	    
	    initWidget();
	    initValue();
	    initEvent();
	}
	
	private void initWidget() {
		lstCity = (ListView) findViewById(R.id.listView);
	}
	
	private void initValue() {
		arrCity = curState.arrCity;
		
		arrItem = new ArrayList<MyListItem>();
		arrItem.add(new SectionItem(""));
		
		City curCity = null;
		
		if (GlobalData.myUser.where != null && !GlobalData.myUser.where.equals("")) {
			String[] regions = GlobalData.myUser.where.split(", ");
			String countryName = regions[0];
			String stateName = null;
			String cityName = null;
			
			if (regions.length > 1) stateName = regions[1]; 
			if (regions.length > 2) cityName = regions[2];
			
			if (countryName.equals(curCountry.name)) {
				if (stateName != null) {
					if (stateName.equals(curState.name)) {
						if (cityName != null) {
							curCity = getCityFromName(cityName);
							arrItem.add(new CellItem(cityName, "selected", false));
							arrItem.add(new SectionItem(""));
						}
					}	
				}
			}
		}
		
		for (City city: arrCity) {
			if (curCity != null) {
				if (!city.name.equals(curCity.name)) {
					arrItem.add(new CellItem(city.name, "", false));
				}
			} else {
				arrItem.add(new CellItem(city.name, "", false));
			}
		}
		
		adpCity = new MyListAdapter(this, arrItem);
		lstCity.setAdapter(adpCity);
		lstCity.setDividerHeight(0);
	}
	
	private void initEvent() {
		lstCity.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				CellItem cellItem = (CellItem) adpCity.getItem(position);
				new QBUserService(CityActivity.this).updateMyRegion(curCountry.name + ", " + curState.name + ", " + cellItem.title);
			}
		});
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle presses on the action bar items
	    switch (item.getItemId()) {
	        case android.R.id.home:
	        	finish();
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
	
	private City getCityFromName(String cityName) {
		
		for (City city: arrCity) {
			if (city.name.equals(cityName)) {
				return city;
			}
		}
		
		return null;
	}
}