package com.han.chatapp.region;

import java.util.ArrayList;

import com.han.chatapp.GlobalData;
import com.han.chatapp.R;
import com.han.chatapp.service.LocationAsyncTask;
import com.han.quickblox.module.QBUserService;
import com.han.widget.listView.CellItem;
import com.han.widget.listView.MyListAdapter;
import com.han.widget.listView.MyListItem;
import com.han.widget.listView.SectionItem;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class CountryActivity extends Activity {

	final int REQUST_CODE_STATE = 1003;
	
	ListView lstCountry;
	MyListAdapter adpCountry;
	
	ArrayList<MyListItem> arrItem;
	ArrayList<Country> arrCountry;
	
	Country curCountry;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_simple_list);

	    ActionBar actionBar = getActionBar();
	    actionBar.setHomeButtonEnabled(true);
	    
	    initWidget();
	    initValue();
	    initEvent();
	}
	
	private void initWidget() {
		lstCountry = (ListView) findViewById(R.id.listView);
	}
	
	private void initValue() {
		arrCountry = CountryXMLParser.parseCountry();
		arrItem = new ArrayList<MyListItem>();
		
		arrItem.add(new SectionItem(""));
		arrItem.add(new SectionItem(getResources().getString(R.string.location)));
		arrItem.add(new CellItem(getResources().getString(R.string.locating), "", false));
		arrItem.add(new SectionItem(""));
		arrItem.add(new SectionItem(getResources().getString(R.string.all_regions)));
		
		curCountry = null;
		
		if (GlobalData.myUser.where != null && !GlobalData.myUser.where.equals("")) {
			String[] regions = GlobalData.myUser.where.split(", ");
			String countryName = regions[0];
			
			curCountry = getCountryFromName(countryName);
			arrItem.add(new CellItem(countryName, "selected", false));
			arrItem.add(new SectionItem(""));
		}
		
		for (Country country: arrCountry) {
			if (curCountry != null) {
				if (!country.name.equals(curCountry.name)) {
					arrItem.add(new CellItem(country.name, "", false));
				}	
			} else {
				arrItem.add(new CellItem(country.name, "", false));
			}
		}
		
		adpCountry = new MyListAdapter(this, arrItem);
		lstCountry.setAdapter(adpCountry);
		lstCountry.setDividerHeight(0);
	}
	
	private void initEvent() {
		new LocationAsyncTask(this).execute(LocationAsyncTask.ACTION_GET_ADDRESS,
				Double.toString(GlobalData.curLat), Double.toString(GlobalData.curLng));
		
//		new LocationAsyncTask(this).execute(LocationAsyncTask.ACTION_GET_ADDRESS,
//				Double.toString(-1.474931), Double.toString(20.980162));
		
		lstCountry.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				CellItem cellItem = (CellItem) adpCountry.getItem(position);
				
				Country country = getCountryFromName(cellItem.title);
				if (country != null) {
					if (country.arrState.size() > 0) {
						StateActivity.curCountry = country;
						CountryActivity.this.startActivityForResult(new Intent(CountryActivity.this, StateActivity.class), REQUST_CODE_STATE);
					} else {
						new QBUserService(CountryActivity.this).updateMyRegion(cellItem.title);
					}
				} else {
					new QBUserService(CountryActivity.this).updateMyRegion(cellItem.title);
				}
			}
		});
	}
	
	public void setCurrentAddress(String address) {
		CellItem cellItem = (CellItem) adpCountry.getItem(2);
		cellItem.title = address;
		
		adpCountry.notifyDataSetChanged();
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle presses on the action bar items
	    switch (item.getItemId()) {
	        case android.R.id.home:
	        	finish();
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
	
	private Country getCountryFromName(String countryName) {
		for (Country country: arrCountry) {
			if (country.name.equals(countryName)) {
				return country;
			}
		}
		
		return null;
	}
	
	protected void onActivityResult(int requestCode, int resultCode, Intent imageReturedIntent) {
		if (requestCode == REQUST_CODE_STATE) {
            if (resultCode == RESULT_OK) {
            	setResult(RESULT_OK);
            	finish();
            }
        }
	}
}