package com.han.chatapp.region;

import java.util.ArrayList;

import com.han.chatapp.GlobalData;
import com.han.chatapp.R;
import com.han.quickblox.module.QBUserService;
import com.han.widget.listView.CellItem;
import com.han.widget.listView.MyListAdapter;
import com.han.widget.listView.MyListItem;
import com.han.widget.listView.SectionItem;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class StateActivity extends Activity {
	
	final int REQUEST_CODE_CITY = 1005;
	
	ListView lstState;
	MyListAdapter adpState;
	
	ArrayList<MyListItem> arrItem;
	ArrayList<State> arrState;
	
	public static Country curCountry;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_simple_list);

	    ActionBar actionBar = getActionBar();
	    actionBar.setHomeButtonEnabled(true);
	    
	    initWidget();
	    initValue();
	    initEvent();
	}
	
	private void initWidget() {
		lstState = (ListView) findViewById(R.id.listView);
	}
	
	private void initValue() {
		arrState = curCountry.arrState;
		
		arrItem = new ArrayList<MyListItem>();
		arrItem.add(new SectionItem(""));
		
		State curState = null;
		
		if (GlobalData.myUser.where != null && !GlobalData.myUser.where.equals("")) {
			String[] regions = GlobalData.myUser.where.split(", ");
			String countryName = regions[0];
			String stateName = null;
			if (countryName.equals(curCountry.name)) {
				if (regions.length > 0) {
					stateName = regions[1];	
				}
				if (stateName != null) {
					curState = getStateFromName(stateName);
					arrItem.add(new CellItem(stateName, "selected", false));
					arrItem.add(new SectionItem(""));
				}
			}
		}
		
		for (State state: arrState) {
			if (curState != null) {
				if (!state.name.equals(curState.name)) {
					arrItem.add(new CellItem(state.name, "", false));
				}
			} else {
				arrItem.add(new CellItem(state.name, "", false));
			}
		}
		
		adpState = new MyListAdapter(this, arrItem);
		lstState.setAdapter(adpState);
		lstState.setDividerHeight(0);
	}
	
	private void initEvent() {
		lstState.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				CellItem cellItem = (CellItem) adpState.getItem(position);
				
				State state = getStateFromName(cellItem.title);
				if (state != null) {
					if (state.arrCity.size() > 0) {
						//StateActivity.curCountry = country;
						//CountryActivity.this.startActivityForResult(new Intent(CountryActivity.this, StateActivity.class), REQUST_CODE_STATE);
						CityActivity.curCountry = curCountry;
						CityActivity.curState = state;
						startActivityForResult(new Intent(StateActivity.this, CityActivity.class), REQUEST_CODE_CITY);
					} else {
						new QBUserService(StateActivity.this).updateMyRegion(curCountry.name + ", " + cellItem.title);
					}
				} else {
					new QBUserService(StateActivity.this).updateMyRegion(curCountry.name + ", " + cellItem.title);
				}
			}
		});
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle presses on the action bar items
	    switch (item.getItemId()) {
	        case android.R.id.home:
	        	finish();
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
	
	private State getStateFromName(String stateName) {
		
		for (State state: arrState) {
			if (state.name.equals(stateName)) {
				return state;
			}
		}
		
		return null;
	}
	
	protected void onActivityResult(int requestCode, int resultCode, Intent imageReturedIntent) {
		if (requestCode == REQUEST_CODE_CITY) {
            if (resultCode == RESULT_OK) {
            	setResult(RESULT_OK);
            	finish();
            }
        }
	}
}