package com.han.chatapp.region;

import java.util.ArrayList;
import org.xmlpull.v1.XmlPullParser;
import com.han.chatapp.App;
import com.han.chatapp.R;

import android.content.res.XmlResourceParser;

public class CountryXMLParser {

	public static ArrayList<Country> parseCountry() {
		ArrayList<Country> arrCountry = new ArrayList<Country>();

		try {
			XmlResourceParser parser = App.getInstance().getResources().getXml(R.xml.region_english);
//			parser.setInput(new StringReader(strResponse));
			
			int eventType = parser.getEventType();
			String xmlNodeName = null;
			Country country = null;
			State state = null;
			City city = null;
			
			while (eventType != XmlPullParser.END_DOCUMENT) {
				switch (eventType) {
					case XmlPullParser.START_TAG:
						xmlNodeName = parser.getName();
						if (xmlNodeName.equals("country")) {
							country = new Country();
							country.name = parser.getAttributeValue(null, "name");
						}
						if (xmlNodeName.equals("state")) {
							state = new State();
							state.name = parser.getAttributeValue(null, "name");
						}
						if (xmlNodeName.equals("city")) {
							city = new City();
							city.name = parser.getAttributeValue(null, "name");
						}
						break;
					case XmlPullParser.TEXT:
						break;
					case XmlPullParser.END_TAG:
						xmlNodeName = parser.getName();
						if (xmlNodeName.equals("country")) {
							arrCountry.add(country);
						}
						if (xmlNodeName.equals("state")) {
							country.arrState.add(state);
						}
						if (xmlNodeName.equals("city")) {
							state.arrCity.add(city);
						}
						break;
					default:
						break;
				}
				eventType = parser.next();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return arrCountry;
	}
}
