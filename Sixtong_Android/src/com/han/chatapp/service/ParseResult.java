package com.han.chatapp.service;

public class ParseResult {

	public String result = "";
	public String message = "";
	
	public ParseResult() {
		result = "";
		message = "";
	}
	
	public ParseResult(String _result) {
		result = _result;
	}
	
	public ParseResult(String _result, String _message) {
		result = _result;
		message = _message;
	}
	
	public ParseResult(boolean isSuccess) {
		if (isSuccess) {
			result = "success";
		} else {
			result = "fail";
		}
	}
	
	public ParseResult(boolean isSuccess, String _message) {
		if (isSuccess) {
			result = "success";
		} else {
			result = "fail";
		}
		message = _message;
	}
	
	public boolean isSuccess() {
		if (result.equals("success"))
			return true;
		
		return false;
	}
}