package com.han.chatapp.service;

import org.json.JSONObject;

public class Parser {
	
	public static final int SUCCESS = 1;
	public static final int FAIL = -1;
	
	public static ParseResult defaultParse(String strResponse) {
		
		ParseResult pResult = new ParseResult();
		
		try {
			JSONObject jsonObj = new JSONObject(strResponse);
				
			pResult.result = jsonObj.getString("result");
			pResult.message = jsonObj.getString("message");
			
		} catch (Exception e) {
			e.printStackTrace();
			pResult = new ParseResult(false, "json parsing error");
		}
		
		return pResult;
	}
}