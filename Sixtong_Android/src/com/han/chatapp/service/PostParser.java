package com.han.chatapp.service;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import com.han.chatapp.model.Comment;
import com.han.chatapp.model.Post;

import android.util.Log;

public class PostParser {

	public static ParseResult parseMyPost(String strResponse) {
		
		try {
			MyPostsParseResult pResult = new MyPostsParseResult();

			JSONObject jsonObj = new JSONObject(strResponse);
			
			pResult.result = jsonObj.getString("result");
			pResult.message = jsonObj.getString("message");
			
			if (!pResult.isSuccess()) 
				return pResult;
			
			if (jsonObj.isNull("posts")) 
				return pResult;
			
			JSONArray arrJsonPost = new JSONArray();
			arrJsonPost = jsonObj.getJSONArray("posts");
			
			ArrayList<Post> arrPost = new ArrayList<Post>();
			for (int i = 0; i < arrJsonPost.length(); i++) {
				JSONObject jsonPost = arrJsonPost.getJSONObject(i);
				
				Post post = new Post();
				post.id = jsonPost.getString("id");
				post.userSecretID = jsonPost.getString("user_secret_id");
				post.url = jsonPost.getString("url");
				post.text = jsonPost.getString("text");
				post.time = jsonPost.getString("time");
				post.orientation = jsonPost.getInt("orientation");
				
				arrPost.add(post);
			}
			
			pResult.arrPost = arrPost;
			return pResult;
		} catch (Exception e) {
			Log.e("json parsing error", e.getMessage());
			return new ParseResult(false, "json parsing error");
		}
	}
	
	public static ParseResult parseNewPost(String strResponse) {
	
		try {
			NewPostParseResult pResult = new NewPostParseResult();

			JSONObject jsonObj = new JSONObject(strResponse);
			
			pResult.result = jsonObj.getString("result");
			pResult.message = jsonObj.getString("message");
			
			if (!pResult.isSuccess()) 
				return pResult;
			
			JSONObject jsonPost = jsonObj.getJSONObject("new_post");
			
			Post post = new Post();
			post.userSecretID = jsonPost.getString("user_secret_id");
			post.url = jsonPost.getString("url");
			post.text = jsonPost.getString("text");
			post.time = jsonPost.getString("time");
			post.orientation = jsonPost.getInt("orientation");
			
			pResult.newPost = post;
			
			return pResult;
		} catch (Exception e) {
			Log.e("json parsing error", e.getMessage());
			return new ParseResult(false, "json parsing error");
		}
	}
	
	public static ParseResult parsePosts(String strResponse) {
		try {
			PostsParseResult pResult = new PostsParseResult();

			JSONObject jsonObj = new JSONObject(strResponse);
			
			pResult.result = jsonObj.getString("result");
			pResult.message = jsonObj.getString("message");
			
			if (!pResult.isSuccess())
				return pResult;
			if (jsonObj.isNull("posts"))
				return pResult;
			
			JSONArray arrJsonBulletins = jsonObj.getJSONArray("posts");
			ArrayList<Post> arrPost = new ArrayList<Post>();
			
			for (int i = 0; i < arrJsonBulletins.length(); i++) {
				JSONObject jsonPost = arrJsonBulletins.getJSONObject(i);
				
				Post post = new Post();
				post.id = jsonPost.getString("id");
				post.userSecretID = jsonPost.getString("user_secret_id");
				post.url = jsonPost.getString("url");
				post.text = jsonPost.getString("text");
				post.time = jsonPost.getString("time");
				post.orientation = jsonPost.getInt("orientation");
								
				if (jsonPost.isNull("comments")) {
					arrPost.add(post);
					continue;
				}
				
				JSONArray arrJsonComment = jsonPost.getJSONArray("comments");
				
				ArrayList<Comment> arrComment = new ArrayList<Comment>();
				for (int j = 0; j < arrJsonComment.length(); j++) {
					JSONObject jsonComment = arrJsonComment.getJSONObject(j);
					
					Comment comment = new Comment();
					comment.userSecretID = jsonComment.getString("user_secret_id");
					comment.text = jsonComment.getString("text");
					comment.time = jsonComment.getString("time");
					comment.fullName = jsonComment.getString("full_name");
					
					arrComment.add(comment);
				}
				
				post.arrComment = arrComment;
				arrPost.add(post);
			}
			
			pResult.arrPosts = arrPost;
			return pResult;
		} catch (Exception e) {
			Log.e("json parsing error", e.getMessage());
			return new ParseResult(false, "json parsing error");
		}
	}
}