package com.han.chatapp.service;

import java.io.File;
import java.nio.charset.Charset;

import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;

import com.han.chatapp.GlobalData;

import android.net.Uri;
import android.util.Log;


public class PostWebService extends WebService {

	public static ParseResult uploadPost(String text, String filePath, String orientation) {
		try {
			MultipartEntity mpEntity = new MultipartEntity();
			
			File file = new File(filePath);
	        Charset chars = Charset.forName("UTF-8");

			mpEntity.addPart("action", new StringBody("upload_post"));
			mpEntity.addPart("secret_id", new StringBody(GlobalData.myUser.secretID));
	        mpEntity.addPart("text", new StringBody(text, chars));
	        mpEntity.addPart("orientation", new StringBody(orientation));
	        mpEntity.addPart("uploaded_file", new FileBody(file));
	        
	        Log.e("method", "upload post");
	        String strResponse = WebService.callHttpRequestMultiPart(URL_API, mpEntity);
	        
	        return PostParser.parseNewPost(strResponse);
	        
		} catch (Exception e) {
			e.printStackTrace();
			return new ParseResult(false, "Coudnt not upload!");
		}
	}
	
	public static ParseResult getMyPosts() {
		String uri = Uri.parse(URL_API)
                .buildUpon()
                .appendQueryParameter("action", PostAsyncTask.ACTION_GET_MY_POST)
                .appendQueryParameter("secret_id", GlobalData.myUser.secretID)
                .build().toString();
		
		Log.e("url", uri);
		Log.e("method", "get my posts");

		String strResponse = WebService.callHttpRequestGeneral(uri, "GET", null);
		
		if (strResponse == null || strResponse.equals("")) {
			ParseResult pResult = new ParseResult(false, "failed call web service");
			return pResult;
		}
		
		return PostParser.parseMyPost(strResponse);
	}
	
	public static ParseResult getPosts(String secret_ids) {
		String uri = Uri.parse(URL_API)
                .buildUpon()
                .appendQueryParameter("action", PostAsyncTask.ACTION_GET_POSTS)
                .appendQueryParameter("secret_ids", secret_ids)
                .build().toString();
		
		Log.e("url", uri);
		Log.e("method", "get bulletins");
		
		String strResponse = WebService.callHttpRequestGeneral(uri, "GET", null);
		
		if (strResponse == null || strResponse.equals("")) {
			ParseResult pResult = new ParseResult(false, "failed call web service");
			return pResult;
		}
		
		return PostParser.parsePosts(strResponse);		
	}
	
	public static ParseResult newComment(String post_id, String text) {
		String uri = Uri.parse(URL_API)
                .buildUpon()
                .appendQueryParameter("action", PostAsyncTask.ACTION_NEW_COMMENT)
                .appendQueryParameter("secret_id", GlobalData.myUser.secretID)
                .appendQueryParameter("full_name", GlobalData.myUser.qbUser.getFullName())
                .appendQueryParameter("post_id", post_id)
                .appendQueryParameter("text", text)
                .build().toString();
		
		Log.e("url", uri);
		Log.e("method", "new comment");
		
		String strResponse = WebService.callHttpRequestGeneral(uri, "GET", null);
		
		if (strResponse == null || strResponse.equals("")) {
			ParseResult pResult = new ParseResult(false, "failed call web service");
			return pResult;
		}
		
		return Parser.defaultParse(strResponse);
	}
}
