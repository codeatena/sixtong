package com.han.chatapp.service;

import com.han.chatapp.R;
import com.han.chatapp.post.NewCommentActivity;
import com.han.chatapp.post.MyPostsActivity;
import com.han.chatapp.post.NewPostActivity;
import com.han.chatapp.post.PostsActivity;
import com.han.chatapp.user.ProfileActivity;
import com.han.utility.DialogUtility;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

public class PostAsyncTask extends AsyncTask<String, Integer, ParseResult> {
	
	public static final int SUCCESS = 1;
	public static final int FAIL = -1;
	
	public Context parent;
	public ProgressDialog dlgLoading;
	public String title;
	
	public String curAction;
	
	public static String ACTION_UPLOAD_POST = "upload_post";
	public static String ACTION_GET_MY_POST = "my_post";
	public static String ACTION_GET_POSTS = "get_posts";
	public static String ACTION_NEW_COMMENT = "new_comment";

	public PostAsyncTask(Context _parent) {
		parent = _parent;
		title = parent.getResources().getString(R.string.please_wait);
	}
	
	public PostAsyncTask(Context _parent, String _title) {
		parent = _parent;
		title = _title;
	}
	
	protected void onPreExecute() {
		dlgLoading = new ProgressDialog(parent);
    	dlgLoading.setMessage(title);
    	dlgLoading.setCanceledOnTouchOutside(false);
    	dlgLoading.setCancelable(false);
    	
    	if (title != null) {
    		dlgLoading.show();
    	}
	}
	
	@Override
	protected ParseResult doInBackground(String... params) {
		ParseResult pResult = new ParseResult(false);
		
		curAction = params[0];
		if (curAction.equals(ACTION_UPLOAD_POST)) {
			pResult = PostWebService.uploadPost(params[1], params[2], params[3]);
		}
		if (curAction.equals(ACTION_GET_MY_POST)) {
//			pResult = PostWebService.uploadPost(params[1], params[2]);
			pResult = PostWebService.getMyPosts();
		}
		if (curAction.equals(ACTION_GET_POSTS)) {
			pResult = PostWebService.getPosts(params[1]);
		}
		if (curAction.equals(ACTION_NEW_COMMENT)) {
			pResult = PostWebService.newComment(params[1], params[2]);
		}
		
		return pResult;
	}
	
	protected void onPostExecute(ParseResult pResult) {
		if (dlgLoading.isShowing())
			dlgLoading.dismiss();
		
		if (!pResult.isSuccess()) {
			DialogUtility.showGeneralAlert(parent, "Error", pResult.message);
			return;
		} else {
			if (curAction.equals(ACTION_UPLOAD_POST)) {
				NewPostActivity newPostActivity = (NewPostActivity) parent;
				NewPostParseResult newPostParseResult = (NewPostParseResult) pResult;
				newPostActivity.succesUpload(newPostParseResult.newPost);
			}
			if (curAction.equals(ACTION_GET_MY_POST)) {
				MyPostsActivity myPostActivity = (MyPostsActivity) parent;
				MyPostsParseResult myPostsParseResult = (MyPostsParseResult) pResult;
				myPostActivity.successGetMyPosts(myPostsParseResult.arrPost);
			}
			if (curAction.equals(ACTION_GET_POSTS)) {
				if (parent instanceof PostsActivity) {
					PostsActivity postsActivity = (PostsActivity) parent;
					PostsParseResult postsParseResult = (PostsParseResult) pResult;
					postsActivity.successGetPosts(postsParseResult.arrPosts);
				}
				if (parent instanceof ProfileActivity) {
					ProfileActivity profileActivity = (ProfileActivity) parent;
					PostsParseResult postsParseResult = (PostsParseResult) pResult;
					profileActivity.successGetPosts(postsParseResult.arrPosts);
				}
			}
			if (curAction.equals(ACTION_NEW_COMMENT)) {
				NewCommentActivity newCommentActivity = (NewCommentActivity) parent;
				newCommentActivity.successNewComment();
			}
		}
	}
	
	protected void onProgressUpdate(Integer... progress) {
		
	}
}