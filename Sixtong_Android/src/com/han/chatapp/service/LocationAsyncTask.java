package com.han.chatapp.service;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import com.han.chatapp.R;
import com.han.chatapp.region.CountryActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;

public class LocationAsyncTask extends AsyncTask<String, Integer, String> {

	public static final int SUCCESS = 1;
	public static final int FAIL = -1;
	
	public Context parent;
	public ProgressDialog dlgLoading;
	public String title;
	
	public String curAction;
	
	public static String ACTION_GET_ADDRESS = "action_get_address";

	public LocationAsyncTask(Context _parent) {
		parent = _parent;
		title = parent.getResources().getString(R.string.please_wait);
	}
	
	public LocationAsyncTask(Context _parent, String _title) {
		parent = _parent;
		title = _title;
	}

	protected void onPreExecute() {
		dlgLoading = new ProgressDialog(parent);
    	dlgLoading.setMessage(title);
    	dlgLoading.setCanceledOnTouchOutside(false);
    	dlgLoading.setCancelable(false);
    	
    	if (title != null) {
    		dlgLoading.show();
    	}
	}
	
	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub
		
		String ret = null;
		curAction = params[0];
		if (curAction.equals(ACTION_GET_ADDRESS)) {
			Geocoder geocoder = new Geocoder(parent, Locale.getDefault());
	        // Get the current location from the input parameter list
	        Location loc = new Location("");
	        loc.setLatitude(Double.parseDouble(params[1]));
	        loc.setLongitude(Double.parseDouble(params[2]));
	        
	        List<Address> addresses = null;
	        try {

	            addresses = geocoder.getFromLocation(loc.getLatitude(), loc.getLongitude(), 1);
	        } catch (IOException e1) {
	        	Log.e("LocationSampleActivity",
	                "IO Exception in getFromLocation()");
	        	e1.printStackTrace();
	        	ret = ("IO Exception trying to get address");
	        } catch (IllegalArgumentException e2) {
	        	String errorString = "Illegal arguments " + Double.toString(loc.getLatitude()) +
	                " , " + Double.toString(loc.getLongitude()) + " passed to address service";
	        	
	        	Log.e("LocationSampleActivity", errorString);
	        	e2.printStackTrace();
	        	ret = errorString;
	        }
	        if (addresses != null && addresses.size() > 0) {
	            Address address = addresses.get(0);
	            
	            String country = address.getCountryName();
	            String addressText = country;
	            String state = address.getAdminArea();
	            String city = address.getLocality();
	            
	            if (state != null && !state.equals("") && !state.equals("null")) {
	            	addressText = addressText + ", " + state;
	            }
	            if (city != null && !city.equals("") && !city.equals("null")) {
	            	addressText = addressText + ", " + city;
	            }
	            
	            ret = addressText;
	        } else {
	            ret = "No address found";
	        }
		}
		
		return ret;
    }
	
    @Override
    protected void onPostExecute(String address) {
    	if (dlgLoading.isShowing()) {
    		dlgLoading.dismiss();
    	}
    	
    	CountryActivity countryActivity = (CountryActivity) parent;
    	countryActivity.setCurrentAddress(address);
    }
}