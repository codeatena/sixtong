package com.han.chatapp.service;

import com.han.chatapp.R;
import com.han.chatapp.user.LoginActivity;
import com.han.chatapp.user.SignupActivity;
import com.han.chatapp.user.VerifyActivity;
import com.han.utility.DialogUtility;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

public class UserAsyncTask extends AsyncTask<String, Integer, ParseResult> {

	public static final int SUCCESS = 1;
	public static final int FAIL = -1;
	
	public Context parent;
	public ProgressDialog dlgLoading;
	public String title;
	
	public String curAction;
	
	public static String ACTION_SIGNUP = "signup";
	public static String ACTION_RESEND = "resend";
	public static String ACTION_VERIFY = "verify";
	public static String ACTION_GET_SECRET_ID = "get_secret_id";

	public UserAsyncTask(Context _parent) {
		parent = _parent;
		title = parent.getResources().getString(R.string.please_wait);
	}
	
	public UserAsyncTask(Context _parent, String _title) {
		parent = _parent;
		title = _title;
	}

	@Override
	protected ParseResult doInBackground(String... params) {
		// TODO Auto-generated method stub
		ParseResult pResult = new ParseResult(false);

		curAction = params[0];
		if (curAction.equals(ACTION_SIGNUP)) {
			pResult = UserWebService.signup(params[1], params[2], params[3], params[4]);
		}
		if (curAction.equals(ACTION_RESEND)) {
			pResult = UserWebService.resend(params[1], params[2]);
		}
		if (curAction.equals(ACTION_VERIFY)) {
			pResult = UserWebService.verify(params[1], params[2], params[3]);
		}
		if (curAction.equals(ACTION_GET_SECRET_ID)) {
			pResult = UserWebService.getSecretIDByPhoneNumber(params[1]);
		}
		
		return pResult;
	}
	
	protected void onPreExecute() {
		dlgLoading = new ProgressDialog(parent);
    	dlgLoading.setMessage(title);
    	dlgLoading.setCanceledOnTouchOutside(false);
    	dlgLoading.setCancelable(false);
    	
    	if (title != null) {
    		dlgLoading.show();
    	}
	}
	
	protected void onPostExecute(ParseResult pResult) {
		if (dlgLoading.isShowing())
			dlgLoading.dismiss();
		
		if (curAction.equals(ACTION_GET_SECRET_ID)) {
			
			LoginActivity loginActivity = (LoginActivity) parent;
			if (pResult.isSuccess()) {
				loginActivity.successGetSecretID(pResult.message);
			} else {
				loginActivity.successGetSecretID(null);
			}
			
			return;
		}
		
		if (!pResult.isSuccess()) {
			DialogUtility.showGeneralAlert(parent, "Error", pResult.message);
		} else {
			if (curAction.equals(ACTION_SIGNUP)) {
				SignupActivity signupActivity = (SignupActivity) parent;
				signupActivity.procVerify(pResult.message);
			}
			if (curAction.equals(ACTION_RESEND)) {
				VerifyActivity verifyActivity = (VerifyActivity) parent;
				verifyActivity.receiveNewSecretID(pResult.message);
			}
			if (curAction.equals(ACTION_VERIFY)) {
				VerifyActivity verifyActivity = (VerifyActivity) parent;
				verifyActivity.sucessVerify(pResult.message);
			}
		}
	}
	
	protected void onProgressUpdate(Integer... progress) {
		
	}
}
