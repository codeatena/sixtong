package com.han.chatapp.service;

import org.json.JSONObject;

import android.util.Log;

public class UserParser extends Parser {

	public static String strSignupError = "";
	
	public static ParseResult parseSignup(String strResponse) {
		ParseResult pResult = new ParseResult();
		try {
			JSONObject jsonObj = new JSONObject(strResponse);
			
			pResult.result = jsonObj.getString("result");
			pResult.message = jsonObj.getString("message");
			
			return pResult;
		} catch (Exception e) {
			Log.e("json parsing error", e.getMessage());
			
			pResult = new ParseResult(false, "json parsing error");
			return pResult;
		}
	}
	
	public static ParseResult parseResend(String strResponse) {
		ParseResult pResult = new ParseResult();
		try {
			JSONObject jsonObj = new JSONObject(strResponse);
			
			pResult.result = jsonObj.getString("result");
			pResult.message = jsonObj.getString("message");
			
			return pResult;
		} catch (Exception e) {
			Log.e("json parsing error", e.getMessage());
			
			pResult = new ParseResult(false, "json parsing error");
			return pResult;
		}
	}
	
	public static ParseResult parseVerify(String strResponse) {
		ParseResult pResult = new ParseResult();
		try {
			JSONObject jsonObj = new JSONObject(strResponse);
			
			pResult.result = jsonObj.getString("result");
			pResult.message = jsonObj.getString("message");
			
			return pResult;
		} catch (Exception e) {
			Log.e("json parsing error", e.getMessage());
			
			pResult = new ParseResult(false, "json parsing error");
			return pResult;
		}
	}
}