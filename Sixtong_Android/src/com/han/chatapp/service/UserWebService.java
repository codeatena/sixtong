package com.han.chatapp.service;

import android.net.Uri;
import android.util.Log;

public class UserWebService extends WebService {
	
	public static ParseResult signup(String user_name, String country_code, String phone_number, String password) {
		String uri = Uri.parse(URL_API)
                .buildUpon()
                .appendQueryParameter("action", UserAsyncTask.ACTION_SIGNUP)
                .appendQueryParameter("user_name", user_name)
                .appendQueryParameter("country_code", country_code)
                .appendQueryParameter("phone_number", phone_number)
                .appendQueryParameter("password", password)
                .build().toString();
		
		Log.e("url", uri);
		Log.e("method", "signup");

		String strResponse = WebService.callHttpRequestGeneral(uri, "GET", null);
		
		if (strResponse == null || strResponse.equals("")) {
			ParseResult pResult = new ParseResult(false, "failed call web service");
			return pResult;
		}
		
		return UserParser.parseSignup(strResponse);
	}
	
	public static ParseResult resend(String country_code, String phone_number) {
		String uri = Uri.parse(URL_API)
                .buildUpon()
                .appendQueryParameter("action", UserAsyncTask.ACTION_RESEND)
                .appendQueryParameter("country_code", country_code)
                .appendQueryParameter("phone_number", phone_number)
                .build().toString();
		
		Log.e("url", uri);
		Log.e("method", "resend");
		
		String strResponse = WebService.callHttpRequestGeneral(uri, "GET", null);
		
		if (strResponse == null || strResponse.equals("")) {
			ParseResult pResult = new ParseResult(false, "failed call web service");
			return pResult;
		}
		
		return UserParser.parseResend(strResponse);
	}
	
	public static ParseResult verify(String country_code, String phone_number, String secret_id) {
		String uri = Uri.parse(URL_API)
                .buildUpon()
                .appendQueryParameter("action", UserAsyncTask.ACTION_VERIFY)
                .appendQueryParameter("country_code", country_code)
                .appendQueryParameter("phone_number", phone_number)
                .appendQueryParameter("secret_id", secret_id)
                .build().toString();
		
		Log.e("url", uri);
		Log.e("method", "resend");
		
		String strResponse = WebService.callHttpRequestGeneral(uri, "GET", null);
		
		if (strResponse == null || strResponse.equals("")) {
			ParseResult pResult = new ParseResult(false, "failed call web service");
			return pResult;
		}
		
		return UserParser.parseVerify(strResponse);
	}
	
	public static ParseResult getSecretIDByPhoneNumber(String phone_number) {
		String uri = Uri.parse(URL_API)
                .buildUpon()
                .appendQueryParameter("action", UserAsyncTask.ACTION_GET_SECRET_ID)
                .appendQueryParameter("phone_number", phone_number)
                .build().toString();
		
		Log.e("url", uri);
		Log.e("method", "get_secret_id");
		
		String strResponse = WebService.callHttpRequestGeneral(uri, "GET", null);

		if (strResponse == null || strResponse.equals("")) {
			ParseResult pResult = new ParseResult(false, "failed call web service");
			return pResult;
		}
		
		return Parser.defaultParse(strResponse);
	}
}