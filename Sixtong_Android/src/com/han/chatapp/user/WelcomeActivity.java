package com.han.chatapp.user;

import com.han.chatapp.R;
import com.han.quickblox.module.QBMyUserService;
import com.han.quickblox.module.QBConst;
import com.han.utility.ImageUtility;
import com.han.utility.PreferenceUtility;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class WelcomeActivity extends Activity {

	ImageView imgPhoto;
	Button btnStart;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_welcome);

	    initWidget();
	    initValue();
	    initEvent();
	}
	
	private void initWidget() {
		imgPhoto = (ImageView) findViewById(R.id.welcome_photo_imageView);
		btnStart = (Button) findViewById(R.id.start_button);
	}
	
	private void initValue() {
		btnStart.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				String secretID = PreferenceUtility.sharedPreferences.getString(PreferenceUtility.PREFERENCE_KEY_USER_SECRET_ID, "");
				String password = PreferenceUtility.sharedPreferences.getString(PreferenceUtility.PREFERENCE_KEY_CUR_USER_PASSWORD, "");
				new QBMyUserService(WelcomeActivity.this).login(QBConst.FORWARD_LOGIN_STRING + secretID, password);
//				startActivity(new Intent(WelcomeActivity.this, MainActivity.class));
//				finish();
			}
        });
        String strPhoto = PreferenceUtility.sharedPreferences.getString(PreferenceUtility.PREFERENCE_KEY_USER_PHOTO, "");
        if (!strPhoto.equals("")) {
        	imgPhoto.setImageBitmap(ImageUtility.StringToBitmap(strPhoto));
        }
	}
	
	private void initEvent() {
		
	}
}