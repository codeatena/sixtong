package com.han.chatapp.user;

import com.han.chatapp.App;
import com.han.chatapp.R;
import com.han.chatapp.service.UserAsyncTask;
import com.han.quickblox.module.QBMyUserService;
import com.han.quickblox.module.QBConst;
import com.han.utility.DialogUtility;
import com.han.utility.PreferenceUtility;
import com.quickblox.chat.QBChatService;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class LoginActivity extends Activity {

	public static LoginActivity instance;
	
	EditText edtSecretID;
	EditText edtPassword;
	
	Button btnReturn;
	Button btnLogin;
	
	int nLoginStage;
	
	final int LOGIN_BY_SECRET_ID = 0;
	final int LOGIN_BY_PHONE_NUMBER = 1;
	
//	Button btnSignup;
	
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

	    ActionBar actionBar = LoginActivity.this.getActionBar();
	    actionBar.setHomeButtonEnabled(true);
	    
	    initWidget();
	    initValue();
	    initEvent();
	    
	    instance = this;
	}
	
	private void initWidget() {
		edtSecretID = (EditText) findViewById(R.id.phone_editText);
		edtPassword = (EditText) findViewById(R.id.password_editText);
		
		btnReturn = (Button) findViewById(R.id.return_button);
		btnLogin = (Button) findViewById(R.id.login_button);
//		btnSignup = (Button) findViewById(R.id.signup_button);
	}
	
	private void initValue() {
//		String curStatus = PreferenceUtility.sharedPreferences.getString(PreferenceUtility.PREFERENCE_KEY_USER_STATUS, "");
		String userName = PreferenceUtility.sharedPreferences.getString(PreferenceUtility.PREFERENCE_KEY_CUR_USER_NAME, "");
		String secretID = "";
		
		if (userName.length() > QBConst.FORWARD_LOGIN_STRING.length()) {
			secretID = userName.substring(QBConst.FORWARD_LOGIN_STRING.length());	
		}
		
		edtSecretID.setText(secretID);
	}
	
	private void initEvent() {
		btnLogin.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
//				actionLoginBySecretCode();
//				successLogin();
				actionLoginByPhoneNumber();
			}
        });
		
		btnReturn.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();				
			}
        });
		
       
        if (!QBChatService.isInitialized()) {
            QBChatService.init(App.getInstance());
        }

//		btnSignup.setOnClickListener(new Button.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				startActivity(new Intent(LoginActivity.this, SignupActivity.class));
//			}
//        });
	}
	
	public void actionLoginByPhoneNumber() {
		
		String phoneNumber = edtSecretID.getText().toString();
		String password = edtPassword.getText().toString();
		
		if (phoneNumber.equals("") || phoneNumber == null) {
			DialogUtility.showGeneralAlert(LoginActivity.this, "Alert", "Please input your secret code or phone number.");
			return;
		}
		if (password.equals("") || password == null) {
			DialogUtility.showGeneralAlert(LoginActivity.this, "Alert", "Please input your password.");
			return;
		}
		
		nLoginStage = LOGIN_BY_PHONE_NUMBER;
		new UserAsyncTask(LoginActivity.this).execute(UserAsyncTask.ACTION_GET_SECRET_ID, phoneNumber);
	}
		
	public void actionLoginBySecretCode() {

		String secretID = edtSecretID.getText().toString();
		String password = edtPassword.getText().toString();
		
		if (secretID.equals("") || secretID == null) {
			DialogUtility.showGeneralAlert(LoginActivity.this, "Alert", "Please input your secret code or phone number.");
			return;
		}
		if (password.equals("") || password == null) {
			DialogUtility.showGeneralAlert(LoginActivity.this, "Alert", "Please input your password.");
			return;
		}
		
		nLoginStage = LOGIN_BY_SECRET_ID;
        new QBMyUserService(this).login(QBConst.FORWARD_LOGIN_STRING + secretID, password);
	}
	
	public void successGetSecretID(String secretID) {
		
		if (secretID == null) {
			actionLoginBySecretCode();
			return;
		}
		
		String password = edtPassword.getText().toString();
		
		if (password.equals("") || password == null) {
			DialogUtility.showGeneralAlert(LoginActivity.this, "Alert", "Please input your password.");
			return;
		}
		
        new QBMyUserService(this).login(QBConst.FORWARD_LOGIN_STRING + secretID, password);
	}
	
	public void failedLogin(String error) {
		if (nLoginStage == LOGIN_BY_PHONE_NUMBER) {
			actionLoginBySecretCode();
		}
		if (nLoginStage == LOGIN_BY_SECRET_ID) {
			DialogUtility.showGeneralAlert(this, "Error", error);
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    // Inflate the menu items for use in the action bar

	    return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle presses on the action bar items
	    switch (item.getItemId()) {
	        case android.R.id.home:
	        	Log.e("home", "back button clicked");
	        	finish();
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
}