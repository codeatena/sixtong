package com.han.chatapp.user;

import com.han.chatapp.GlobalData;
import com.han.chatapp.R;
import com.han.chatapp.region.CountryActivity;
import com.han.quickblox.module.QBUserService;
import com.han.utility.CameraUtility;
import com.han.utility.ImageUtility;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MyProfileActivity extends Activity {

	RelativeLayout rlytMyPhoto;
	ImageView imgMyPhoto;
	RelativeLayout rlytMyFullName;
	TextView txtMyFullName;
//	RelativeLayout rlytSecretID;
//	TextView txtSecretID;
	TextView txtPhoneNumber;
	RelativeLayout rlytGender;
	TextView txtGender;
	RelativeLayout rlytWhere;
	TextView txtWhere;
	RelativeLayout rlytWords;
	TextView txtWords;
	
	final int REQUEST_CODE_EDIT_NAME = 1001;
	final int REQUEST_CODE_EDIT_WORDS = 1002;
	final int REQUEST_CODE_EDIT_WHERE = 1003;

	private Uri fileUri;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_my_profile);

	    ActionBar actionBar = getActionBar();
	    actionBar.setHomeButtonEnabled(true);
	    
	    initWidget();
	    initValue();
	    initEvent();
	}
	
	private void initWidget() {
		rlytMyPhoto = (RelativeLayout) findViewById(R.id.my_photo_relativeLayout);
		imgMyPhoto = (ImageView) findViewById(R.id.my_photo_imageView);
		rlytMyFullName = (RelativeLayout) findViewById(R.id.my_fullname_relativeLayout);
		txtMyFullName = (TextView) findViewById(R.id.my_fullname_textView);
		txtPhoneNumber = (TextView) findViewById(R.id.my_profile_phone_textView);
//		rlytSecretID = (RelativeLayout) findViewById(R.id.my_secret_id_relativeLayout);
//		txtSecretID = (TextView) findViewById(R.id.my_secret_id_textView);
		rlytGender = (RelativeLayout) findViewById(R.id.my_gender_relativeLayout);
		txtGender = (TextView) findViewById(R.id.my_gender_textView);
		rlytWhere = (RelativeLayout) findViewById(R.id.my_posts_relativeLayout);
		txtWhere = (TextView) findViewById(R.id.my_where_textView);
		rlytWords = (RelativeLayout) findViewById(R.id.my_words_relatvieLayout);
		txtWords = (TextView) findViewById(R.id.my_words_textView);
	}
	
	public void initValue() {
		if (GlobalData.myUser.strPhoto != null && !GlobalData.myUser.strPhoto.equals("")) {
			imgMyPhoto.setImageBitmap(ImageUtility.StringToBitmap(GlobalData.myUser.strPhoto));	
		}
		
		txtMyFullName.setText(GlobalData.myUser.qbUser.getFullName());
		txtPhoneNumber.setText(GlobalData.myUser.qbUser.getPhone());
//		String userName = PreferenceUtility.sharedPreferences.getString(PreferenceUtility.PREFERENCE_KEY_CUR_USER_NAME, "");
//		String secretID = userName.substring(QBConst.FORWARD_LOGIN_STRING.length()); 	
//		txtSecretID.setText(secretID);
		txtGender.setText(GlobalData.myUser.gender);
		txtWhere.setText(GlobalData.myUser.where);
		txtWords.setText(GlobalData.myUser.words);
		
//		String[] locales = Locale.getISOCountries();
//		
//		for (String countryCode : locales) {
//	 
//		    Locale obj = new Locale("", countryCode);
//	 
//		    Log.e("country information: ", "Country Code = " + obj.getCountry() 
//			+ ", Country Name = " + obj.getDisplayCountry(Locale.ENGLISH));
//	 
//		 }
	}
	
	private void initEvent() {
		rlytMyPhoto.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
					
					private static final int TAKE_PICTURE = 0;
					private static final int SELECT_PICTURE = 1;
					private static final int CANCEL = 2;
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						switch (which) {
							case TAKE_PICTURE:
								Log.e("TAKE_PICTURE", "click");
								takePhoto();
								break;
							case SELECT_PICTURE:
								Log.e("SELECT_PICTURE", "click");
								selectPhoto();
								break;
							case CANCEL:
				    			dialog.dismiss();
				    			break;
						}
					}
				};
				
				new AlertDialog.Builder(MyProfileActivity.this)
			    	.setItems(R.array.dialog_get_picture, listener)
			    	.setCancelable(true)
			    	.show();
			}
        });
		
		rlytMyFullName.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				EditNameActivity.defaultName = GlobalData.myUser.qbUser.getFullName();
				startActivityForResult(new Intent(MyProfileActivity.this, EditNameActivity.class), REQUEST_CODE_EDIT_NAME);
			}
		});
		
		rlytGender.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
					
					private static final int SELECT_MALE = 0;
					private static final int SELECT_FEMALE = 1;
						
					@Override
					public void onClick(DialogInterface dialog, int which) {
						switch (which) {
							case SELECT_MALE:
								new QBUserService(MyProfileActivity.this).updateMyGender("Male");
								break;
							case SELECT_FEMALE:
								new QBUserService(MyProfileActivity.this).updateMyGender("Female");
								break;
						}
					}
				};
				new AlertDialog.Builder(MyProfileActivity.this)
				.setTitle(R.string.gender)
				.setItems(R.array.dialog_sel_gender, listener)
		    	.setCancelable(true)
		    	.setPositiveButton(R.string.cancel, new DialogInterface.OnClickListener() {
			    	public void onClick(DialogInterface dialog, int which) { 
			    		
			    	}
			    })
		    	.show();
			}
		});
		
		rlytWhere.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivityForResult(new Intent(MyProfileActivity.this, CountryActivity.class), REQUEST_CODE_EDIT_WHERE);
			}
		});
		
		rlytWords.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				EditWordsActivity.defaultWords = GlobalData.myUser.words;
				startActivityForResult(new Intent(MyProfileActivity.this, EditWordsActivity.class), REQUEST_CODE_EDIT_WORDS);
			}
		});
	}
	
	public void takePhoto() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
 
        fileUri = CameraUtility.getOutputMediaFileUri(CameraUtility.MEDIA_TYPE_IMAGE);
        
        try {
            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        } catch (Exception e) {
        	e.printStackTrace();
        }
 
        startActivityForResult(intent, CameraUtility.CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }
	
	protected void onActivityResult(int requestCode, int resultCode, Intent imageReturedIntent) {
		if (requestCode == CameraUtility.CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) { 	
            	previewImage();
            }
        } else if (requestCode == CameraUtility.SELECT_PICTURE_REQUEST_CODE) {
			if (resultCode == RESULT_OK) {
				fileUri = imageReturedIntent.getData();
				fileUri = ImageUtility.getRealUriFromContentUri(fileUri);
				previewImage();
			}
		} else {
			initValue();
		}
	}
	
	private void previewImage() {
		Log.e("Image File path", fileUri.getPath());
				
		Bitmap bmp = ImageUtility.getBitmapFromFileUri(fileUri, 50, 50);
		imgMyPhoto.setImageBitmap(bmp);
		
		String strBmp = ImageUtility.BitmapToString(bmp);
		new QBUserService(this).updateMyPhoto(strBmp);
    }
	
	public void selectPhoto() {
		Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
		photoPickerIntent.setType("image/*");
		startActivityForResult(photoPickerIntent, CameraUtility.SELECT_PICTURE_REQUEST_CODE);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle presses on the action bar items
	    switch (item.getItemId()) {
	        case android.R.id.home:
	        	finish();
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
}