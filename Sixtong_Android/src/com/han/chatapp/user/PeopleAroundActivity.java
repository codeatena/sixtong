package com.han.chatapp.user;

import java.util.ArrayList;

import com.han.chatapp.R;
import com.han.chatapp.model.User;
import com.han.quickblox.module.QBLocationService;
import com.han.utility.ImageUtility;
import com.han.widget.listView.CellItem;
import com.han.widget.listView.MyListAdapter;
import com.han.widget.listView.MyListItem;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class PeopleAroundActivity extends Activity {
	
	ListView lstPeoples;
	MyListAdapter adpPeoples;
	ArrayList<User> arrUser = new ArrayList<User>();
	
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_simple_list);

	    ActionBar actionBar = getActionBar();
	    actionBar.setHomeButtonEnabled(true);
	    
	    initWidget();
	    initValue();
	    initEvent();
	}
	
	private void initWidget() {
		lstPeoples = (ListView) findViewById(R.id.listView);
	}
	
	private void initValue() {
		ArrayList<MyListItem> arrItem = new ArrayList<MyListItem>();
		adpPeoples = new MyListAdapter(this, arrItem);
		lstPeoples.setAdapter(adpPeoples);
		lstPeoples.setDividerHeight(0);
	}
	
	private void initEvent() {
		new QBLocationService(this).readPeoplesAround();
		lstPeoples.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				ProfileActivity.ownUser = arrUser.get(position);
				startActivity(new Intent(PeopleAroundActivity.this, ProfileActivity.class));
			}
		});
	}
	
	@SuppressLint("DefaultLocale")
	public void successGetAroundPeoples(ArrayList<User> _arrUser) {
		arrUser = _arrUser;
		Log.e("people around count", Integer.toString(arrUser.size()));
		for (User user: arrUser) {
			String userName = user.qbUser.getFullName() + " (" + user.gender + ")";
			String dis = String.format("%.2f", user.distance) + " Mi";
			CellItem cellItem = new CellItem(userName, dis, user.words, true);
			cellItem.setImageBitmap(ImageUtility.StringToBitmap(user.strPhoto));
			adpPeoples.add(cellItem);
		}
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle presses on the action bar items
	    switch (item.getItemId()) {
	        case android.R.id.home:
	        	finish();
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
}