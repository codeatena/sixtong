package com.han.chatapp.user;

import com.han.chatapp.R;
import com.han.quickblox.module.QBUserService;

import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;

public class EditNameActivity extends Activity {

	EditText edtName;
	public static String defaultName = ""; 
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_name);
	    
	    initWidget();
	    initValue();
	    initEvent();
	}
	
	private void initWidget() {
	    ActionBar actionBar = getActionBar();
	    actionBar.setHomeButtonEnabled(true);
	    actionBar.setTitle(R.string.full_name);
	}
	
	private void initValue() {
		edtName = (EditText) findViewById(R.id.name_editText);
		edtName.setText(defaultName);
	}
	
	private void initEvent() {
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    // Inflate the menu items for use in the action bar
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.edit_name_activity_action, menu);

	    return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle presses on the action bar items
	    switch (item.getItemId()) {
	        case android.R.id.home:
	        	finish();
	            return true;
	        case R.id.action_save:
	        	actionSave();
	        	return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
	
	private void actionSave() {
		new QBUserService(this).updateMyFullName(edtName.getText().toString());
	}
}
