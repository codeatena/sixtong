package com.han.chatapp.user;

import com.han.chatapp.R;
import com.han.chatapp.service.UserAsyncTask;
import com.han.quickblox.module.QBMyUserService;
import com.han.utility.PreferenceUtility;
import com.han.utility.StringUtility;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class VerifyActivity extends Activity {
	
	Button btnLast;
	Button btnSignup;
	
	TextView txtPhoneNumber;
	EditText edtSecretID;
	TextView txtSecretID;
	
	Button btnResend;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_verify);

//	    ActionBar actionBar = SignupActivity.this.getActionBar();
//	    actionBar.setHomeButtonEnabled(true);
	    
	    initWidget();
	    initValue();
	    initEvent();
	}
	
	private void initWidget() {
		btnLast = (Button) findViewById(R.id.verify_last_button);
		btnSignup = (Button) findViewById(R.id.verify_signup_button);
		
		txtPhoneNumber = (TextView) findViewById(R.id.verify_phone_number_textView);
		edtSecretID = (EditText) findViewById(R.id.secret_id_editText);
		txtSecretID = (TextView) findViewById(R.id.secret_id_textView);
		btnResend = (Button) findViewById(R.id.resend_button);
	}
	
	private void initValue() {
		String countryCode = PreferenceUtility.sharedPreferences.getString(PreferenceUtility.PREFERENCE_KEY_USER_COUNTRY_CODE, "");
		String phoneNumber = PreferenceUtility.sharedPreferences.getString(PreferenceUtility.PREFERENCE_KEY_USER_PHONE_NUMBER, "");
		String secretID = PreferenceUtility.sharedPreferences.getString(PreferenceUtility.PREFERENCE_KEY_USER_SECRET_ID, "");
		
		txtSecretID.setText(secretID);
		txtPhoneNumber.setText("(" + countryCode + ") " + StringUtility.getPhoneFormatString(phoneNumber));
	}
	
	private void initEvent() {
		btnResend.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				String countryCode = PreferenceUtility.sharedPreferences.getString(PreferenceUtility.PREFERENCE_KEY_USER_COUNTRY_CODE, "");
				String phoneNumber = PreferenceUtility.sharedPreferences.getString(PreferenceUtility.PREFERENCE_KEY_USER_PHONE_NUMBER, "");
				
				new UserAsyncTask(VerifyActivity.this).execute(UserAsyncTask.ACTION_RESEND, countryCode, phoneNumber);
			}
        });
		btnLast.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
        });
		btnSignup.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				String countryCode = PreferenceUtility.sharedPreferences.getString(PreferenceUtility.PREFERENCE_KEY_USER_COUNTRY_CODE, "");
				String phoneNumber = PreferenceUtility.sharedPreferences.getString(PreferenceUtility.PREFERENCE_KEY_USER_PHONE_NUMBER, "");
				String secretID = edtSecretID.getText().toString();
				
				new UserAsyncTask(VerifyActivity.this).execute(UserAsyncTask.ACTION_VERIFY, countryCode, phoneNumber, secretID);
			}
        });
	}
	
	public void receiveNewSecretID(String secretID) {
		PreferenceUtility.prefEditor.putString(PreferenceUtility.PREFERENCE_KEY_USER_SECRET_ID, secretID);
		PreferenceUtility.prefEditor.commit();
		txtSecretID.setText(secretID);
	}
	
	public void sucessVerify(String secretID) {
		PreferenceUtility.prefEditor.putString(PreferenceUtility.PREFERENCE_KEY_USER_SECRET_ID, secretID);
		PreferenceUtility.prefEditor.commit();
		
		new QBMyUserService(this).createQBUser();
	}
	
	public void successCreateQBUser() {
		PreferenceUtility.prefEditor.putString(PreferenceUtility.PREFERENCE_KEY_USER_STATUS, PreferenceUtility.USER_STATUS_VERIFIED);
		PreferenceUtility.prefEditor.commit();
		
		startActivity(new Intent(this, WelcomeActivity.class));
		finish();
	}
}
