package com.han.chatapp.user;

import com.han.chatapp.R;
import com.han.chatapp.service.UserAsyncTask;
import com.han.utility.CameraUtility;
import com.han.utility.DialogUtility;
import com.han.utility.ImageUtility;
import com.han.utility.PreferenceUtility;
import com.han.utility.StringUtility;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

public class SignupActivity extends Activity {

	Button btnLast;
	Button btnNext;
	
	ImageView imgPhoto;
	ImageView imgCamera;
	
	EditText edtFullName;
	Spinner spnCountryCode;
	EditText edtPhone;
	EditText edtPassword;
	EditText edtConfirmPassword;
	
	private Uri fileUri;
	boolean isHavePhoto;
	
	final int NOTHING_MODE = 0;
	final int TAKE_PICTURE_MODE = 1;
	final int SELECT_PICTURE_MODE = 2;
	
	int nGetPicture;
	
	ProgressDialog progressDialog;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_signup);

//	    ActionBar actionBar = SignupActivity.this.getActionBar();
//	    actionBar.setHomeButtonEnabled(true);
	    
	    initWidget();
	    initValue();
	    initEvent();
	}
	
	private void initWidget() {
		
		btnLast = (Button) findViewById(R.id.last_button);
		btnNext = (Button) findViewById(R.id.next_button);
		
		imgPhoto = (ImageView) findViewById(R.id.my_post_photo_imageView);
		imgCamera = (ImageView) findViewById(R.id.camera_imageView);
		
		edtFullName = (EditText) findViewById(R.id.username_editText);
		spnCountryCode = (Spinner) findViewById(R.id.country_code_spinner);
		edtPhone = (EditText) findViewById(R.id.secret_id_editText);
		edtPassword = (EditText) findViewById(R.id.signup_password_editText);
		edtConfirmPassword = (EditText) findViewById(R.id.signup_confirm_editText);
	}
	
	private void initValue() {
		isHavePhoto = false;
		nGetPicture = NOTHING_MODE;
	}
	
	private void initEvent() {
		
		btnLast.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();				
			}
        });
		
		btnNext.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				
//				DialogUtility.show(getApplicationContext(), spnCountryCode.getSelectedItem().toString());
				signupInServer();
//				signupInServer();
//				actionSignup();
//				uploadPhoto();
			}
        });
		
		imgPhoto.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
					
					private static final int TAKE_PICTURE = 0;
					private static final int SELECT_PICTURE = 1;
					private static final int CANCEL = 2;
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						switch (which) {
							case TAKE_PICTURE:
								Log.e("TAKE_PICTURE", "click");
								takePhoto();
								break;
							case SELECT_PICTURE:
								Log.e("SELECT_PICTURE", "click");
								selectPhoto();
								break;
							case CANCEL:
				    			dialog.dismiss();
				    			break;
						}
					}
				};
				
				new AlertDialog.Builder(SignupActivity.this)
			    	.setItems(R.array.dialog_get_picture, listener)
			    	.setCancelable(true)
			    	.show();
			}
        });
	}
	
	protected void onActivityResult(int requestCode, int resultCode, Intent imageReturedIntent) {
		if (requestCode == CameraUtility.CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
            	nGetPicture = TAKE_PICTURE_MODE;
            	previewImage();
            }
        }
		if (requestCode == CameraUtility.SELECT_PICTURE_REQUEST_CODE) {
			if (resultCode == RESULT_OK) {
            	nGetPicture = SELECT_PICTURE_MODE;
				fileUri = imageReturedIntent.getData();
				fileUri = ImageUtility.getRealUriFromContentUri(fileUri);
				previewImage();
			}
		}
	}
	
	private void previewImage() {
		Bitmap bmp = ImageUtility.getBitmapFromFileUri(fileUri, 50, 50);
		imgPhoto.setImageBitmap(bmp);
        imgCamera.setVisibility(View.GONE);
        Log.e("Image File path", fileUri.getPath());
        isHavePhoto = true;
    }
	
	public void takePhoto() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
 
        fileUri = CameraUtility.getOutputMediaFileUri(CameraUtility.MEDIA_TYPE_IMAGE);
        
        try {
            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        } catch (Exception e) {
        	e.printStackTrace();
        }
 
        startActivityForResult(intent, CameraUtility.CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }
	
	public void selectPhoto() {
		Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
		photoPickerIntent.setType("image/*");
		startActivityForResult(photoPickerIntent, CameraUtility.SELECT_PICTURE_REQUEST_CODE);
	}
	
	@SuppressLint("InflateParams")
	private void signupInServer() {
		String fullName = edtFullName.getText().toString();
		String countryCode = StringUtility.getBetweenString(spnCountryCode.getSelectedItem().toString(), "(", ")");
//		DialogUtility.showGeneralAlert(this, "debug", countryCode);
		String phoneNumber = edtPhone.getText().toString();
		String password = edtPassword.getText().toString();
		String confirmPassword = edtConfirmPassword.getText().toString();
		
		if (fullName.equals("") || fullName == null) {
			DialogUtility.showGeneralAlert(SignupActivity.this, "Alert", "Please input your name.");
			return;
		}
		if (countryCode.equals("") || countryCode == null) {
			DialogUtility.showGeneralAlert(SignupActivity.this, "Alert", "Please select your country.");
			return;
		}
		if (phoneNumber.equals("") || phoneNumber == null) {
			DialogUtility.showGeneralAlert(SignupActivity.this, "Alert", "Please input your phone number.");
			return;
		}
		if (phoneNumber.length() < 10) {
			DialogUtility.showGeneralAlert(SignupActivity.this, "Alert", "Your phone number is invalid format");
			return;
		}
		if (password.equals("") || password == null) {
			DialogUtility.showGeneralAlert(SignupActivity.this, "Alert", "Please input your password.");
			return;
		}
		if (!password.equals(confirmPassword)) {
			DialogUtility.showGeneralAlert(SignupActivity.this, "Alert", "Your passwords are not matched.");
			return;
		}
		if (password.length() < 8) {
			DialogUtility.showGeneralAlert(SignupActivity.this, "Alert", "You must enter 8 charaters at least.");
			return;
		}
		AlertDialog.Builder builder = new AlertDialog.Builder(SignupActivity.this);
	    LayoutInflater inflater = SignupActivity.this.getLayoutInflater();
	    
	    View view = inflater.inflate(R.layout.dialog_confirm_phone, null);
	    TextView txtPhoneNumber = (TextView) view.findViewById(R.id.phone_number_textView);
	    txtPhoneNumber.setText(StringUtility.getPhoneFormatString(phoneNumber));
	    
	    builder.setView(view)
	           .setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
	               @Override
	               public void onClick(DialogInterface dialog, int id) {
	            	   requestSignupInServer();
	               }
	           })
	           .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
	               @Override
	               public void onClick(DialogInterface dialog, int id) {
	                   
	               }
	           });
	    
	    builder.create();
	    builder.show();
	}
	
	private void requestSignupInServer() {
		
		String fullName = edtFullName.getText().toString();
		String countryCode = StringUtility.getBetweenString(spnCountryCode.getSelectedItem().toString(), "(", ")");
//		DialogUtility.showGeneralAlert(this, "debug", countryCode);
		String phoneNumber = edtPhone.getText().toString();
		String password = edtPassword.getText().toString();
		
		new UserAsyncTask(this).execute(UserAsyncTask.ACTION_SIGNUP, fullName, countryCode, phoneNumber, password);
	}
	
	/*
	private void uploadPhoto(String strID) {

		try {
			
			File file = null;
			
			if (nGetPicture == TAKE_PICTURE_MODE) {
				Log.e("photo path", fileUri.getPath());
				file = new File(fileUri.getPath());
			}
			if (nGetPicture == SELECT_PICTURE_MODE) {
				String[] filePath = { MediaStore.Images.Media.DATA };
	            Cursor c = getContentResolver().query(fileUri, filePath, null, null, null);
	            c.moveToFirst();
	            int columnIndex = c.getColumnIndex(filePath[0]);
	            
	            // get Photo Path
	            String photopathStr = c.getString(columnIndex);
	            
				Log.e("photo path", photopathStr);
				file = new File(photopathStr);
			}
			
			QBCustomObject qbCustomObject = new QBCustomObject("Contacts", strID);
			QBCustomObjectsFiles.uploadFile(file, qbCustomObject, "my_photo", new QBEntityCallbackImpl<QBCustomObjectFileField>() {
				
	            @Override
	            public void onSuccess(QBCustomObjectFileField qbCustomObjectField, Bundle bundle) {
	            	Log.e("QBCustomObjectFileField", qbCustomObjectField.getFileName());
	            	successSignup();
	            }
	
	            @Override
	            public void onError(List<String> errors) {
	            	DialogUtility.show(SignupActivity.this, errors.get(0));
	            }
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	*/
	
	public void procVerify(String secretID) {
		
		String fullName = edtFullName.getText().toString();
		String countryCode = StringUtility.getBetweenString(spnCountryCode.getSelectedItem().toString(), "(", ")");
//		DialogUtility.showGeneralAlert(this, "debug", countryCode);
		String phoneNumber = edtPhone.getText().toString();
		String password = edtPassword.getText().toString();
		
		String strBmp = "";
		if (isHavePhoto) {
			strBmp = ImageUtility.BitmapToString(ImageUtility.getBitmapFromImageView(imgPhoto));
		}
		
		PreferenceUtility.prefEditor.putString(PreferenceUtility.PREFERENCE_KEY_USER_STATUS, PreferenceUtility.USER_STATUS_SIGNUP);
		PreferenceUtility.prefEditor.putString(PreferenceUtility.PREFERENCE_KEY_USER_FULL_NAME, fullName);
		PreferenceUtility.prefEditor.putString(PreferenceUtility.PREFERENCE_KEY_USER_COUNTRY_CODE, countryCode);
		PreferenceUtility.prefEditor.putString(PreferenceUtility.PREFERENCE_KEY_USER_PHONE_NUMBER, phoneNumber);
		PreferenceUtility.prefEditor.putString(PreferenceUtility.PREFERENCE_KEY_USER_PASSWORD, password);
		PreferenceUtility.prefEditor.putString(PreferenceUtility.PREFERENCE_KEY_USER_SECRET_ID, secretID);
		PreferenceUtility.prefEditor.putString(PreferenceUtility.PREFERENCE_KEY_USER_PHOTO, strBmp);
		PreferenceUtility.prefEditor.commit();
		
		startActivity(new Intent(this, VerifyActivity.class));
		finish();
	}
	
//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//	    // Inflate the menu items for use in the action bar
//
//	    return super.onCreateOptionsMenu(menu);
//	}
//	
//	@Override
//	public boolean onOptionsItemSelected(MenuItem item) {
//	    // Handle presses on the action bar items
//	    switch (item.getItemId()) {
//	        case android.R.id.home:
//	        	Log.e("home", "back button clicked");
//	        	finish();
//	            return true;
//	        default:
//	            return super.onOptionsItemSelected(item);
//	    }
//	}
}