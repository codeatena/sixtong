package com.han.chatapp.user;

import java.util.ArrayList;

import com.han.chatapp.R;
import com.han.chatapp.chat.ChatActivity;
import com.han.chatapp.model.Message;
import com.han.chatapp.model.Post;
import com.han.chatapp.model.User;
import com.han.chatapp.post.PostsActivity;
import com.han.chatapp.service.PostAsyncTask;
import com.han.quickblox.module.QBDialogService;
import com.han.quickblox.module.QBUserService;
import com.han.utility.ImageUtility;
import com.han.utility.SMSUtility;
import com.koushikdutta.urlimageviewhelper.UrlImageViewCallback;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.ContentUris;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract.Contacts;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ProfileActivity extends Activity {

	public static User ownUser;
	
	ImageView imgPhoto;
	TextView txtFullName;
	TextView txtPhone;
	TextView txtNotJoined;
	
	LinearLayout llytMyProfile;
	
	TextView txtWhere;
	TextView txtWords;
	
	RelativeLayout rlytPosts;
	LinearLayout llytPosts;
	
	Button btnAction;
	
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_profile);

	    ActionBar actionBar = ProfileActivity.this.getActionBar();
	    actionBar.setHomeButtonEnabled(true);
	    
	    initWidget();
	    initValue();
	    initEvent();
	}
	
	private void initWidget() {
		imgPhoto = (ImageView) findViewById(R.id.profile_photo_imageView);
		
		txtFullName = (TextView) findViewById(R.id.profile_name_textView);
		txtPhone = (TextView) findViewById(R.id.profile_phone_textView);
		txtNotJoined = (TextView) findViewById(R.id.not_join_textView);
		
		llytMyProfile = (LinearLayout) findViewById(R.id.profile_linearLayout);
		
		txtWhere = (TextView) findViewById(R.id.profile_where_textView);
		txtWords = (TextView) findViewById(R.id.profile_words_textView);

		rlytPosts = (RelativeLayout) findViewById(R.id.profile_post_relativeLayout);
		llytPosts = (LinearLayout) findViewById(R.id.profile_posts_linearLayout);
		
		btnAction = (Button) findViewById(R.id.profile_action_button);
	}
	
	private void initValue() {
		if (ownUser.qbUser == null) {
		    Uri contactUri = ContentUris.withAppendedId(Contacts.CONTENT_URI, Long.parseLong(ownUser.contact.id));
		    Uri photoUri = Uri.withAppendedPath(contactUri, Contacts.Photo.CONTENT_DIRECTORY);
			
			txtFullName.setText(ownUser.contact.name);
			btnAction.setText(R.string.invite_string);
			txtNotJoined.setText(ownUser.contact.name + " has not joined in chatapp yet");
			txtPhone.setText(ownUser.contact.numbers.get(0).number);

	        imgPhoto.setImageURI(photoUri);
	        if (imgPhoto.getDrawable() == null) {
	        	imgPhoto.setImageResource(R.drawable.ic_action_person);
	        }
	        
	        llytMyProfile.setVisibility(View.GONE);
		} else {
			txtPhone.setText(ownUser.qbUser.getPhone());
//			txtPhone.setVisibility(View.GONE);
			txtNotJoined.setVisibility(View.GONE);

	        if (ownUser.strPhoto == null || ownUser.strPhoto.equals("")) {
		        imgPhoto.setImageResource(R.drawable.ic_action_person);
	        } else {
	        	imgPhoto.setImageBitmap(ImageUtility.StringToBitmap(ownUser.strPhoto));
	        }
	        
			String username = ownUser.qbUser.getFullName() + " (" + ownUser.gender + ")";
			txtFullName.setText(username);
			
			txtWhere.setText(ownUser.where);
			txtWords.setText(ownUser.words);
			
			if (ownUser.isMyFriend()) {
				btnAction.setText(R.string.chat_now);
			} else if (ownUser.isMyInviter()) {
				btnAction.setText(R.string.accept);
			} else {
				btnAction.setText(R.string.say_hello);
			}
			
			new PostAsyncTask(this).execute(PostAsyncTask.ACTION_GET_POSTS, ownUser.secretID);
		}
	}
	
	private void initEvent() {
		btnAction.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (ownUser.qbUser == null) {
					SMSUtility.sendSMS(ProfileActivity.this, ownUser.contact.numbers.get(0).number, "This app is good app for commnunication");
				} else {
					if (ownUser.isMyFriend()) {
						ChatActivity.opponentID = ownUser.qbUser.getId();
						ChatActivity.dialog = QBDialogService.getQBDialogWithSpecificUser(ownUser.qbUser.getId());
						ProfileActivity.this.startActivity(new Intent(ProfileActivity.this, ChatActivity.class));
					} else if (ownUser.isMyInviter()) {
						// accept
						acceptUser();
					} else {
						// send invite
						inviteUser();
					}
				}
			}
        });
		rlytPosts.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				PostsActivity.ownUser = ownUser;
				startActivity(new Intent(ProfileActivity.this, PostsActivity.class));
			}
        });
	}
	
	public void successGetPosts(ArrayList<Post> arrPosts) {
		for (final Post post: arrPosts) {
			ImageView imgPost = new ImageView(this);
			
			LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(50, 50);
			layoutParams.leftMargin = 1;
			layoutParams.rightMargin = 1;
			imgPost.setLayoutParams(layoutParams);
        	imgPost.setScaleType(ScaleType.FIT_XY);
        	
        	llytPosts.addView(imgPost);

            UrlImageViewHelper.setUrlDrawable(imgPost, post.url, new UrlImageViewCallback() {
                @Override
                public void onLoaded(ImageView imageView, Bitmap loadedBitmap, String url, boolean loadedFromCache) {
                	imageView.setImageBitmap(ImageUtility.getRotatedBitmap(loadedBitmap, post.orientation));
                }
            });
        }
	}
	
	private void acceptUser() {
		Message.sendAcceptMessage(ownUser.qbUser.getId());
		new QBUserService(this).acceptInvite(ownUser);
	}
	
	private void inviteUser() {
		Message.sendInviteMessage(ownUser.qbUser.getId());
		new QBUserService(this).sendInvite(ownUser);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    // Inflate the menu items for use in the action bar
		if (ownUser.isMyFriend()) {
		    MenuInflater inflater = getMenuInflater();
		    inflater.inflate(R.menu.profile_activity_action, menu);	
		}
	    return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle presses on the action bar items
	    switch (item.getItemId()) {
	    	case R.id.action_block:
	    		actionBlock();
	    		return true;
	        case android.R.id.home:
	        	Log.e("home", "back button clicked");
	        	finish();
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
	
	private void actionBlock() {
		new QBUserService(this).block(ownUser);
	}
}