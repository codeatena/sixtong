package com.han.chatapp;

import java.util.Locale;

import com.han.chatapp.database.DatabaseHelper;
import com.han.utility.PreferenceUtility;
import android.app.Application;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.util.Log;

public class App extends Application {

    private static App instance;
    
    public static App getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        initApplication();
    }

    private void initApplication() {
        instance = this;
        
        PreferenceUtility.sharedPreferences = getSharedPreferences(PreferenceUtility.PREF_NAME, MODE_PRIVATE);
        PreferenceUtility.prefEditor = PreferenceUtility.sharedPreferences.edit();
        
        setLanguage();
        
//        GlobalData.dbHelper = new DatabaseHelper(this);
    }
    
    public static DatabaseHelper getDBHelper() {
    	return new DatabaseHelper(instance);
    }
    
    public int getAppVersion() {
        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }
    
    public void setLanguage() {
    	
    	int nCurLanguage = PreferenceUtility.sharedPreferences.getInt(PreferenceUtility.PREFERENCE_KEY_LANGUAGE, PreferenceUtility.LANGUAGE_AUTO);
    	Locale locale = null;
    	Log.e("current locale", Locale.getDefault().getLanguage());
    	
    	if (nCurLanguage == 1) {
    		locale = new Locale(Locale.getDefault().getLanguage());
    	}
    	if (nCurLanguage == 2) { // auto
    		locale = new Locale("zh");	
    	}
    	if (nCurLanguage == 3) {
    		locale = new Locale("en");
    	}
    	
	    Locale.setDefault(locale);
	    Configuration config = new Configuration();
	    config.locale = locale;
	    
	    getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
    }	
}