package com.han.chatapp;

import java.util.ArrayList;

import com.han.chatapp.model.MyUser;
import com.han.chatapp.model.User;
import com.han.chatapp.region.Country;
import com.han.quickblox.module.QBGroupChatService;
import com.han.quickblox.module.QBPrivateChatService;

public class GlobalData {

	public static double curLat;
	public static double curLng;
	
	public static MyUser myUser = new MyUser();
//	public static DatabaseHelper dbHelper;
	public static ArrayList<User> arrContactUsers;
	public static ArrayList<Country> arrCountry;
	
	public static QBPrivateChatService qbPrivateChatService = new QBPrivateChatService();
	public static QBGroupChatService qbGroupChatService = new QBGroupChatService();
	
	public static ArrayList<User> arrAllUsers;
	
	public static void init() {
		myUser = new MyUser();
		
		arrContactUsers = new ArrayList<User>();
		arrCountry = new ArrayList<Country>();
		
		arrAllUsers = new ArrayList<User>();
	}
}