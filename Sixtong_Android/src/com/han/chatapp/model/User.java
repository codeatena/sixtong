package com.han.chatapp.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;

import com.han.chatapp.App;
import com.han.chatapp.GlobalData;
import com.han.contact.model.Contact;
import com.han.utility.ValueUtility;
import com.quickblox.core.QBEntityCallbackImpl;
import com.quickblox.core.request.QBCustomObjectRequestBuilder;
import com.quickblox.core.request.QBPagedRequestBuilder;
import com.quickblox.customobjects.QBCustomObjects;
import com.quickblox.customobjects.model.QBCustomObject;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;

public class User {
	
	public int id;
	public QBUser qbUser;
	public Contact contact;
	public String qbCustomObjectID;
	public String secretID;
	public String gender;
	public String where;
	public String words;
	public String strPhoto;
	public double distance;
	
	public ArrayList<String> arrFriendIDs = new ArrayList<String>();
	public ArrayList<String> arrInviterIDs = new ArrayList<String>();
	
	static String TABLE_NAME = "table_user";
	
	static String FIELD_ID = "id";
	static String FIELD_QB_USER_ID = "qb_user_id";
	static String FIELD_LOGIN_NAME = "login_name";
	static String FIELD_FULL_NAME = "full_name";
	static String FIELD_PHONE = "phone";
	static String FIELD_CUSTOM_OBJECT_ID = "qb_custom_object_id";
	static String FIELD_SECRET_ID = "secret_id";
	static String FIELD_GENDER = "gender";
	static String FIELD_WHERE = "my_where";
	static String FIELD_WORDS = "words";
	static String FIELD_PHOTO = "photo";
	
	public User() {
		qbUser = null;
		contact = null;
		strPhoto = null;
		secretID = "";
		gender = "Male";
		where = "";
		words = "";
		distance = 0.0f;
		qbCustomObjectID = "";
	}
	
//    public void findQBUser() {
//        for (QBUser tmpUser: DataHolder.getDataHolder().getQbUsersList()) {
//        	if (contact.numbers.get(0).number.equals(tmpUser.getPhone())) {
//        		qbUser = tmpUser;
//        		return;
//        	}
//        }
//        qbUser = null;
//    }
    
    public void getAllInf() {
    	if (qbUser == null) return;
    	User user = getUserFromQBUserID(qbUser.getId());
    	if (user == null) {
    		getMoreInfFromServer();
    	} else {
    		this.strPhoto = user.strPhoto;
    	}
    }
    
    public void getInfFromServer(int qbUserID) {
    	QBPagedRequestBuilder pagedRequestBuilder = new QBPagedRequestBuilder();
    	pagedRequestBuilder = null;
    	
    	ArrayList<Integer> arrIDs = new ArrayList<Integer>();
    	arrIDs.add(qbUserID);
    	
    	QBUsers.getUsersByIDs(arrIDs, pagedRequestBuilder, new QBEntityCallbackImpl<ArrayList<QBUser>>() {
    		@Override
            public void onSuccess(ArrayList<QBUser> qbUsers, Bundle bundle) {
    			if (qbUsers.size() > 0) {
    				qbUser = qbUsers.get(0);
    				getMoreInfFromServer();
    			}
            }
    		
            @Override
            public void onError(List<String> errors) {
            	
            }
    	});
    }
    
    public void getMoreInfFromServer() {
    	QBCustomObjectRequestBuilder requestBuilder = new QBCustomObjectRequestBuilder();
		requestBuilder.eq("user_id", qbUser.getId());
		QBCustomObjects.getObjects("Contacts", requestBuilder, new QBEntityCallbackImpl<ArrayList<QBCustomObject>>() {
			@Override
		    public void onSuccess(ArrayList<QBCustomObject> arg0, Bundle bundle) {
				Log.e("my contact count", Integer.toString(arg0.size()));
				QBCustomObject tmpObject = arg0.get(0);
            	HashMap<String, Object> fields = tmpObject.getFields();
            	qbCustomObjectID = tmpObject.getCustomObjectId();
            	for (String key: fields.keySet()) {
            		if (key.equals("secret_id")) {
            			String value = (String) fields.get(key);
            			if (value != null) {
            				secretID = value;
            			}
            		}
            		if (key.equals("gender")) {
            			String value = (String) fields.get(key);
            			if (value != null) {
            				gender = value;
            			}
            		}
            		if (key.equals("where")) {
            			String value = (String) fields.get(key);
            			if (value != null) {
            				where = value;
            			}
            		}
            		if (key.equals("words")) {
            			String value = (String) fields.get(key);
            			if (value != null) {
            				words = value;
            			}
            		}
            		if (key.equals("photo")) {
            			String value = (String) fields.get(key);
            			if (value != null) {
            				strPhoto = (String) fields.get(key);	
            			}
            		}
            	}
    			successGetInfFromServer();
			}
			
			@Override
			public void onError(List<String> erros) {
				// TODO Auto-generated method stub
//				DialogUtility.show(SplashActivity.this, erros.get(0));
			}
		});
    }
    
    private void successGetInfFromServer() {
    	User.addUser(this);
    }
    	
	public static void createTable(SQLiteDatabase db) {
		String sqlCreateTable = "CREATE TABLE " + TABLE_NAME + "(" +
				FIELD_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
				FIELD_QB_USER_ID + " INTEGER, " +
				FIELD_LOGIN_NAME + " TEXT, " +
				FIELD_FULL_NAME + " TEXT, " +
				FIELD_PHONE + " TEXT, " +
				FIELD_CUSTOM_OBJECT_ID + " TEXT, " +
				FIELD_SECRET_ID + " TEXT, " +
				FIELD_GENDER + " TEXT, " +
				FIELD_WHERE + " TEXT, " +
				FIELD_WORDS + " TEXT, " +
				FIELD_PHOTO + " TEXT" + ")";
		db.execSQL(sqlCreateTable);
		
		Log.e("create table", "success");
	}
	
	public static void dropTable(SQLiteDatabase db) {
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
	}
	
	public static void deleteAll() {
		SQLiteDatabase db = App.getDBHelper().getWritableDatabase();
        db.delete(TABLE_NAME, null, null);
        db.close();
	}
	
	public static long addUser(User user) {
		
		if (User.getUserFromQBUserID(user.qbUser.getId()) != null) 
			return -1;
		
		SQLiteDatabase db = App.getDBHelper().getWritableDatabase();
		
	    ContentValues values = new ContentValues();
	    
	    values.put(FIELD_QB_USER_ID, user.qbUser.getId());
	    values.put(FIELD_LOGIN_NAME, user.qbUser.getLogin());
	    values.put(FIELD_FULL_NAME, user.qbUser.getFullName());
	    values.put(FIELD_PHONE, user.qbUser.getPhone());
	    values.put(FIELD_CUSTOM_OBJECT_ID, user.qbCustomObjectID);
	    values.put(FIELD_SECRET_ID, user.secretID);
	    values.put(FIELD_GENDER, user.gender);
	    values.put(FIELD_WHERE, user.where);
	    values.put(FIELD_WORDS, user.words);
	    values.put(FIELD_PHOTO, user.strPhoto);

	    long record_id = db.insert(TABLE_NAME, null, values);
	    db.close();
	    
	    return record_id;
	}
	
	public static void updateUser(long id, User user) {
		SQLiteDatabase db = App.getDBHelper().getWritableDatabase();
		
	    ContentValues values = new ContentValues();
	    
	    values.put(FIELD_QB_USER_ID, user.qbUser.getId());
	    values.put(FIELD_LOGIN_NAME, user.qbUser.getLogin());
	    values.put(FIELD_FULL_NAME, user.qbUser.getFullName());
	    values.put(FIELD_PHONE, user.qbUser.getPhone());
	    values.put(FIELD_CUSTOM_OBJECT_ID, user.qbCustomObjectID);
	    values.put(FIELD_SECRET_ID, user.secretID);
	    values.put(FIELD_GENDER, user.gender);
	    values.put(FIELD_WHERE, user.where);
	    values.put(FIELD_WORDS, user.words);
	    values.put(FIELD_PHOTO, user.strPhoto);
	    
	    db.update(TABLE_NAME, values, "id = ? ", new String[] { Long.toString(id) } );
	}
	
	public static User getUserFromQBUserID(int qbUserID) {
		
		ArrayList<User> arrRecord = new ArrayList<User>();
		String selectQuery = "SELECT  * FROM " + TABLE_NAME + " WHERE" 
				+ " qb_user_id='" + Integer.toString(qbUserID) + "'";
 
        SQLiteDatabase db = App.getDBHelper().getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
 
        if (cursor.moveToFirst()) {
            do {
            	User user = new User();
            	QBUser qbUser = new QBUser();
            	
            	user.id = cursor.getInt(0);
            	
            	qbUser.setId(cursor.getInt(1));
            	qbUser.setLogin(cursor.getString(2));
            	qbUser.setFullName(cursor.getString(3));
            	qbUser.setPhone(cursor.getString(4));
            	user.qbCustomObjectID = (String) ValueUtility.getValueWithDefault("", cursor.getString(5));
            	user.secretID = (String) ValueUtility.getValueWithDefault("", cursor.getString(6));
            	user.gender = (String) ValueUtility.getValueWithDefault("", cursor.getString(7));
            	user.where = (String) ValueUtility.getValueWithDefault("", cursor.getString(8));
            	user.words = (String) ValueUtility.getValueWithDefault("", cursor.getString(9));
            	user.strPhoto = (String) ValueUtility.getValueWithDefault("", cursor.getString(10));
            	user.qbUser = qbUser;
            	
                arrRecord.add(user);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        
        if (arrRecord.size() > 0)
        	return arrRecord.get(0);
        else
        	return null;
	}
	
	public static User getUserFromSecretID(String secretID) {
		ArrayList<User> arrRecord = new ArrayList<User>();
		String selectQuery = "SELECT  * FROM " + TABLE_NAME + " WHERE" 
				+ " secret_id='" + secretID + "'";
 
        SQLiteDatabase db = App.getDBHelper().getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
 
        if (cursor.moveToFirst()) {
            do {
            	User user = new User();
            	QBUser qbUser = new QBUser();
            	
            	user.id = cursor.getInt(0);
            	
            	qbUser.setId(cursor.getInt(1));
            	qbUser.setLogin(cursor.getString(2));
            	qbUser.setFullName(cursor.getString(3));
            	qbUser.setPhone(cursor.getString(4));
            	user.qbCustomObjectID = cursor.getString(5);
            	user.secretID = cursor.getString(6);
            	user.gender = cursor.getString(7);
            	user.where = cursor.getString(8);
            	user.words = cursor.getString(9);
            	user.strPhoto = cursor.getString(10);
            	user.qbUser = qbUser;
            	
                arrRecord.add(user);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        
        if (arrRecord.size() > 0)
        	return arrRecord.get(0);
        else
        	return null;
	}
	
	public boolean isMyFriend() {
		if (qbUser == null) return false;
		for (String qbUserID: GlobalData.myUser.arrFriendIDs) {
			if (qbUser.getId() == Integer.parseInt(qbUserID)) {
				return true;
			}
		}
		return false;
	}
	
	public boolean isMyInviter() {
		if (qbUser == null) return false;
		for (String qbUserID: GlobalData.myUser.arrInviterIDs) {
			if (qbUser.getId() == Integer.parseInt(qbUserID)) {
				return true;
			}
		}
		return false;
	}
	
	public boolean isUpdated(User oldUser) {

		if (!oldUser.qbUser.getId().equals(qbUser.getId()))
			return true;
		if (!oldUser.qbUser.getFullName().equals(qbUser.getFullName())) 
			return true;
		if (!oldUser.qbUser.getPhone().equals(qbUser.getPhone()))
			return true;
		if (!oldUser.qbCustomObjectID.equals(qbCustomObjectID))
			return true;
		if (!oldUser.secretID.equals(secretID))
			return true;
		if (!oldUser.gender.equals(gender))
			return true;
		if (!oldUser.where.equals(where))
			return true;
		if (!oldUser.words.equals(words))
			return true;
		if (!oldUser.strPhoto.equals(strPhoto))
			return true;
		
		return false;
	}
	
	public static boolean isOnline(int qbUserID) {
		for (User user: GlobalData.arrAllUsers) {
			if (user.qbUser.getId().equals(qbUserID)) {
				return user.isOnline();
			}
		}
		return false;
	}
	
	public boolean isOnline() {
		long currentTime = System.currentTimeMillis();
		
		if (qbUser == null) return false;
		
		long userLastRequestAtTime = qbUser.getLastRequestAt().getTime();
		 
		// if user didn't do anything last 5 minutes (5*60*1000 milliseconds)    
		if((currentTime - userLastRequestAtTime) > 60 * 1000) {
		     // user is offline now
			return false;
		}
		
		return true;
	}
	
	public static void updateUsersDatabase(ArrayList<User> arrUsers) {
		
		for (User user: arrUsers) {
			User oldUser = User.getUserFromQBUserID(user.qbUser.getId());
			if (oldUser == null) {
				User.addUser(user);
			} else {
				if (user.isUpdated(oldUser))
					User.updateUser(oldUser.id, user);
			}
		}
	}
	
	public static User getUserInAllUser(int qbUserID) {
		for (User user: GlobalData.arrAllUsers) {
			if (user.qbUser.getId().equals(qbUserID)) {
				return user;
			}
		}
		
		return null;
	}
}