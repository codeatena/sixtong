package com.han.chatapp.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONObject;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;

import com.han.chatapp.App;
import com.han.chatapp.GlobalData;
import com.han.utility.TimeUtility;
import com.quickblox.chat.model.QBDialog;
import com.quickblox.core.QBEntityCallbackImpl;
import com.quickblox.core.helper.StringifyArrayList;
import com.quickblox.messages.QBMessages;
import com.quickblox.messages.model.QBEnvironment;
import com.quickblox.messages.model.QBEvent;
import com.quickblox.messages.model.QBNotificationType;
import com.quickblox.messages.model.QBPushType;

public class Message {
	
	public static final int INVITE_MESSAGE = 0;
	public static final int ACCEPT_MESSAGE = 1;
	public static final int CHAT_MESSAGE = 2;
	
	public static final int STATUS_UNREAD = 0;
	public static final int STATUS_READ = 1;
	
	static String TABLE_NAME = "table_message";
	
	static String FIELD_ID = "id";
	static String FIELD_TEXT = "text";
	static String FIELD_SENDER_ID = "sender_id";
	static String FIELD_RECEIVER_ID = "receiver_id";
	static String FIELD_TYPE = "type";
	static String FIELD_STATUS = "status";
	static String FIELD_TIME = "time";
	
	public int id;
	public String text;
	public int sender_qb_id;
	public int receiver_qb_id;
	public int type;
	public int status;
	public long time;
	
	public static void createTable(SQLiteDatabase db) {
		String sqlCreateTable = "CREATE TABLE " + TABLE_NAME + "(" +
				"id INTEGER PRIMARY KEY AUTOINCREMENT, " +
				"text TEXT, " +
                "sender_id INTEGER, " +
                "receiver_id INTEGER, " +
                "type INTEGER, " +
                "status INTEGER, " +
                "time INTEGER" + ")";
		db.execSQL(sqlCreateTable);
	}
	
	public static void dropTable(SQLiteDatabase db) {
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
	}
	
	public static long addMessage(Message message) {
		SQLiteDatabase db = App.getDBHelper().getWritableDatabase();
		
		message.time = TimeUtility.getCurrentTimeStamp();
	    ContentValues values = new ContentValues();
	    
	    values.put(FIELD_TEXT, message.text);
	    values.put(FIELD_SENDER_ID, message.sender_qb_id);
	    values.put(FIELD_RECEIVER_ID, message.receiver_qb_id);
	    values.put(FIELD_TYPE, message.type);
	    values.put(FIELD_STATUS, message.status);
	    values.put(FIELD_TIME, message.time);

	    long record_id = db.insert(TABLE_NAME, null, values);
	    db.close();
	    
	    return record_id;
	}
	
	public static void deleteAll() {
		SQLiteDatabase db = App.getDBHelper().getWritableDatabase();
        db.delete(TABLE_NAME, null, null);
        db.close();
	}
	
	public static void updateRecord1(long id, ContentValues updateValues) {
		SQLiteDatabase db = App.getDBHelper().getWritableDatabase();
	    db.update(TABLE_NAME, updateValues, "id = ? ", new String[] { Long.toString(id) } );
	    db.close();
	}
	
	public static ArrayList<Message> getInviteUnReadMessage() {
		ArrayList<Message> arrRecord = new ArrayList<Message>();
		String selectQuery = "SELECT  * FROM " + TABLE_NAME + " WHERE" 
				+ " receiver_id='" + Integer.toString(GlobalData.myUser.qbUser.getId()) + "'"
				+ " AND type='" + Integer.toString(GlobalData.myUser.qbUser.getId()) + "'"
				+ " AND status='" + Integer.toString(Message.STATUS_UNREAD) + "'";
 
        SQLiteDatabase db = App.getDBHelper().getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
 
        if (cursor.moveToFirst()) {
            do {
            	Message message = new Message();
            	message.id = cursor.getInt(0);
            	message.text = cursor.getString(1);
            	message.sender_qb_id = cursor.getInt(2);
            	message.receiver_qb_id = cursor.getInt(3);
            	message.type = cursor.getInt(4);
            	message.status = cursor.getInt(5);
            	message.time = cursor.getLong(6);
                arrRecord.add(message);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
		return arrRecord;
	}
	
	public static ArrayList<Message> getInviteMessages() {
		ArrayList<Message> arrRecord = new ArrayList<Message>();
		String selectQuery = "SELECT  * FROM " + TABLE_NAME + " WHERE" 
				+ " receiver_id='" + Integer.toString(GlobalData.myUser.qbUser.getId()) + "'"
				+ " AND type='" + Integer.toString(Message.INVITE_MESSAGE) + "'";
 
        SQLiteDatabase db = App.getDBHelper().getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
 
        if (cursor.moveToFirst()) {
            do {
            	Message message = new Message();
            	message.id = cursor.getInt(0);
            	message.text = cursor.getString(1);
            	message.sender_qb_id = cursor.getInt(2);
            	message.receiver_qb_id = cursor.getInt(3);
            	message.type = cursor.getInt(4);
            	message.status = cursor.getInt(5);
            	message.time = cursor.getLong(6);
                arrRecord.add(message);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
		return arrRecord;
	}
	
	public static void deleteInvieteMessages(int sender_id) {
		
        SQLiteDatabase db = App.getDBHelper().getWritableDatabase();
        db.delete(TABLE_NAME, " sender_id = '" + Integer.toString(sender_id) + "'", null);
        db.close();
	}
	
	public static ArrayList<Message> getAllMessages() {
		ArrayList<Message> arrRecord = new ArrayList<Message>();
		String selectQuery = "SELECT  * FROM " + TABLE_NAME;
 
        SQLiteDatabase db = App.getDBHelper().getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
 
        if (cursor.moveToFirst()) {
            do {
            	Message message = new Message();
            	message.id = cursor.getInt(0);
            	message.text = cursor.getString(1);
            	message.sender_qb_id = cursor.getInt(2);
            	message.receiver_qb_id = cursor.getInt(3);
            	message.type = cursor.getInt(4);
            	message.status = cursor.getInt(5);
            	message.time = cursor.getLong(6);
                arrRecord.add(message);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
		return arrRecord;
	}
	

	
	public void parsePushMessage(String str) {
		try {
			JSONObject jsonObj = new JSONObject(str);
        	text = jsonObj.getString(FIELD_TEXT);
        	sender_qb_id = jsonObj.getInt(FIELD_SENDER_ID);
        	receiver_qb_id = jsonObj.getInt(FIELD_RECEIVER_ID);
        	type = jsonObj.getInt(FIELD_TYPE);
        	status = jsonObj.getInt(FIELD_STATUS);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public String getPushMessage() {
		String str = "";
		try {
			JSONObject jsonObj = new JSONObject();
			jsonObj.put(FIELD_TEXT, 		text);
			jsonObj.put(FIELD_SENDER_ID, 	sender_qb_id);
			jsonObj.put(FIELD_RECEIVER_ID, 	receiver_qb_id);
			jsonObj.put(FIELD_TYPE, 		type);
			jsonObj.put(FIELD_STATUS, 		status);
			str = jsonObj.toString();
			return str;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static void sendInviteMessage(int inviter_id) {
		Message message = Message.createInviteMessage(inviter_id);
		Message.sendMessage(message);
		Message.addMessage(message);
	}
	
	public static void sendAcceptMessage(int friend_id) {
		Message message = Message.createAcceptMessage(friend_id);
		Message.sendMessage(message);
		Message.addMessage(message);
	}
	
	public static Message createInviteMessage(int receiver_id) {
		Message message = new Message();
		message.text = "I want to chat with you.";
		message.type = Message.INVITE_MESSAGE;
		message.sender_qb_id = GlobalData.myUser.qbUser.getId();
		message.receiver_qb_id = receiver_id;
		message.status = Message.STATUS_UNREAD;
		
		return message;
	}
	
	public static Message createAcceptMessage(int receiver_id) {
		Message message = new Message();
		message.text = "I accept your invite. We can chat here.";
		message.type = Message.ACCEPT_MESSAGE;
		message.sender_qb_id = GlobalData.myUser.qbUser.getId();
		message.receiver_qb_id = receiver_id;
		message.status = Message.STATUS_UNREAD;
		
		return message;
	}
	
	public static Message createChatMessage(int receiver_id, String body) {
		Message message = new Message();
		message.text = body;
		message.type = Message.CHAT_MESSAGE;
		message.sender_qb_id = GlobalData.myUser.qbUser.getId();
		message.receiver_qb_id = receiver_id;
		message.status = Message.STATUS_UNREAD;
		
		return message;
	}
	
	public static void sendMessage(Message message) {
		
		StringifyArrayList<Integer> userIds = new StringifyArrayList<Integer>();
		userIds.add(message.receiver_qb_id);
		
		HashMap<String, String> data = new HashMap<String, String>();
		data.put("data.message", message.getPushMessage());
		data.put("data.type", GlobalData.myUser.qbUser.getFullName());
//		data.put("data.title", "fullName");

		QBEvent event = new QBEvent();
		event.setUserIds(userIds);
		event.setEnvironment(QBEnvironment.DEVELOPMENT);
		event.setNotificationType(QBNotificationType.PUSH);
		event.setPushType(QBPushType.GCM);
		event.setMessage(data);
		
        QBMessages.createEvent(event, new QBEntityCallbackImpl<QBEvent>() {
            @Override
            public void onSuccess(QBEvent qbEvent, Bundle bundle) {
            	Log.e("Message send", "success");
            }

            @Override
            public void onError(List<String> errors) {
//            	DialogUtility.show(ProfileActivity.this, errors.get(0));
            }
        });
	}
	
	public static void sendPrivateChatMessage(int opponentID, String body) {
		if (User.isOnline(opponentID)) {
			return;
		}
		
		Message message = Message.createChatMessage(opponentID, body);
		sendMessage(message);
	}
	
	public static void sendGroupChatMessage(QBDialog dialog, String body) {
		ArrayList<Integer> arrUserID = dialog.getOccupants();
		for (Integer userID: arrUserID) {
			if (User.isOnline(userID)) {
				continue;
			}
			Message message = Message.createChatMessage(userID, body);
			sendMessage(message);
		}
	}
}