package com.han.chatapp.model;

import java.util.ArrayList;

import android.media.ExifInterface;

public class Post {
	
	public String id = "";
	public String text = "";
	public String userSecretID = "";
	public String url = "";
	public String time = "";
	public int orientation = ExifInterface.ORIENTATION_NORMAL;
	
	
	public ArrayList<Comment> arrComment = new ArrayList<Comment>();
}
