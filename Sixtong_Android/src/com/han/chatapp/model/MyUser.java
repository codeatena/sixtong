package com.han.chatapp.model;

import java.util.ArrayList;

import com.han.chatapp.GlobalData;
import com.han.utility.PreferenceUtility;
import com.quickblox.chat.model.QBDialog;

public class MyUser extends User {


	public ArrayList<QBDialog> arrDialogs = new ArrayList<QBDialog>();

	public MyUser() {
		
	}
	
	public ArrayList<User> getFriends() {
		ArrayList<User> arrFriends = new ArrayList<User>();
		for (String friendID: GlobalData.myUser.arrFriendIDs) {
			User friend = User.getUserInAllUser(Integer.parseInt(friendID));
			if (friend != null) {
				arrFriends.add(friend);
			}
		}
		return arrFriends;
	}
	
	public ArrayList<User> getInviters() {
		ArrayList<User> arrInviters = new ArrayList<User>();
		for (String inviteID: GlobalData.myUser.arrInviterIDs) {
			User inviter = User.getUserInAllUser(Integer.parseInt(inviteID));
			if (inviter != null) {
				arrInviters.add(inviter);
			}
		}
		return arrInviters;
	}
	
	public void checkUpdatedContacts() {
		
		String strMyFriendIDs = PreferenceUtility.sharedPreferences.getString(PreferenceUtility.PREFERENCE_KEY_FRIEND_IDS, "");
		
		String[] myFriendIDs = strMyFriendIDs.split(",");
		
		for (String friendID: arrFriendIDs) {
			boolean isNewFriend = true;
			for (int i = 0; i < myFriendIDs.length; i++) {
				if (friendID.equals(myFriendIDs[i])) {
					isNewFriend = false;
					break;
				}
			}
			if (isNewFriend) {
				PreferenceUtility.increaseContactsMoment();
			}
		}
		
		boolean isFirstFriend = false;
		strMyFriendIDs = "";
		
		for (String friendID: arrFriendIDs) {
			if (!isFirstFriend) {
				strMyFriendIDs = friendID;
				isFirstFriend = true;
			} else {
				strMyFriendIDs = strMyFriendIDs + "," + friendID;
			}
		}
		
		PreferenceUtility.prefEditor.putString(PreferenceUtility.PREFERENCE_KEY_FRIEND_IDS, strMyFriendIDs);
		PreferenceUtility.prefEditor.commit();
		
		String strMyInviterIDs = PreferenceUtility.sharedPreferences.getString(PreferenceUtility.PREFERENCE_KEY_INVITER_IDS, "");
		String[] myInviterIDs = strMyInviterIDs.split(",");
		
		for (String inviterID: arrInviterIDs) {
			boolean isNewInviter = true;
			for (int i = 0; i < myInviterIDs.length; i++) {
				if (inviterID.equals(myInviterIDs[i])) {
					isNewInviter = false;
					break;
				}
			}
			if (isNewInviter) {
				PreferenceUtility.increaseContactsMoment();
			}
		}
		
		boolean isFirstInviter = false;
		strMyInviterIDs = "";
		
		for (String inviterID: arrInviterIDs) {
			if (!isFirstInviter) {
				strMyInviterIDs = inviterID;
				isFirstInviter = true;
			} else {
				strMyInviterIDs = strMyInviterIDs + "," + inviterID;
			}
		}
		
		PreferenceUtility.prefEditor.putString(PreferenceUtility.PREFERENCE_KEY_INVITER_IDS, strMyInviterIDs);
		PreferenceUtility.prefEditor.commit();
	}
}