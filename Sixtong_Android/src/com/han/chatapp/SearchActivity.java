package com.han.chatapp;

import java.util.ArrayList;

import com.han.chatapp.model.User;
import com.han.chatapp.user.ProfileActivity;
import com.han.quickblox.module.QBMyUserService;
import com.han.utility.ImageUtility;
import com.han.widget.listView.CellItem;
import com.han.widget.listView.MyListAdapter;
import com.han.widget.listView.MyListItem;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.MenuItemCompat.OnActionExpandListener;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.AdapterView.OnItemClickListener;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

public class SearchActivity extends Activity {

	ListView lstPeoples;
	MyListAdapter adpPeoples;
	ArrayList<User> arrUsers;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_simple_list);
		
		initWidget();
		initValue();
		initEvent();
	}
	
	private void initWidget() {
		lstPeoples = (ListView) findViewById(R.id.listView);
		ArrayList<MyListItem> arrItem = new ArrayList<MyListItem>();
		adpPeoples = new MyListAdapter(this, arrItem);
		lstPeoples.setAdapter(adpPeoples);
		lstPeoples.setDividerHeight(0);
	}
	
	private void initValue() {
		new QBMyUserService(this).readAllUsers();
	}
	
	private void initEvent() {
		lstPeoples.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				ProfileActivity.ownUser = arrUsers.get(position);
				startActivity(new Intent(SearchActivity.this, ProfileActivity.class));
			}
		});
	}
	
	public void successGetAllPeoples(ArrayList<User> _arrUsers) {
		arrUsers = _arrUsers;
		for (User user: arrUsers) {
			String userName = user.qbUser.getFullName();
			String gender = user.gender;
			CellItem cellItem = new CellItem(userName, gender, user.words, true);
			cellItem.setImageBitmap(ImageUtility.StringToBitmap(user.strPhoto));
			adpPeoples.add(cellItem);
		}
	}
	
	@SuppressLint("NewApi")
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.search_activity_action, menu);
	    MenuItem searchItem = menu.findItem(R.id.action_search);
	    SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
	    searchView.onActionViewExpanded();
	    ActionBar actionBar = SearchActivity.this.getActionBar();
	    actionBar.setHomeButtonEnabled(true);
	    
	    MenuItemCompat.setOnActionExpandListener(searchItem, new OnActionExpandListener() {
	        @Override
	        public boolean onMenuItemActionCollapse(MenuItem item) {
	        	Log.e("searchitem", "onMenuItemActionCollapse");

	            return true;
	        }

	        @Override
	        public boolean onMenuItemActionExpand(MenuItem item) {
	        	Log.e("searchitem", "onMenuItemActionExpand");

	            return true;
	        }
	    });
	    
	    return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle presses on the action bar items
	    switch (item.getItemId()) {
	        case android.R.id.home:
	        	Log.e("home", "back button clicked");
	        	finish();
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
}
