package com.han.chatapp.main;

import com.han.chatapp.GlobalData;
import com.han.chatapp.R;
import com.han.chatapp.post.MyPostsActivity;
import com.han.chatapp.settings.SettingsActivity;
import com.han.chatapp.user.MyProfileActivity;
import com.han.utility.ImageUtility;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MeFragment extends Fragment {
	
	ImageView imgPhoto;
	TextView txtFullName;
	TextView txtPhone;
//	Button btnSignout;
	RelativeLayout rlytMyProfile;
	RelativeLayout rlytMyPosts;
	RelativeLayout rlytSettings;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_me, container, false);
		
		imgPhoto = (ImageView) rootView.findViewById(R.id.my_photo_imageView);
		txtFullName = (TextView) rootView.findViewById(R.id.me_fullname_textView);
		txtPhone = (TextView) rootView.findViewById(R.id.me_phone_textView);
//		btnSignout = (Button) rootView.findViewById(R.id.signout_button);
		rlytMyProfile = (RelativeLayout) rootView.findViewById(R.id.my_profile_relativeLayout);
		rlytMyPosts = (RelativeLayout) rootView.findViewById(R.id.my_posts_relativeLayout);
		rlytSettings = (RelativeLayout) rootView.findViewById(R.id.settings_relativeLayout);
		
		initValue();
		initEvent();
		
		return rootView;
	}
	
	private void initValue() {
		if (GlobalData.myUser.strPhoto != null && !GlobalData.myUser.strPhoto.equals("")) {
			imgPhoto.setImageBitmap(ImageUtility.StringToBitmap(GlobalData.myUser.strPhoto));	
		}
		txtFullName.setText(GlobalData.myUser.qbUser.getFullName());
		txtPhone.setText("6tong No. " +  GlobalData.myUser.secretID);
	}
	
	private void initEvent() {
//		btnSignout.setOnClickListener(new Button.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				signout();
////				uploadPhoto();
//			}
//        });
		rlytMyPosts.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(getActivity(), MyPostsActivity.class));
			}
        });
		rlytMyProfile.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(getActivity(), MyProfileActivity.class));
			}
        });
		rlytSettings.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				getActivity().startActivityForResult(new Intent(getActivity(), SettingsActivity.class), MainActivity.REQUEST_CODE_SETTINGS);
			}
        });
	}
}