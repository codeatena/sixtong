package com.han.chatapp.main;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationRequest;
import com.han.chatapp.GlobalData;
import com.han.chatapp.R;
import com.han.chatapp.SearchActivity;
import com.han.chatapp.StartActivity;
import com.han.chatapp.chat.CreateGroupChatActivity;
import com.han.chatapp.settings.SettingsActivity;
//import com.han.contact.MobileContactsActivity;
import com.han.quickblox.module.QBDialogService;
import com.han.quickblox.module.QBRuntimeService;
import com.han.utility.DialogUtility;
import com.han.utility.LocationUtility;
import com.han.utility.PreferenceUtility;
import com.han.utility.TabsUtility;
import com.han.utility.TimeUtility;
import com.quickblox.chat.model.QBChatMessage;
import com.quickblox.chat.model.QBDialog;
import com.quickblox.chat.model.QBDialogType;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.QBEntityCallbackImpl;
import com.quickblox.location.QBLocations;
import com.quickblox.location.model.QBLocation;
import com.quickblox.messages.QBMessages;
import com.quickblox.messages.model.QBEnvironment;
import com.quickblox.messages.model.QBSubscription;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

@SuppressLint("NewApi")
@SuppressWarnings("deprecation")

public class MainActivity extends FragmentActivity implements
	ActionBar.TabListener, LocationListener,
	GooglePlayServicesClient.ConnectionCallbacks,
	GooglePlayServicesClient.OnConnectionFailedListener {

	private static MainActivity instance;
	
	Timer timer = null;
	
	private LocationRequest mLocationRequest;
    private LocationClient mLocationClient;
	private LocationManager locationManager;
	
	private ViewPager viewPager;
	private TabsPagerAdapter mAdapter;
	public ActionBar actionBar;
	
	public static ChatsFragment 	fragChats;
	public static ContactsFragment 	fragContacts;
	public static DiscoverFragment 	fragDiscover;
	public static MeFragment 		fragMe;

	public static final int REQUEST_CODE_SETTINGS = 1008;
	
	public static final int TAB_INDEX_CHATS = 0;
	public static final int TAB_INDEX_CONTACTS = 1;
	public static final int TAB_INDEX_DISCOVER = 2;
	public static final int TAB_INDEX_ME = 3;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		instance = this;
		
		initWidget();
		initValue();
		initEvent();
	}
	
	public static MainActivity getInstatnce() {
		return instance;
	}
	
	private void initWidget() {
		
		fragChats = new ChatsFragment();
		fragContacts = new ContactsFragment();
		fragDiscover = new DiscoverFragment();
		fragMe = new MeFragment();
		
		viewPager = (ViewPager) findViewById(R.id.pager);
		actionBar = getActionBar();
		mAdapter = new TabsPagerAdapter(getSupportFragmentManager());

		viewPager.setAdapter(mAdapter);
		actionBar.setHomeButtonEnabled(false);
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		
		// Adding Tabs
//		for (String tab_name: tabs) {
//			actionBar.addTab(actionBar.newTab().setText(tab_name).setCustomView(R.layout.tab_item)
//					.setTabListener(this));
//		}
		
		actionBar.addTab(actionBar.newTab()
			    .setCustomView(TabsUtility.renderTabView(this, R.string.chats, R.drawable.tab_orange, 0))
			    .setTabListener(this));
		
		actionBar.addTab(actionBar.newTab()
			    .setCustomView(TabsUtility.renderTabView(this, R.string.contacts, R.drawable.tab_orange, 0))
			    .setTabListener(this));
		
		actionBar.addTab(actionBar.newTab()
			    .setCustomView(TabsUtility.renderTabView(this, R.string.discover, R.drawable.tab_orange, 0))
			    .setTabListener(this));
		
		actionBar.addTab(actionBar.newTab()
			    .setCustomView(TabsUtility.renderTabView(this, R.string.me, R.drawable.tab_orange, 0))
			    .setTabListener(this));
	}
	
	private void initValue() {

		initLocation();
		subscribeToPushNotifications();

//		readMyMoreInfo();
		
//		Message.deleteAll();
		/*
		ArrayList<Message> arrInviteMessage = Message.getInviteMessage();
		Log.e("invite message count", Integer.toString(arrInviteMessage.size()));
		
		if (arrInviteMessage.size() > 0) {
			Log.e("id", Integer.toString(arrInviteMessage.get(0).id));
			Log.e("text", arrInviteMessage.get(0).text);
			Log.e("sender_id", Integer.toString(arrInviteMessage.get(0).sender_qb_id));
			Log.e("receiver_id", Integer.toString(arrInviteMessage.get(0).receiver_qb_id));
			Log.e("type", Integer.toString(arrInviteMessage.get(0).type));
			Log.e("status", Integer.toString(arrInviteMessage.get(0).status));
			Log.e("time", Long.toString(arrInviteMessage.get(0).time));
		}
		*/
	}
	
	private void initLocation() {
		mLocationRequest = LocationRequest.create();
        mLocationRequest.setInterval(LocationUtility.UPDATE_INTERVAL_IN_MILLISECONDS);

        // Use high accuracy
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        // Set the interval ceiling to one minute
        mLocationRequest.setFastestInterval(LocationUtility.FAST_INTERVAL_CEILING_IN_MILLISECONDS);

        mLocationClient = new LocationClient(this, this, this);
        
		locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
	   	
        // Creating a criteria object to retrieve provider
        Criteria criteria = new Criteria();

        // Getting the name of the best provider
        String provider = locationManager.getBestProvider(criteria, true);

        // Getting Current Location
        Location location = locationManager.getLastKnownLocation(provider);
//        searchAction(location);
        
        onLocationChanged(location);
        
        locationManager.requestLocationUpdates(provider, 200, 1, this);
	}
	
	public void subscribeToPushNotifications() {
		String deviceId = "";

        final TelephonyManager mTelephony = (TelephonyManager) this.getSystemService(
                Context.TELEPHONY_SERVICE);
        if (mTelephony.getDeviceId() != null) {
            deviceId = mTelephony.getDeviceId(); //*** use for mobiles
        } else {
            deviceId = Settings.Secure.getString(this.getContentResolver(),
                    Settings.Secure.ANDROID_ID); //*** use for tablets
        }
        
        String registrationID = PreferenceUtility.sharedPreferences.getString(PreferenceUtility.PREFERENCE_KEY_PUSH_REG_ID, "");
        Log.e("device id, reg id", deviceId + ", " + registrationID);
        
	    QBMessages.subscribeToPushNotificationsTask(registrationID, deviceId, QBEnvironment.DEVELOPMENT,
	    		new QBEntityCallback<ArrayList<QBSubscription>>() {
			@Override
			public void onError(List<String> erros) {
				// TODO Auto-generated method stub
				DialogUtility.show(MainActivity.this, erros.get(0));
			}

			@Override
			public void onSuccess() {
				// TODO Auto-generated method stub
				Log.e("subscription", "success");
			}

			@Override
			public void onSuccess(ArrayList<QBSubscription> arg0, Bundle arg1) {
				// TODO Auto-generated method stub
				Log.e("subscription", "success with arraylist");
			}
	    });
	}
	
	public void addPrivateChatMessage(final QBChatMessage chatMessage) {
		
		runOnUiThread(new Runnable() {
    		public void run() {
    			int senderID = chatMessage.getSenderId();
    			for (QBDialog dialog: GlobalData.myUser.arrDialogs) {
    				if (dialog.getType().equals(QBDialogType.PRIVATE)) {
    					int opponentID = QBDialogService.getOpponentIDForPrivateDialog(dialog);
    					if (senderID == opponentID) {
    						dialog.setUnreadMessageCount(dialog.getUnreadMessageCount() + 1);
    						dialog.setLastMessage(chatMessage.getBody());
    						dialog.setLastMessageDateSent(TimeUtility.getCurrentTimeStamp() / 1000);
    					}
    				}
    			}
    			updateChats();
    		}
    	});
	}
	
	public void addGroupChatMessage(final QBDialog dialog, final QBChatMessage chatMessage) {
		runOnUiThread(new Runnable() {
    		public void run() {
    			
    			if (dialog == null) return;
    			
				dialog.setUnreadMessageCount(dialog.getUnreadMessageCount() + 1);
				dialog.setLastMessage(chatMessage.getBody());
				dialog.setLastMessageDateSent(TimeUtility.getCurrentTimeStamp() / 1000);
				
	    		updateChats();
    		}
		});
	}
	
	private void initEvent() {
		viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				// on changing the page
				// make respected tab selected
				actionBar.setSelectedNavigationItem(position);
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
				
			}
		});
        
//		new QBRuntimeService().startService();

		if (timer != null) {
			timer.cancel();
			timer = null;
		}
		timer = new Timer();
		timer.schedule(new TimerTask() {
	        @Override
	        public void run() {
	        	runOnUiThread(new Runnable() {
	        		public void run() {
	    	    		new QBRuntimeService().startService();
	        		}
	        	});
	        }
		}, 0, 5000);
		
		updateBadge(TAB_INDEX_CONTACTS, PreferenceUtility.getContactsBadge());
	}
	
	public void updateBadge(int nTabIndex, int nBadgeNumber) {
		
		if (actionBar == null) return;
		if (actionBar.getTabCount() <= nTabIndex) return;
		if (actionBar.getTabAt(nTabIndex) == null) return;
		
		if (PreferenceUtility.isEnableMomentsUpdates()) {
			if (nTabIndex == viewPager.getCurrentItem()) {
				PreferenceUtility.clearMoment(nTabIndex);
				if (nTabIndex == TAB_INDEX_CHATS) {
					TabsUtility.updateTabBadge(actionBar.getTabAt(nTabIndex), nBadgeNumber);
				} else {
					TabsUtility.updateTabBadge(actionBar.getTabAt(nTabIndex), 0);
				}
			} else {
				TabsUtility.updateTabBadge(actionBar.getTabAt(nTabIndex), nBadgeNumber);
			}
		} else {
			TabsUtility.updateTabBadge(actionBar.getTabAt(nTabIndex), 0);
		}
	}
	
	public void updateChats() {

		int k = 0;
		
		if (GlobalData.myUser.arrDialogs != null) {
			for (QBDialog dialog: GlobalData.myUser.arrDialogs) {
				k += dialog.getUnreadMessageCount();
			}
		}
		
		updateBadge(TAB_INDEX_CHATS, k);
		
		if (fragChats != null)
			fragChats.initValue();
	}
	
	public void updateContacts() {
		
		int nContactsBadge = PreferenceUtility.getContactsBadge();
		
		updateBadge(MainActivity.TAB_INDEX_CONTACTS, nContactsBadge);
		
		if (fragContacts != null) {
			fragContacts.initValue();
		}
	}
	
	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
		
	}

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		// on tab selected
		// show respected fragment view
		
		Log.e("onTabSelected", Integer.toString(tab.getPosition()));
		viewPager.setCurrentItem(tab.getPosition());
		
		if (tab.getPosition() == TAB_INDEX_CHATS) {
			updateBadge(TAB_INDEX_CONTACTS, 0);
		}
		if (tab.getPosition() == TAB_INDEX_CONTACTS) {
			PreferenceUtility.clearMoment(TAB_INDEX_CONTACTS);
			updateBadge(TAB_INDEX_CONTACTS, 0);
		}
	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    // Inflate the menu items for use in the action bar
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.main_activity_action, menu);
	    return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle presses on the action bar items
	    switch (item.getItemId()) {
	        case R.id.action_search:
	            openSearch();
	            return true;
	        case R.id.add_contacts:
//	        	addContacts();
	        	return true;
	        case R.id.group_chat:
	        	makeGrouChat();
	        	return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
	
	private void openSearch() {
		startActivity(new Intent(this, SearchActivity.class));
	}
	
//	private void addContacts() {
//		startActivity(new Intent(this, MobileContactsActivity.class));
//	}
	
	private void makeGrouChat() {
		startActivity(new Intent(this, CreateGroupChatActivity.class));
	}
	
	public void searchAction(Location location) {
		
		if (location == null) {
			DialogUtility.showGeneralAlert(this, "GPS Error!", 
				"Can't get current position.\nPlease check your phone's Location Service.");
			return;
		}
		
		String strLat = Double.toString(location.getLatitude());
		String strLng = Double.toString(location.getLongitude());
		
	    GlobalData.curLat = location.getLatitude();
	    GlobalData.curLng = location.getLongitude();
	    
		QBLocation qbLocation = new QBLocation(GlobalData.curLat, GlobalData.curLng, "");
		QBLocations.createLocation(qbLocation, new QBEntityCallbackImpl<QBLocation>() {
			@Override
            public void onSuccess(QBLocation qbLocation, Bundle bundle) {

            }

            @Override
            public void onError(List<String> errors) {

            }
		});
	    
		Log.e("Latitude, Longitude = ", strLat + " " + strLng);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
		if (requestCode == REQUEST_CODE_SETTINGS) {
            if (resultCode == SettingsActivity.RESULT_CODE_SIGN_OUT) {
            	startActivity(new Intent(MainActivity.this, StartActivity.class));
            	finish();
            }
        }
	}
	
    @Override
    protected void onResume() {
	    Log.e("MainActivity", "resume");
        super.onResume();
    }
    
    @Override
    protected void onPause() {
	    Log.e("MainActivity", "pause");
        super.onPause();
    }
    
	@Override
    protected void onDestroy() {
		if (timer != null) {
			timer.cancel();
			timer = null;
		}
		
		instance = null;
        super.onDestroy();
    }

	@Override
	public void onConnectionFailed(ConnectionResult arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onConnected(Bundle arg0) {
		// TODO Auto-generated method stub
//        Toast.makeText(this, "GPS Connected", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onDisconnected() {
		// TODO Auto-generated method stub
//        Toast.makeText(this, "GPS Disconnected.", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onLocationChanged(Location arg0) {
		// TODO Auto-generated method stub
	    searchAction(arg0);
	}

	@Override
	public void onProviderDisabled(String arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onProviderEnabled(String arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
		// TODO Auto-generated method stub
	}
	
	public void onStart() {
        super.onStart();
        mLocationClient.connect();
        
	    Log.e("MainActivity", "start");
	}
	
	public void onStop() {
        mLocationClient.disconnect();
	    locationManager.removeUpdates(this);
	    super.onStop();
	    
	    Log.e("MainActivity", "stop");
	} 
}