package com.han.chatapp.main;

/**
 * Created by igorkhomenko on 9/12/14.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.han.chatapp.R;
import com.han.chatapp.model.User;
import com.han.quickblox.module.QBDialogService;
import com.han.utility.ImageUtility;
import com.han.utility.TimeUtility;
import com.quickblox.chat.model.QBDialog;
import com.quickblox.chat.model.QBDialogType;

import java.util.ArrayList;

public class DialogAdapter extends ArrayAdapter<QBDialog> {

    public DialogAdapter(Context context, ArrayList<QBDialog> dialogs) {
        super(context, 0, dialogs);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

    	View view = convertView;
        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            view = inflater.inflate(R.layout.row_dialog, parent, false);
        }
        
        // Populate the data into the template view using the data object
        ImageView imgDialog = (ImageView) view.findViewById(R.id.dialog_imageView);
        TextView txtName = (TextView) view.findViewById(R.id.dialog_name_textView);
        TextView txtLastMessage = (TextView) view.findViewById(R.id.dialog_last_message_textView);
        TextView txtTime = (TextView) view.findViewById(R.id.dialog_time_textView);
        TextView txtBadge = (TextView) view.findViewById(R.id.badge_textView);
        
        QBDialog dialog = this.getItem(position);

        if (dialog.getType().equals(QBDialogType.GROUP)) {
        	txtName.setText(dialog.getName());
        	imgDialog.setImageResource(R.drawable.ic_action_group);
        }
        if (dialog.getType().equals(QBDialogType.PRIVATE)) {
        	imgDialog.setImageResource(R.drawable.ic_action_person);
            Integer opponentID = QBDialogService.getOpponentIDForPrivateDialog(dialog);
            User user = User.getUserFromQBUserID(opponentID);
            if (user == null) {
            	user = new User();
            	user.getInfFromServer(opponentID);
            } else {
            	txtName.setText(user.qbUser.getFullName());
            	if (user.strPhoto != null && !user.strPhoto.equals("")) {
            		imgDialog.setImageBitmap(ImageUtility.StringToBitmap(user.strPhoto));
            	}
            }
        }
        
    	txtLastMessage.setText(dialog.getLastMessage());
    	txtTime.setText(TimeUtility.getStringFromTimeStamp(dialog.getLastMessageDateSent() * 1000, "yyyy-MM-dd HH:mm:ss"));
    	
    	if (dialog.getUnreadMessageCount() == 0) {
    		txtBadge.setVisibility(View.GONE);
    	} else {
    		txtBadge.setVisibility(View.VISIBLE);
        	txtBadge.setText(Integer.toString(dialog.getUnreadMessageCount()));
    	}
    	
//    	ChatManager chat;
    	
//		if (dialog.getType().equals(QBDialogType.PRIVATE)) {
//            int receiver_id = QBDialogService.getOpponentIDForPrivateDialog(dialog);
//            chat = new PrivateChatManagerImpl(getContext(), receiver_id);
//		}
//		if (dialog.getType().equals(QBDialogType.GROUP)) {
//            chat = new GroupChatManagerImpl(this);
//
//			((GroupChatManagerImpl) chat).joinGroupChat(dialog, new QBEntityCallbackImpl() {
//                @Override
//                public void onSuccess() {
//                	Log.e("GroupChatManagerImpl join", "success");
//                }
//                
//                @Override
//                public void onError(List list) {
//                    AlertDialog.Builder dialog = new AlertDialog.Builder(ChatActivity.this);
//                    dialog.setMessage("error when join group chat: " + list.toString()).create().show();
//                }
//            });
//		}
		
        return view;
    }
}