package com.han.chatapp.main;

import java.util.ArrayList;
import java.util.List;
import com.han.chatapp.GlobalData;
import com.han.chatapp.R;
import com.han.chatapp.model.Message;
import com.han.chatapp.model.User;
import com.han.chatapp.user.ProfileActivity;
import com.han.quickblox.module.QBUserService;
import com.han.utility.ImageUtility;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class ContactsFragment extends Fragment {

	final int BUTTON_ID = 1000;
	
	ArrayList<User> arrUsers = new ArrayList<User>();
	ListView lstSearchedUser;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.fragment_contacts, container, false);
		
		lstSearchedUser = (ListView) view.findViewById(R.id.searched_user_listView);
		Log.e("my contactsfragment", "refresh");

		initValue();
		initEvent();

		return view;
	}
	
	public void initValue() {
		arrUsers = new ArrayList<User>();
		arrUsers.addAll(GlobalData.myUser.getFriends());
		arrUsers.addAll(GlobalData.myUser.getInviters());
		
		lstSearchedUser.setAdapter(new UserAdapter(this.getActivity(), R.layout.row_user, arrUsers));
		lstSearchedUser.setItemsCanFocus(true);
	}
	
	private void initEvent() {
		lstSearchedUser.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				// TODO Auto-generated method stub
				Log.e("list click position", Integer.toString(position));
				User user = arrUsers.get(position);
				ProfileActivity.ownUser = user;
				ContactsFragment.this.startActivity(new Intent(ContactsFragment.this.getActivity(), ProfileActivity.class));
//				Contact contact = arrSrchContact.get(position);
			}
        });
	}
	
	public class UserAdapter extends ArrayAdapter<User> {

		public UserAdapter(Context context, int _row_id, List<User> _arrData) {
			// TODO Auto-generated constructor stub
			super(context, _row_id, _arrData);
		}
		
		@SuppressLint("ViewHolder")
		public View getView(int position, View convertView, ViewGroup parent) {
	        View row = convertView;
	        LayoutInflater inflater = LayoutInflater.from(getContext());
	        row = inflater.inflate(R.layout.row_user, parent, false);
	        
	        TextView txtFullName = (TextView) row.findViewById(R.id.me_fullname_textView);
	        ImageView imgPhoto = (ImageView) row.findViewById(R.id.user_photo_imageView);
	        Button btnAccept = (Button) row.findViewById(R.id.accept_button);
	        
	        User user = getItem(position);
	        txtFullName.setText(user.qbUser.getFullName());
	        
	        if (user.strPhoto == null || user.strPhoto.equals("")) {
		        imgPhoto.setImageResource(R.drawable.ic_action_person);
	        } else {
	        	imgPhoto.setImageBitmap(ImageUtility.StringToBitmap(user.strPhoto));
	        }
	        
	        if (user.isMyInviter()) {
	        	btnAccept.setVisibility(View.VISIBLE);
	        } else {
	        	btnAccept.setVisibility(View.GONE);
	        }

	        btnAccept.setId(BUTTON_ID + position);
	        btnAccept.setOnClickListener(new Button.OnClickListener() {
				@Override
				public void onClick(View v) {
					int position = v.getId() - BUTTON_ID;
					User user = getItem(position);
					Message.sendAcceptMessage(user.qbUser.getId());
					new QBUserService(getActivity()).acceptInvite(user);
				}
	        });
			
	        return row;
		}
    }
}