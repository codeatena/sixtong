package com.han.chatapp.main;

import com.han.chatapp.R;
import com.han.chatapp.post.PostsActivity;
import com.han.chatapp.user.PeopleAroundActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

public class DiscoverFragment extends Fragment {

	RelativeLayout rlytBulletinBoard;
	RelativeLayout rlytPeopleAround;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_discover, container, false);
		
		rlytBulletinBoard = (RelativeLayout) rootView.findViewById(R.id.bulletin_board_relativeLayout);
		rlytPeopleAround = (RelativeLayout) rootView.findViewById(R.id.people_around_relativeLayout);
		
		initValue();
		initEvent();
		
		return rootView;
	}
	
	private void initValue() {
		
	}
	
	private void initEvent() {
		rlytBulletinBoard.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				PostsActivity.ownUser = null;
				getActivity().startActivity(new Intent(getActivity(), PostsActivity.class));
			}
		});
		
		rlytPeopleAround.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				getActivity().startActivity(new Intent(getActivity(), PeopleAroundActivity.class));
			}
		});
	}
}