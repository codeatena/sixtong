package com.han.chatapp.main;

import java.util.ArrayList;
import com.han.chatapp.GlobalData;
import com.han.chatapp.R;
import com.han.chatapp.chat.ChatActivity;
import com.quickblox.chat.model.QBDialog;
import com.quickblox.chat.model.QBDialogType;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

public class ChatsFragment extends Fragment {

	ListView lstDialogs;
	DialogAdapter dialogAdapter;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_chats, container, false);
		
		Log.e("my chatsfragment", "refresh");

		lstDialogs = (ListView) rootView.findViewById(R.id.dialog_listView);
		
		initValue();
		initEvent();
		
		return rootView;
	}

	public void initValue() {
		if (this.getActivity() == null) return;
		
		ArrayList<QBDialog> arrDialog = new ArrayList<QBDialog>();
		
		for (QBDialog dialog: GlobalData.myUser.arrDialogs) {
			arrDialog.add(dialog);
		}
		
		for (int i = arrDialog.size(); i > 0; i--) {
			QBDialog tmpDialog = arrDialog.get(i - 1);
			if (tmpDialog.getType().equals(QBDialogType.GROUP)) {
				QBDialog dialog = GlobalData.myUser.arrDialogs.get(i - 1);
				GlobalData.myUser.arrDialogs.remove(i - 1);
				GlobalData.myUser.arrDialogs.add(0, dialog);
			}
		}
		
        dialogAdapter = new DialogAdapter(this.getActivity(), GlobalData.myUser.arrDialogs);
        lstDialogs.setAdapter(dialogAdapter);
	}
	
	private void initEvent() {
        lstDialogs.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                QBDialog selectedDialog = (QBDialog) dialogAdapter.getItem(position);
            	ChatActivity.dialog = selectedDialog;
                ChatsFragment.this.getActivity().startActivity(new Intent(ChatsFragment.this.getActivity(), ChatActivity.class));
            }
        });
	}
	
//	public void addPrivateChatMessage(final QBChatMessage chatMessage) {
//		getActivity().runOnUiThread(new Runnable() {
//    		public void run() {
//    			int senderID = chatMessage.getSenderId();
//    			Log.e("sender id", Integer.toString(senderID));
//    			
//    			for (QBDialog dialog: GlobalData.myUser.arrDialogs) {
//    				if (dialog.getType().equals(QBDialogType.PRIVATE)) {
//    					int opponentID = QBDialogService.getOpponentIDForPrivateDialog(dialog);
//    					if (senderID == opponentID) {
//    						dialog.setUnreadMessageCount(dialog.getUnreadMessageCount() + 1);
//    					}
//    				}
//    			}
//    			
//    			initValue();
//    		}
//    	});
//	}
}