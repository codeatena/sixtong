package com.han.chatapp.main;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

public class TabsPagerAdapter extends FragmentPagerAdapter {

	final int COUNT_TAB = 4;
	
	public TabsPagerAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int index) {

		Log.e("pager index", Integer.toString(index));
		
		switch (index) {
			case 0:
				return MainActivity.fragChats;
			case 1:
				return MainActivity.fragContacts;
			case 2:
				return MainActivity.fragDiscover;
			case 3:
				return MainActivity.fragMe;
		}

		return null;
	}

	@Override
	public int getCount() {
		// get item count - equal to number of tabs
		return COUNT_TAB;
	}
}
