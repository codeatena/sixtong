package com.han.chatapp.settings;

import com.han.chatapp.R;
import com.han.utility.PreferenceUtility;
import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;

public class SettingsNotificationsActivity extends Activity {

	Switch swchNewMessageAlerts;
	Switch swchNotificationCenter;
	Switch swchSound;
	Switch swchInAppVibrate;
	Switch swchMomentUpdates;
	
	RelativeLayout rlytNewMessageAlerts;
	RelativeLayout rlytNotificationCenter;

	LinearLayout llytSoundSetting;
	RelativeLayout rltySound;
	LinearLayout lltyAlertSound;
	RelativeLayout rltyInAppVibrate;
	
	RelativeLayout rltyMomentUpdates;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_notifications);

	    ActionBar actionBar = getActionBar();
	    actionBar.setHomeButtonEnabled(true);
	    
	    initWidget();
	    initValue();
	    initEvent();
	}
	
	private void initWidget() {
		swchNewMessageAlerts = (Switch) findViewById(R.id.new_message_alerts_switch);
		swchNotificationCenter = (Switch) findViewById(R.id.notification_center_switch);
		swchSound = (Switch) findViewById(R.id.sound_switch);
		swchInAppVibrate = (Switch) findViewById(R.id.in_app_vibrate_switch);
		swchMomentUpdates = (Switch) findViewById(R.id.moment_updates_switch);
		
		rlytNewMessageAlerts = (RelativeLayout) findViewById(R.id.new_message_alerts_relativeLayout);
		rlytNotificationCenter = (RelativeLayout) findViewById(R.id.notification_center_relativeLayout);
		
		llytSoundSetting = (LinearLayout) findViewById(R.id.sound_setting_linearLayout);
		
		rltySound = (RelativeLayout) findViewById(R.id.sound_relativeLayout);
		lltyAlertSound = (LinearLayout) findViewById(R.id.alert_sound_linearLayout);
		rltyInAppVibrate = (RelativeLayout) findViewById(R.id.in_app_vibrate_relativeLayout);
		
		rltyMomentUpdates = (RelativeLayout) findViewById(R.id.moment_updates_relativeLayout);
	}
	
	private void initValue() {
		swchNewMessageAlerts.setChecked(PreferenceUtility.isEnableNewMessageAlerts());
		swchNotificationCenter.setChecked(PreferenceUtility.isEnableNotificationCenter());
		swchSound.setChecked(PreferenceUtility.isEnableSound());
		swchInAppVibrate.setChecked(PreferenceUtility.isEnableInAppVibrate());
		swchMomentUpdates.setChecked(PreferenceUtility.isEnableMomentsUpdates());
		
		if (PreferenceUtility.isEnableNewMessageAlerts()) {
			swchNotificationCenter.setVisibility(View.VISIBLE);
			llytSoundSetting.setVisibility(View.VISIBLE);
		} else {
			swchNotificationCenter.setVisibility(View.GONE);
			llytSoundSetting.setVisibility(View.GONE);
		}
		
		if (PreferenceUtility.isEnableSound()) {
			lltyAlertSound.setVisibility(View.VISIBLE);
		} else {
			lltyAlertSound.setVisibility(View.GONE);
		}
	}
	
	private void initEvent() {
		swchNewMessageAlerts.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
		    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked) {
					swchNotificationCenter.setVisibility(View.VISIBLE);
					llytSoundSetting.setVisibility(View.VISIBLE);
				} else {
					swchNotificationCenter.setVisibility(View.GONE);
					llytSoundSetting.setVisibility(View.GONE);
				}
				PreferenceUtility.setEnableNewMessageAlerts(isChecked);
		    }
		});
		swchNotificationCenter.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
		    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				PreferenceUtility.setEnableNotficationCenter(isChecked);
		    }
		});
		swchSound.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
		    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked) {
					lltyAlertSound.setVisibility(View.VISIBLE);
				} else {
					lltyAlertSound.setVisibility(View.GONE);
				}
				PreferenceUtility.setEnableSound(isChecked);
		    }
		});
		swchInAppVibrate.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
		    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				PreferenceUtility.setEnableInAppVibrate(isChecked);
		    }
		});
		swchMomentUpdates.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
		    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				PreferenceUtility.setEnableMomentsUpdates(isChecked);
		    }
		});
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle presses on the action bar items
	    switch (item.getItemId()) {
	        case android.R.id.home:
	        	finish();
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
}