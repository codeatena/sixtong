package com.han.chatapp.settings;

import java.util.ArrayList;
import java.util.Locale;

import com.han.chatapp.R;
import com.han.utility.PreferenceUtility;
import com.han.widget.listView.CellItem;
import com.han.widget.listView.MyListAdapter;
import com.han.widget.listView.MyListItem;
import com.han.widget.listView.SectionItem;
import android.app.ActionBar;
import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class LanguageActivity extends Activity {

	ListView lstLanguage;
	MyListAdapter adpLanguage;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_simple_list);

	    ActionBar actionBar = LanguageActivity.this.getActionBar();
	    actionBar.setHomeButtonEnabled(true);
	    
	    initWidget();
	    initValue();
	    initEvent();
	}
	
	private void initWidget() {
		lstLanguage = (ListView) findViewById(R.id.listView);
	}
	
	private void initValue() {
		ArrayList<MyListItem> arrItem = new ArrayList<MyListItem>();
		
		String[] languages = getResources().getStringArray(R.array.languages_array);
		
		arrItem.add(new SectionItem(""));
		
		int nCurLanguage = PreferenceUtility.sharedPreferences.getInt(PreferenceUtility.PREFERENCE_KEY_LANGUAGE, PreferenceUtility.LANGUAGE_AUTO);
		
		for (int i = 0; i < languages.length; i++) {
			arrItem.add(new CellItem(languages[i], R.drawable.abc_ic_cab_done_holo_light, false));
		}
		
		adpLanguage = new MyListAdapter(this, arrItem);
		adpLanguage.nSelectedItem = nCurLanguage;
		
		lstLanguage.setAdapter(adpLanguage);
		lstLanguage.setDividerHeight(0);
	}
	
	private void initEvent() {
		lstLanguage.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				adpLanguage.nSelectedItem = position;			
				adpLanguage.notifyDataSetChanged();
			}
		});
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    // Inflate the menu items for use in the action bar
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.edit_name_activity_action, menu);

	    return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle presses on the action bar items
	    switch (item.getItemId()) {
	        case android.R.id.home:
	        	finish();
	            return true;
	        case R.id.action_save:
	        	actionSave();
	        	return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
	
	private void actionSave() {

		PreferenceUtility.prefEditor.putInt(PreferenceUtility.PREFERENCE_KEY_LANGUAGE, adpLanguage.nSelectedItem);
		PreferenceUtility.prefEditor.commit();
		
//		App.getInstance().setLanguage();
//		setLanguage();
		
		finish();
	}
	
    public void setLanguage() {
    	
    	int nCurLanguage = PreferenceUtility.sharedPreferences.getInt(PreferenceUtility.PREFERENCE_KEY_LANGUAGE, PreferenceUtility.LANGUAGE_AUTO);
    	Locale locale = null;
    	Log.e("current locale", Locale.getDefault().getLanguage());
    	
    	if (nCurLanguage == 1) {
    		locale = new Locale(Locale.getDefault().getLanguage());
    	}
    	if (nCurLanguage == 2) { // auto
    		locale = new Locale("zh");	
    	}
    	if (nCurLanguage == 3) {
    		locale = new Locale("");
    	}
    	
	    Locale.setDefault(locale);
	    Configuration config = new Configuration();
	    config.locale = locale;
	    
	    getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
    }
}