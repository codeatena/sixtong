package com.han.chatapp.settings;

import com.han.chatapp.R;
import com.han.utility.PreferenceUtility;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class SettingsChatActivity extends Activity {

	RelativeLayout rlytFontSize;
	TextView txtFontSize;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings_chat);

	    ActionBar actionBar = SettingsChatActivity.this.getActionBar();
	    actionBar.setHomeButtonEnabled(true);
	    
	    initWidget();
	    initValue();
	    initEvent();
	}
	
	private void initWidget() {
		rlytFontSize = (RelativeLayout) findViewById(R.id.font_size_relatvieLayout);
		txtFontSize = (TextView) findViewById(R.id.my_words_textView);
	}
	
	private void initValue() {
		txtFontSize.setText(PreferenceUtility.getFontSizeString());
	}
	
	private void initEvent() {
		rlytFontSize.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
					
					private final int FONT_SIZE_SMALL = 0;
					private final int FONT_SIZE_MEDIUM = 1;
					private final int FONT_SIZE_LARGE = 2;
					private final int FONT_SIZE_X_LARGE = 3;
					private final int FONT_SIZE_XX_LARGE = 4;

					@Override
					public void onClick(DialogInterface dialog, int which) {
						switch (which) {
							case FONT_SIZE_SMALL:
								PreferenceUtility.setFontSize(0);
								initValue();
								break;
							case FONT_SIZE_MEDIUM:
								PreferenceUtility.setFontSize(1);
								initValue();
								break;
							case FONT_SIZE_LARGE:
								PreferenceUtility.setFontSize(2);
								initValue();
								break;
							case FONT_SIZE_X_LARGE:
								PreferenceUtility.setFontSize(3);
								initValue();
								break;
							case FONT_SIZE_XX_LARGE:
								PreferenceUtility.setFontSize(4);
								initValue();
								break;
						}
					}
				};
				
				new AlertDialog.Builder(SettingsChatActivity.this)
					.setTitle(R.string.font_size)
			    	.setItems(R.array.font_size_array, listener)
			    	.setCancelable(true)
			    	.setPositiveButton(R.string.cancel, new DialogInterface.OnClickListener() {
			    		public void onClick(DialogInterface dialog, int which) { 
			    			
			    		}
			    	})
			    	.show();
			}
        });
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle presses on the action bar items
	    switch (item.getItemId()) {
	        case android.R.id.home:
	        	finish();
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
}
