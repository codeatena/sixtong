package com.han.chatapp.settings;

import com.han.chatapp.R;
import com.han.utility.PreferenceUtility;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class SettingsGeneralActivity extends Activity {

	RelativeLayout rlytLanguage;
	TextView txtLanguage;
	
	static int REQUEST_CODE_LANGUAGE = 1009;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings_general);

	    ActionBar actionBar = SettingsGeneralActivity.this.getActionBar();
	    actionBar.setHomeButtonEnabled(true);
	    
	    initWidget();
	    initValue();
	    initEvent();
	}
	
	private void initWidget() {
		rlytLanguage = (RelativeLayout) findViewById(R.id.language_relativeLayout);
		txtLanguage = (TextView) findViewById(R.id.my_words_textView);
	}
	
	private void initValue() {
		txtLanguage.setText(PreferenceUtility.getLanguageString());
	}
	
	private void initEvent() {
		rlytLanguage.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivityForResult(new Intent(SettingsGeneralActivity.this, LanguageActivity.class), REQUEST_CODE_LANGUAGE);
			}
		});
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
		if (requestCode == REQUEST_CODE_LANGUAGE) {
			txtLanguage.setText(PreferenceUtility.getLanguageString());
        }
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle presses on the action bar items
	    switch (item.getItemId()) {
	        case android.R.id.home:
	        	finish();
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
}
