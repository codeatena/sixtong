package com.han.chatapp.settings;

import java.util.ArrayList;
import com.han.chatapp.R;
import com.han.quickblox.module.QBMyUserService;
import com.han.widget.listView.CellItem;
import com.han.widget.listView.MyListAdapter;
import com.han.widget.listView.MyListItem;
import com.han.widget.listView.SectionItem;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class SettingsActivity extends Activity {

	public static final int RESULT_CODE_SIGN_OUT = 5;
	
	ListView lstSettings;
	MyListAdapter adpSettings;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_simple_list);

	    ActionBar actionBar = SettingsActivity.this.getActionBar();
	    actionBar.setHomeButtonEnabled(true);
	    
	    initWidget();
	    initValue();
	    initEvent();
	}
	
	private void initWidget() {
		lstSettings = (ListView) findViewById(R.id.listView);
	}
	
	private void initValue() {
		ArrayList<MyListItem> arrItem = new ArrayList<MyListItem>();
		
		arrItem.add(new SectionItem(""));
		arrItem.add(new CellItem(getResources().getString(R.string.notifications), "", false));
		arrItem.add(new CellItem(getResources().getString(R.string.chat), "", false));
		arrItem.add(new CellItem(getResources().getString(R.string.general), "", false));
		
		arrItem.add(new SectionItem(""));
		arrItem.add(new CellItem(getResources().getString(R.string.about), "", false));
		arrItem.add(new CellItem(getResources().getString(R.string.logout), "", false));
		
		adpSettings = new MyListAdapter(this, arrItem);
		lstSettings.setAdapter(adpSettings);
		lstSettings.setDividerHeight(0);
	}
	
	private void initEvent() {
		lstSettings.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				if (position == 1) {
					startActivity(new Intent(SettingsActivity.this, SettingsNotificationsActivity.class));
				}
				if (position == 2) {
					startActivity(new Intent(SettingsActivity.this, SettingsChatActivity.class));
				}
				if (position == 3) {
					startActivity(new Intent(SettingsActivity.this, SettingsGeneralActivity.class));
				}
				if (position == 5) {
					startActivity(new Intent(SettingsActivity.this, AboutActivity.class));
				}
				if (position == 6) {
					new QBMyUserService(SettingsActivity.this).signout();
				}
			}
		});
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle presses on the action bar items
	    switch (item.getItemId()) {
	        case android.R.id.home:
	        	finish();
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
}